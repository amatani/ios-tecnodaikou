#!/bin/bash

set -e

source .deploy.conf
[ ! -z $UPLOADER_HOST ]
[ ! -z $JENKINS_URL ]
[ ! -z $JENKINS_TOKEN ]

scp build/driver_staging.ipa uploader@${UPLOADER_HOST}:techno/driver_staging.ipa
scp build/driver_production.ipa uploader@${UPLOADER_HOST}:techno/driver_production.ipa

curl "${JENKINS_URL}/buildByToken/build?job=techno-driver-mobile-i-archive&token=${JENKINS_TOKEN}"
