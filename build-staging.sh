#! /bin/sh

if [ ! -f .build-ama-staging.conf ]; then
  exit 1
fi
source .build-ama-staging.conf
if [ -z $DEPLOYGATE_TOKEN ]; then
  exit 1
fi

# APP TITLE
APPTITLE="テクノ代行スケジュール登録アプリ"

# Xcodeのプロジェクト名
PROJ_FILE_PATH="driver.xcodeproj"

# ワークスペース
WORKSPACE_NAME="driver.xcworkspace"

# バージョン
VERSION="1.0.0"

# ターゲット名
TARGET_NAME="driver_staging"

# プロビジョニングプロファイル名（******.mobileprovisionの拡張子のけた名前）
PROVISIONING_PROFILE_NAME="technodriverstaging_inhouse"

# ダウンロードURL
DOWNLOADURL="https://driver.technodriver.net/abc/def"

# アーカイブ出力ファイルパス
PATHNAME="$HOME/Desktop"

# Deploygateアップロード用Token
# .build-ama-staging.conf により設定

# Deploygateアップロード用メッセージ
DEPLOYGATE_MESSAGE="Terminal Upload."

# Deploygateアップロード用オーナーURL
DEPLOYGATE_OWENERNAME="https://deploygate.com/api/users/akafune/apps"

# クリーン
# -------------------------
xcodebuild clean -project "${PROJ_FILE_PATH}"

# コンパイル
xcodebuild -workspace "${WORKSPACE_NAME}" -scheme "${TARGET_NAME}" archive -archivePath "${PATHNAME}"/"${TARGET_NAME}".xcarchive

# ipaファイル作成
xcodebuild -exportArchive -archivePath "${PATHNAME}"/"${TARGET_NAME}".xcarchive -exportPath "${PATHNAME}" -exportOptionsPlist exportOptions.plist

echo '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>items</key>
  <array>
    <dict>
      <key>assets</key>
      <array>
        <dict>
          <key>kind</key>
          <string>software-package</string>
          <key>url</key>
          <string>'${DOWNLOADURL}'/'${TARGET_NAME}'.ipa</string>
        </dict>
      </array>
      <key>metadata</key>
      <dict>
        <key>bundle-identifier</key>
        <string>net.technodriver.driver</string>
        <key>bundle-version</key>
        <string>'${VERSION}'</string>
        <key>kind</key>
        <string>software</string>
        <key>title</key>
        <string>'${APPTITLE}'</string>
      </dict>
    </dict>
  </array>
</dict>
</plist>' > "${PATHNAME}"/"${TARGET_NAME}".plist

#コンパイルファイル削除
rm -r "${PATHNAME}"/"${TARGET_NAME}".xcarchive

#deploygateアップロード
curl -F "file=@${PATHNAME}/${TARGET_NAME}.ipa" -F "token=${DEPLOYGATE_TOKEN}" -F "message=${DEPLOYGATE_MESSAGE}" ${DEPLOYGATE_OWENERNAME}
