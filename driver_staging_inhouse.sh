#! /bin/sh

# コンフィグレーション(「Debug」、「Release」、「Ad hoc」)
CONFIGURATION="Release build"

# ワークスペース
WORKSPACE_NAME="driver.xcworkspace"

# Xcodeのプロジェクト名
PROJ_FILE_PATH="driver.xcodeproj"

# ターゲット名
TARGET_NAME="driver_staging"

# スキーム名
SCHEME_NAME="driver_staging"

# アーカイブ出力ファイル
ARCHIVE_PATHNAME="/Users/MacServer/Desktop/driver_staging.xcarchive"

# iPA出力ファイル
IPA_PATHNAME="/Users/MacServer/Desktop/driver_staging.ipa"

# プロビジョニングプロファイル名（******.mobileprovisionの拡張子のけた名前） 
PROVISIONING_PROFILE_NAME="technodriverstaging_inhouse_201404262127"


# クリーン
# -------------------------
xcodebuild clean -project "${PROJ_FILE_PATH}"

# コンパイル
xcodebuild -workspace "${WORKSPACE_NAME}" -scheme "${SCHEME_NAME}" archive -archivePath "${ARCHIVE_PATHNAME}"

# ipaファイル作成
xcodebuild -exportArchive -exportFormat ipa -archivePath "${ARCHIVE_PATHNAME}" -exportPath "${IPA_PATHNAME}" -exportProvisioningProfile "${PROVISIONING_PROFILE_NAME}"
