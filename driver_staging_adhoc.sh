#! /bin/sh

# コンフィグレーション(「Debug」、「Release」、「Ad hoc」)
CONFIGURATION="Ad hoc"

# ワークスペース
WORKSPACE_NAME="driver.xcworkspace"

# Xcodeのプロジェクト名
PROJ_FILE_PATH="driver.xcodeproj"

# ターゲット名
TARGET_NAME="driver_staging"

# スキーム名
SCHEME_NAME="driver_staging"

# アーカイブ出力ファイル
ARCHIVE_PATHNAME="/Users/amatani/Desktop/driver_staging.xcarchive"

# iPA出力ファイル
IPA_PATHNAME="/Users/amatani/Desktop/driver_staging.ipa"

# プロビジョニングプロファイル名（ファイルの拡張子のけた名前） 
PROVISIONING_PROFILE_NAME="technodriver_adhoc_201602021810"



# クリーン
# -------------------------
xcodebuild clean -project "${PROJ_FILE_PATH}"

# コンパイル
xcodebuild -workspace "${WORKSPACE_NAME}" -scheme "${SCHEME_NAME}" archive -archivePath "${ARCHIVE_PATHNAME}"

# ipaファイル作成
xcodebuild -exportArchive -exportFormat ipa -archivePath "${ARCHIVE_PATHNAME}" -exportPath "${IPA_PATHNAME}" -exportProvisioningProfile "${PROVISIONING_PROFILE_NAME}"





xcodebuild clean -project "driver.xcodeproj"

xcodebuild -project "driver.xcodeproj" -sdk "iphoneos11.2" -configuration "Release" -target "driver_staging" install DSTROOT="/Users/amatani/Desktop/driver_staging.app"

xcrun -sdk "iphoneos7.0" PackageApplication "/Users/amatani/Desktop/driver_staging.app" -o "/Users/amatani/Desktop/driver_staging.ipa" -embed "~/Library/MobileDevice/Provisioning\  Profiles/sample.mobileprovision"
