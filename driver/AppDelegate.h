//
//  AppDelegate.h
//  tecnodaikou
//
//  Created by MacServer on 2015/12/04.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

@interface AppDelegate : UIResponder <UIApplicationDelegate>

//ログインのドライバーナンバー桁数設定（ゼロパディング用）
@property (assign, nonatomic) long loginNoColumCount;

@property (strong, nonatomic) UIWindow *window;

@end

