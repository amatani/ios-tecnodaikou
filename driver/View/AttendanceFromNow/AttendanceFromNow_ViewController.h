//
//  AttendanceFromNow_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/28.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@protocol AttendanceFromNow_ViewControllerDelegate <NSObject>
- (void)AttendanceFromNow_LoginBackActionView;
@end

@interface AttendanceFromNow_ViewController : UIViewController <ApiDelegate, CLLocationManagerDelegate, GMSMapViewDelegate> {
    
    id<AttendanceFromNow_ViewControllerDelegate> _AttendanceFromNow_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    GMSMapView *gmapView;
    GMSMarker *marker;
    GMSCameraPosition *camera;
    double dbl_GPSlatitude;
    double dbl_GPSlongitude;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
}
@property (nonatomic) id<AttendanceFromNow_ViewControllerDelegate> AttendanceFromNow_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

@property (nonatomic, retain) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UIView *googlemapView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *TimeSelectSW;

@property (weak, nonatomic) IBOutlet UIView *fadeinMessage;

@property (weak, nonatomic) IBOutlet UIView *potsitionSetView;

@property (weak, nonatomic) IBOutlet UIView *postView;


- (IBAction)LeftButton:(id)sender;

- (IBAction)Set_Push:(id)sender;

- (IBAction)Chenge_Push:(id)sender;

- (IBAction)setPotision_Push:(id)sender;

- (IBAction)Post_Push:(id)sender;

- (IBAction)Rechange_Push:(id)sender;

@end