//
//  AttendanceFromNow_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2016/01/28.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "AttendanceFromNow_ViewController.h"

@interface AttendanceFromNow_ViewController ()
@end

@implementation AttendanceFromNow_ViewController

@synthesize AttendanceFromNow_delegate = _AttendanceFromNow_delegate;
@synthesize rootView = _rootView;
@synthesize api = _api;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([AttendanceFromNow_ViewController class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;

    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([AttendanceFromNow_ViewController class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    // ユーザから位置情報の利用について承認
    if (nil == self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        // iOS 8以上
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            // NSLocationWhenInUseUsageDescriptionに設定したメッセージでユーザに確認
            [ self.locationManager requestWhenInUseAuthorization];
            // NSLocationAlwaysUsageDescriptionに設定したメッセージでユーザに確認
            //[locationManager requestAlwaysAuthorization];
        }
    }
    
    if (nil == self.locationManager){
        self.locationManager = [[CLLocationManager alloc] init];
    }
    self.locationManager.delegate = self;
    
    //self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    // 更新間隔はdistanceFilterプロパティ
    //self.locationManager.distanceFilter = 500;
    
    // 情報の更新を開始すれば、位置情報を取得
    [self.locationManager startUpdatingLocation];
    
    //メッセージ非表示
    _fadeinMessage.hidden = true;
    //位置調整確定ボタン非表示
    _potsitionSetView.hidden = true;
    //送信ボタン非表示
    _postView.hidden = true;
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([AttendanceFromNow_ViewController class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([AttendanceFromNow_ViewController class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([AttendanceFromNow_ViewController class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([AttendanceFromNow_ViewController class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([AttendanceFromNow_ViewController class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation* location = [locations lastObject];
    
    dbl_GPSlatitude = location.coordinate.latitude;
    dbl_GPSlongitude = location.coordinate.longitude;
    
    NSLog(@"緯度 %+.6f, 経度 %+.6f\n", dbl_GPSlatitude, dbl_GPSlongitude);
    
    if(nil == gmapView){
        //Googlemapセット
        [GMSServices provideAPIKey:@"AIzaSyAe5ogIO4YtOwCeOj0PwcSMURZ3D5FZN08"];
        //GoogleMap生成
        camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                             longitude:location.coordinate.longitude
                                                  zoom:18];
        gmapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
        // 移動ジェスチャを無効に
        gmapView.settings.scrollGestures = NO;
        gmapView.delegate = self;
        
        //地図セット
        gmapView.frame = self.googlemapView.bounds;
        [self.googlemapView addSubview:gmapView];
    }
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    
    NSLog(@"You tapped at %f,%f", position.target.latitude, position.target.longitude);
    
    camera = [GMSCameraPosition cameraWithLatitude:position.target.latitude
                                         longitude:position.target.longitude
                                              zoom:mapView.camera.zoom];
    
    //マップピンセット
    marker.map = nil;
    marker = [[GMSMarker alloc] init];
    marker.position = camera.target;
    //    marker.snippet = @"Hello World";
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = gmapView;
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"エラー" message:@"位置情報が取得できませんでした。" delegate:nil cancelButtonTitle:NSLocalizedString(@"Dialog_Ok",@"") otherButtonTitles: nil];
    [alertView show];
    
}

- (IBAction)LeftButton:(id)sender {
    
    // 前画面に戻る
    [self.navigationController popToViewController:_rootView animated:YES];
}

- (IBAction)Set_Push:(id)sender {
    
    //送信ボタン表示
    _postView.hidden = false;
}

- (IBAction)Chenge_Push:(id)sender {
    
    //メッセージ表示
    _fadeinMessage.hidden = false;
    
    // 移動ジェスチャを有効に
    gmapView.settings.scrollGestures = YES;
    
    //位置調整確定ボタン表示
    _potsitionSetView.hidden = false;
}

- (IBAction)setPotision_Push:(id)sender {
    
    //送信ボタン非表示
    _postView.hidden = true;
    
    //メッセージ非表示
    _fadeinMessage.hidden = true;
    
    // 移動ジェスチャを無効に
    gmapView.settings.scrollGestures = NO;
    
    //位置調整確定ボタン表示
    _potsitionSetView.hidden = true;
}

- (IBAction)Post_Push:(id)sender {
    
    CGPoint point = gmapView.center;
    CLLocationCoordinate2D maplocation = [gmapView.projection coordinateForPoint:point];
    NSLog(@"緯度 %+.6f, 経度 %+.6f\n", maplocation.latitude, maplocation.longitude);
    
    long lng_TimeSet = _TimeSelectSW.selectedSegmentIndex;
    NSLog(@"追加時間セット - %ld",lng_TimeSet);
    
    [self messageError:@"OK" Title:@"" message:@"送信完了しました。"];
}

- (IBAction)Rechange_Push:(id)sender {
    
    //送信ボタン非表示
    _postView.hidden = true;
    
    //メッセージ表示
    _fadeinMessage.hidden = false;
    
    // 移動ジェスチャを有効に
    gmapView.settings.scrollGestures = YES;
    
    //位置調整確定ボタン表示
    _potsitionSetView.hidden = false;
}

-(void)messageError:(NSString*)typeName
              Title:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if([typeName isEqualToString:@""]){
        
        //OKのみのシングルアクション
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
    }else if([typeName isEqualToString:@"OK"]){
        
        //保存完了アクション
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self okSaveButtonPushed];
        }]];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)okSaveButtonPushed {
    // 保存完了後のアラートアクション
    // 前画面に戻る
    [self.navigationController popViewControllerAnimated:YES];
}

//バックアクション
//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
