//
//  PasswordReset_ViewController.m
//  driver
//
//  Created by MacServer on 2016/03/11.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "PasswordReset_ViewController.h"

@interface PasswordReset_ViewController () {
    
    UITextField *Set_TextField;
}
@end

@implementation PasswordReset_ViewController

@synthesize PasswordReset_delegate = _PasswordReset_delegate;
@synthesize rootView = _rootView;
@synthesize api = _api;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //データピッカーの初期日設定
    NSCalendar* calendar = [NSCalendar currentCalendar];
    //12時間表記対策
    [calendar setLocale:[NSLocale systemLocale]];
    //タイムゾーンの指定
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
    NSDateComponents* components = [[NSDateComponents alloc] init];
    
    components.year = 2000;
    components.month = 1;
    components.weekday = 1;
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    
    NSDate* fromFormatDate = [calendar dateFromComponents:components];
    self.Picker_Date.date = fromFormatDate;
    
    //データピッカーの最大値設定
    self.Picker_Date.maximumDate = [NSDate date];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    //ツールバーを生成（メールアドレス）
    UIToolbar *meil_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    meil_Text_toolBar.barStyle = UIBarStyleDefault;
    [meil_Text_toolBar sizeToFit];
    UIBarButtonItem *meil_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_meil_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(meil_Text_toolBar_closeKeyboard:)];
    NSArray *meil_Text_toolBarItems = [NSArray arrayWithObjects:meil_Text_spacer, _meil_Text_commitBtn, nil];
    [meil_Text_toolBar setItems:meil_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.meil_Text.inputAccessoryView = meil_Text_toolBar;
    
    //ツールバーを生成（パスワード）
    UIToolbar *Password_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    Password_Text_toolBar.barStyle = UIBarStyleDefault;
    [Password_Text_toolBar sizeToFit];
    UIBarButtonItem *Password_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_Password_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DriverNo_Text_toolBar_closeKeyboard:)];
    NSArray *Password_Text_toolBarItems = [NSArray arrayWithObjects:Password_Text_spacer, _Password_Text_commitBtn, nil];
    [Password_Text_toolBar setItems:Password_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.Password_Text.inputAccessoryView = Password_Text_toolBar;
    
    //ツールバーを生成（電話番号）
    UIToolbar *Tel_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    Tel_Text_toolBar.barStyle = UIBarStyleDefault;
    [Tel_Text_toolBar sizeToFit];
    UIBarButtonItem *Tel_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_Tel_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(Tel_Text_toolBar_closeKeyboard:)];
    NSArray *Tel_Text_toolBarItems = [NSArray arrayWithObjects:Tel_Text_spacer, _Tel_Text_commitBtn, nil];
    [Tel_Text_toolBar setItems:Tel_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.Tel_Text.inputAccessoryView = Tel_Text_toolBar;
    
    //テキストフィールド入力時の起動メソッド設定
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //スクロールを制限
    if(scrollView.contentOffset.y < 0){
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
    }
}

/////////////// ↓ テキスト入力関連 ↓ ////////////////////
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    Set_TextField = textField;
    return YES;
}

//テキストフィールドを編集する直後に呼び出されます
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    NSLog(@"textFieldDidBeginEditing");
}

//入力開始時の制御
-(void)keyboardWillShow:(NSNotification*)note {
    
    // キーボードの表示開始時の場所と大きさを取得します。
    CGRect keyboardFrameBegin = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if(Set_TextField == self.meil_Text){
        int pointY = self.meil_Text.frame.origin.y + 100 + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
    
    if(Set_TextField == self.Password_Text){
        int pointY = self.Password_Text.frame.origin.y + 100 + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
    
    if(Set_TextField == self.Tel_Text){
        int pointY = self.Tel_Text.frame.origin.y + 100 + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    
    if(textField == self.meil_Text){
        //キーボードを隠す
        [self.meil_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    if(textField == self.Password_Text){
        //キーボードを隠す
        [self.Password_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    if(textField == self.Tel_Text){
        //キーボードを隠す
        [self.Tel_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    return NO;
}
/////////////// ↑ テキスト入力関連 ↑ ////////////////////

/////////////// ↓ テキスト入力補助 ↓ ////////////////////
-(void)meil_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.meil_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}

-(void)DriverNo_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.Password_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}

-(void)Tel_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.Tel_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}
/////////////// ↑ テキスト入力補助 ↑ ////////////////////

//次画面からバックする前に処理するメソッド
- (void)lostAcountSet_totalBackActionView {
    
    //フォームのクローズ
    [self.navigationController popToViewController:_rootView animated:NO];
}

- (IBAction)Cansel_push:(id)sender {
    
    //フォームのクローズ
    [self.navigationController popToViewController:_rootView animated:YES];
}

- (IBAction)Check_push:(id)sender {
    
    [self.meil_Text resignFirstResponder];
    [self.Password_Text resignFirstResponder];
    [self.Tel_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
    
    if(![self.meil_Text.text isEqualToString:@""]){
        if([InputCheck check_Password:self.Password_Text.text]){
            if(![self.Tel_Text.text isEqualToString:@""]){
                if((self.Tel_Text.text.length == 11)){
                
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"YYYY-MM-dd"];
                    //12時間表記対策
                    [formatter setLocale:[NSLocale systemLocale]];
                    //タイムゾーンの指定
                    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
                    NSString* date_converted = [formatter stringFromDate:self.Picker_Date.date];
                    
                    //メールアドレスでログイン
                    if(![_api Api_PasswordReset:self mailAdress:self.meil_Text.text driverNo:[Configuration getID] telNo:self.Tel_Text.text birtheday:date_converted]){
                        
                        //エラーメッセージ用トースト
                        [self.view makeToast:@"Not Set ViewController."];
                    }
                }else{
                    [self messageError:@"" message:@"携帯電話番号の入力は11桁指定です。再入力お願いします。"];
                }
            }else{
                [self messageError:@"" message:@"未入力項目があります。\n再入力お願いします。"];
            }
        }else{
            [self messageError:@"" message:@"PWは英数字4桁指定です。再入力お願いします。"];
        }
    }else{
        [self messageError:@"" message:@"未入力項目があります。\n再入力お願いします。"];
    }
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"")
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//バックアクション
//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        //フォームのクローズ
        [self.navigationController popToViewController:_rootView animated:NO];
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                                                        [formatter setDateFormat:@"YYYY-MM-dd"];
                                                        //12時間表記対策
                                                        [formatter setLocale:[NSLocale systemLocale]];
                                                        //タイムゾーンの指定
                                                        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
                                                        NSString* date_converted = [formatter stringFromDate:self.Picker_Date.date];
                                                        
                                                        [_api Api_PasswordReset:self mailAdress:self.meil_Text.text driverNo:[Configuration getID] telNo:self.Tel_Text.text birtheday:date_converted];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
