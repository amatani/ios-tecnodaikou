//
//  PasswordReset_ViewController.h
//  driver
//
//  Created by MacServer on 2016/03/11.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PasswordReset_ViewControllerDelegate<NSObject>
- (void)PasswordReset_LoginBackActionView;
@end

@interface PasswordReset_ViewController : UIViewController <ApiDelegate> {
    
    id<PasswordReset_ViewControllerDelegate> _PasswordReset_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
}
@property (nonatomic) id<PasswordReset_ViewControllerDelegate> PasswordReset_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;

@property (weak, nonatomic) IBOutlet UITextField *meil_Text;
@property (weak, nonatomic) IBOutlet UITextField *Password_Text;
@property (weak, nonatomic) IBOutlet UITextField *Tel_Text;
@property (weak, nonatomic) IBOutlet UIDatePicker *Picker_Date;

- (IBAction)Cansel_push:(id)sender;
- (IBAction)Check_push:(id)sender;

@end