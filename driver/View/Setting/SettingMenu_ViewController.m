//
//  SettingMenu_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2016/02/01.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "SettingMenu_ViewController.h"

@interface SettingMenu_ViewController ()
@end

@implementation SettingMenu_ViewController

@synthesize SettingMenu_delegate = _SettingMenu_delegate;
@synthesize rootView = _rootView;
@synthesize api = _api;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    //セル
    UINib *nib = [UINib nibWithNibName:@"SettingMenu_Cell" bundle:nil];
    SettingMenu_Cell *cell = [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
    settingTable.rowHeight = cell.frame.size.height;
    [settingTable registerNib:nib forCellReuseIdentifier:@"SettingMenu_Cell"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
    
    if(_bln_loginFlg == true){
        [_SettingMenu_delegate SettingMenu_LoginBackActionView];
    }
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableViewControllerDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == settingTable){
        // Instantiate or reuse cell
        SettingMenu_Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingMenu_Cell"];
        
        NSUInteger row = (NSUInteger)indexPath.section;
        switch (row) {
            case 0:
                cell.lbl_Name.text = @"基本時間変更";
                break;
            case 1:
                cell.lbl_Name.text = @"プッシュ通知設定";
                break;
            case 2:
                cell.lbl_Name.text = @"メールアドレス変更";
                break;
            case 3:
                cell.lbl_Name.text = @"パスワード変更";
                break;
            case 4:
                cell.lbl_Name.text = @"アプリの使い方";
                break;
            case 5:
                cell.lbl_Name.text = [NSString stringWithFormat:@"アプリVer.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"BundleUseInfoVersion"]];
                break;
                
            default:
                break;
        }
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld, %ld", (long)indexPath.section, (long)indexPath.row);
    
    UIStoryboard *storyboard;
    TotalBasicTimeSetting_ViewController *TotalBasicTimeSetting_initialViewController;
    PushSetting_ViewController* PushSetting_initialViewController;
    MailAdressChenge_ViewController* MailAdressChenge_initialViewController;
//    PasswordReset_ViewController* PasswordReset_initialViewController;
    LostAcount_ViewController *LostAcount_initialViewController;
    UsedInfomation_ViewController *UsedInfomation_ViewController;
    
    switch (indexPath.section) {
        case 0:
            //基本時間設定画面
            storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
            TotalBasicTimeSetting_initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"TotalBasicTimeSetting"];
            TotalBasicTimeSetting_initialViewController.TotalBasicTimeSetting_delegate = self;
            TotalBasicTimeSetting_initialViewController.secondView = self;
            TotalBasicTimeSetting_initialViewController.rootView = self;
            TotalBasicTimeSetting_initialViewController.api = _api;
            
            [self.navigationController pushViewController:TotalBasicTimeSetting_initialViewController animated:YES];
            break;
            
        case 1:
            //プッシュ通知設定画面
            storyboard = [UIStoryboard storyboardWithName:@"PushSetting_ViewController" bundle:[NSBundle mainBundle]];
            PushSetting_initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"PushSetting"];
            PushSetting_initialViewController.PushSetting_delegate = self;
            PushSetting_initialViewController.rootView = self;
            PushSetting_initialViewController.api = _api;
            
            [self.navigationController pushViewController:PushSetting_initialViewController animated:YES];
            break;
            
        case 2:
            //メールアドレス変更画面
            storyboard = [UIStoryboard storyboardWithName:@"MailAdressChenge_ViewController" bundle:[NSBundle mainBundle]];
            MailAdressChenge_initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"MailAdressChenge"];
            MailAdressChenge_initialViewController.MailAdressChenge_delegate = self;
            MailAdressChenge_initialViewController.rootView = self;
            MailAdressChenge_initialViewController.api = _api;
            
            [self.navigationController pushViewController:MailAdressChenge_initialViewController animated:YES];
            break;
            
        case 3:
            //パスワード変更画面
            storyboard = [UIStoryboard storyboardWithName:@"TopLogin_ViewController" bundle:[NSBundle mainBundle]];
            LostAcount_initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"LostAcount"];
            LostAcount_initialViewController.LostAcount_delegate = self;
            LostAcount_initialViewController.loginrootView = self;
            LostAcount_initialViewController.loginSetView = self;
            LostAcount_initialViewController.api = _api;
            LostAcount_initialViewController.str_BackButtonText = @"戻る";
            
            [self.navigationController pushViewController:LostAcount_initialViewController animated:NO];
            break;
            
        case 4:
            //アプリの使い方画面
            storyboard = [UIStoryboard storyboardWithName:@"UsedInfomation_ViewController" bundle:[NSBundle mainBundle]];
            UsedInfomation_ViewController = [storyboard instantiateViewControllerWithIdentifier: @"UsedInfomation"];
            UsedInfomation_ViewController.UsedInfomation_delegate = self;
            UsedInfomation_ViewController.rootView = self;
            
            [self.navigationController pushViewController:UsedInfomation_ViewController animated:NO];
            break;
    }
}

- (IBAction)LeftButton:(id)sender {
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:YES];
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"")
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//バックアクション
- (void)TotalBasicTimeSetting_BackActionView {
}

//Login画面移動用
- (void)TotalBasicTimeSetting_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}
- (void)PushSetting_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}
- (void)MailAdressChenge_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}
- (void)PasswordReset_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
