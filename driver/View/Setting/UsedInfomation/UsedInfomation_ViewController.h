//
//  UsedInfomation_ViewController.h
//  driver
//
//  Created by MacServer on 2016/04/19.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

@protocol UsedInfomation_ViewControllerDelegate <NSObject>
@end

@interface UsedInfomation_ViewController : UIViewController
{
    id<UsedInfomation_ViewControllerDelegate> _UsedInfomation_delegate;
    UIViewController* _rootView;
}
@property (nonatomic) id<UsedInfomation_ViewControllerDelegate> UsedInfomation_delegate;
@property (nonatomic) UIViewController* rootView;

@property (weak, nonatomic) IBOutlet UIWebView *web_view;

- (IBAction)LeftButton:(id)sender;

@end