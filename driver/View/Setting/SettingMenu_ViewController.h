//
//  SettingMenu_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/02/01.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "SettingMenu_Cell.h"
#import "TotalBasicTimeSetting_ViewController.h"
#import "PushSetting_ViewController.h"
#import "MailAdressChenge_ViewController.h"
#import "PasswordReset_ViewController.h"
#import "LostAcount_ViewController.h"
#import "UsedInfomation_ViewController.h"

@protocol SettingMenu_ViewControllerDelegate <NSObject>
- (void)SettingMenu_LoginBackActionView;
@end

@interface SettingMenu_ViewController : UIViewController <ApiDelegate, TotalBasicTimeSetting_ViewControllerDelegate, PushSetting_ViewControllerDelegate, MailAdressChenge_ViewControllerDelegate, PasswordReset_ViewControllerDelegate, LostAcount_ViewControllerDelegate, UsedInfomation_ViewControllerDelegate>
{
    id<SettingMenu_ViewControllerDelegate> _SettingMenu_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    __weak IBOutlet UITableView *settingTable;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    NSMutableArray* _array_BasicSyussya;
    NSMutableArray* _array_BasicTaisya;
    long _lng_BasicTimeStart;
    long _lng_BasicTimeEnd;
    
}
@property (nonatomic) id<SettingMenu_ViewControllerDelegate> SettingMenu_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
@property (nonatomic) NSMutableArray* array_BasicSyussya;
@property (nonatomic) NSMutableArray* array_BasicTaisya;
@property (nonatomic) long lng_BasicTimeStart;
@property (nonatomic) long lng_BasicTimeEnd;

- (IBAction)LeftButton:(id)sender;

@end
