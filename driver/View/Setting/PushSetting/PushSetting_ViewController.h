//
//  PushSetting_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/02/01.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "SettingMenuSwitch_Cell.h"

@protocol PushSetting_ViewControllerDelegate <NSObject>
- (void)PushSetting_LoginBackActionView;
@end

@interface PushSetting_ViewController : UIViewController <ApiDelegate> {
    
    id<PushSetting_ViewControllerDelegate> _PushSetting_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    __weak IBOutlet UITableView *settingTable;
    
    BOOL bln_SW_before_attendance;
    BOOL bln_SW_empty;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
}
@property (nonatomic) id<PushSetting_ViewControllerDelegate> PushSetting_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

- (IBAction)RightButton:(id)sender;

@end