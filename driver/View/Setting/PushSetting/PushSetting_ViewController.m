//
//  PushSetting_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2016/02/01.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "PushSetting_ViewController.h"

@interface PushSetting_ViewController ()
@end

@implementation PushSetting_ViewController

@synthesize PushSetting_delegate = _PushSetting_delegate;
@synthesize rootView = _rootView;
@synthesize api = _api;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([PushSetting_ViewController class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    //セル
    UINib *nib = [UINib nibWithNibName:@"SettingMenuSwitch_Cell" bundle:nil];
    SettingMenuSwitch_Cell *cell = [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
    settingTable.rowHeight = cell.frame.size.height;
    [settingTable registerNib:nib forCellReuseIdentifier:@"SettingMenuSwitch_Cell"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([PushSetting_ViewController class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    //通知設置を取得
    [_api Api_NotificationGetting:self];
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([PushSetting_ViewController class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([PushSetting_ViewController class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([PushSetting_ViewController class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([PushSetting_ViewController class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([PushSetting_ViewController class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewControllerDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == settingTable){
        // Instantiate or reuse cell
        SettingMenuSwitch_Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingMenuSwitch_Cell"];
        
        NSUInteger row = (NSUInteger)indexPath.section;
        switch (row) {
            case 0:
                cell.lbl_Name.text = @"予定が休みでも空き枠通知を希望";
                cell.sw.on = [Configuration getPushSetting_empty];
                // イベントを付ける
                [cell.sw addTarget:self action:@selector(sw_pushButton1:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case 1:
                cell.lbl_Name.text = @"出社前通知";
                cell.sw.on = [Configuration getPushSetting_before_attendance];
                // イベントを付ける
                [cell.sw addTarget:self action:@selector(sw_pushButton2:) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            default:
                break;
        }
        
        return cell;
    }
    
    return nil;
}

- (void)sw_pushButton1:(UISwitch *)sw {
    
    if([Configuration getPushSetting_empty] == YES){
        
        bln_SW_empty = NO;
    }else{
        
        bln_SW_empty = YES;
    }
    
    [_api Api_NotificationSetting:self empty:bln_SW_empty before_attendance:[Configuration getPushSetting_before_attendance]];
}

- (void)sw_pushButton2:(UISwitch *)sw {
    
    if([Configuration getPushSetting_before_attendance] == YES){
        
        bln_SW_before_attendance = NO;
    }else{
        
        bln_SW_before_attendance = YES;
    }
    
    [_api Api_NotificationSetting:self empty:[Configuration getPushSetting_empty] before_attendance:bln_SW_before_attendance];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld, %ld", (long)indexPath.section, (long)indexPath.row);
}

- (IBAction)RightButton:(id)sender {
    
    // 前画面に戻る
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"")
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//バックアクション
//Api Delegateからのアクション
- (void)Api_Err_Other {
    
    // 前画面に戻る
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        bln_SW_empty = empty;
        bln_SW_before_attendance = before_attendance;
        [Configuration setPushSetting_empty:empty];
        [Configuration setPushSetting_before_attendance:before_attendance];
        
        [settingTable reloadData];
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_NotificationGetting:self];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }else{
            
            // 前画面に戻る
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        [Configuration setPushSetting_empty:bln_SW_empty];
        [Configuration setPushSetting_before_attendance:bln_SW_before_attendance];
        
        [settingTable reloadData];
    }else{
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }else{
            
            bln_SW_empty = [Configuration getPushSetting_empty];
            bln_SW_before_attendance = [Configuration getPushSetting_before_attendance];
            
            [settingTable reloadData];
        }
    }
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
