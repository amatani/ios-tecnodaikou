//
//  SettingMenu_Cell.h
//
//  Created by MacNote on 2014/08/11.
//  Copyright (c) 2014年 akafune, inc. All rights reserved.
//

@interface SettingMenu_Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;

@end