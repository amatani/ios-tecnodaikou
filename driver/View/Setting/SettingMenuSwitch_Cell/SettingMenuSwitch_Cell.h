//
//  SettingMenuSwitch_Cell.h
//  tecnodaikou
//
//  Created by MacServer on 2016/02/01.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

@interface SettingMenuSwitch_Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;
@property (weak, nonatomic) IBOutlet UISwitch *sw;

@end