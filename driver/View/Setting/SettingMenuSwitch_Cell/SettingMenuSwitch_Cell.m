//
//  SettingMenuSwitch_Cell.m
//  tecnodaikou
//
//  Created by MacServer on 2016/02/01.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "SettingMenuSwitch_Cell.h"

@implementation SettingMenuSwitch_Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
