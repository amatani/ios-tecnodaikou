//
//  MailAdressChenge_ViewController.m
//  driver
//
//  Created by MacServer on 2016/02/04.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "MailAdressChenge_ViewController.h"

@interface MailAdressChenge_ViewController () {
    
    UITextField *Set_TextField;
}
@end

@implementation MailAdressChenge_ViewController

@synthesize MailAdressChenge_delegate = _MailAdressChenge_delegate;
@synthesize rootView = _rootView;
@synthesize api = _api;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;

    //ツールバーを生成（旧メールアドレス）
    UIToolbar *oldmail_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    oldmail_Text_toolBar.barStyle = UIBarStyleDefault;
    [oldmail_Text_toolBar sizeToFit];
    UIBarButtonItem *oldmail_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_oldmail_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(oldMail_Text_toolBar_closeKeyboard:)];
    NSArray *meil_Text_toolBarItems = [NSArray arrayWithObjects:oldmail_Text_spacer, _oldmail_Text_commitBtn, nil];
    [oldmail_Text_toolBar setItems:meil_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.oldmail_Text.inputAccessoryView = oldmail_Text_toolBar;
    
    //ツールバーを生成（新メールアドレス）
    UIToolbar *newmail_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    newmail_Text_toolBar.barStyle = UIBarStyleDefault;
    [newmail_Text_toolBar sizeToFit];
    UIBarButtonItem *newmail_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_newmail_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(newMail_Text_toolBar_closeKeyboard:)];
    NSArray *DriverNo_Text_toolBarItems = [NSArray arrayWithObjects:newmail_Text_spacer, _newmail_Text_commitBtn, nil];
    [newmail_Text_toolBar setItems:DriverNo_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.newmail_Text.inputAccessoryView = newmail_Text_toolBar;

    //テキストフィールド入力時の起動メソッド設定
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
    
    
    if(_bln_loginFlg == true){
        
        //ログイン画面へ戻る
        [_MailAdressChenge_delegate MailAdressChenge_LoginBackActionView];
    }
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //スクロールを制限
    if(scrollView.contentOffset.y < 0){
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
    }
}

/////////////// ↓ テキスト入力関連 ↓ ////////////////////
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    Set_TextField = textField;
    return YES;
}

//テキストフィールドを編集する直後に呼び出されます
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    NSLog(@"textFieldDidBeginEditing");
}

//入力開始時の制御
-(void)keyboardWillShow:(NSNotification*)note {
    
    // キーボードの表示開始時の場所と大きさを取得します。
    CGRect keyboardFrameBegin = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if(Set_TextField == self.oldmail_Text){
        int pointY = self.oldmail_Text.frame.origin.y + 30 + 60 + 14 + 10 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
    
    if(Set_TextField == self.newmail_Text){
        int pointY = self.newmail_Text.frame.origin.y + 30 + 60 + 14 + 10 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    
    if(textField == self.oldmail_Text){
        //キーボードを隠す
        [self.oldmail_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    if(textField == self.newmail_Text){
        //キーボードを隠す
        [self.newmail_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    return NO;
}
/////////////// ↑ テキスト入力関連 ↑ ////////////////////

/////////////// ↓ テキスト入力補助 ↓ ////////////////////
-(void)oldMail_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.oldmail_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}

-(void)newMail_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.newmail_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}

/////////////// ↑ テキスト入力補助 ↑ ////////////////////

- (IBAction)Cansel_push:(id)sender {
    
    // 前画面に戻る
    [self.navigationController popToViewController:_rootView animated:YES];
}

- (IBAction)Save_push:(id)sender {
    
    //メールアドレス変更
    if(![_api Api_MailAdressChenge:self oldMailAdress:self.oldmail_Text.text newMailAdress:self.newmail_Text.text]){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set ViewController."];
    }
}

//バックアクション
//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){

        // 前画面に戻る
        [self.navigationController popToViewController:_rootView animated:YES];
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_MailAdressChenge:self oldMailAdress:self.oldmail_Text.text newMailAdress:self.newmail_Text.text];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
