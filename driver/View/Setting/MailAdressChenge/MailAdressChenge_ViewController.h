//
//  MailAdressChenge_ViewController.h
//  driver
//
//  Created by MacServer on 2016/02/04.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

@protocol MailAdressChenge_ViewControllerDelegate<NSObject>
- (void)MailAdressChenge_LoginBackActionView;
@end

@interface MailAdressChenge_ViewController : UIViewController <ApiDelegate> {
    
    id<MailAdressChenge_ViewControllerDelegate> _MailAdressChenge_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
}
@property (nonatomic) id<MailAdressChenge_ViewControllerDelegate> MailAdressChenge_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;

@property (weak, nonatomic) IBOutlet UITextField* oldmail_Text;
@property (weak, nonatomic) IBOutlet UITextField* newmail_Text;

- (IBAction)Cansel_push:(id)sender;
- (IBAction)Save_push:(id)sender;
@end