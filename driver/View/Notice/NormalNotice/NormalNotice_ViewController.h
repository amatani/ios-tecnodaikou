//
//  NormalNotice_ViewController.h
//  driver
//
//  Created by MacServer on 2016/02/04.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "NormalNotice_Cell.h"
#import "NormalNoticeInfo_ViewController.h"

@protocol NormalNotice_ViewControllerDelegate <NSObject>
@end

@interface NormalNotice_ViewController : UIViewController <ApiDelegate, NormalNoticeInfo_ViewControllerDelegate> {
    
    id<NormalNotice_ViewControllerDelegate> _NormalNotice_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    __weak IBOutlet UITableView *_listTable;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    NSMutableArray* _array_Id;
    NSMutableArray* _array_Title;
    NSMutableArray* _array_Read;
    NSMutableArray* _array_Created_at;
    NSMutableArray* _array_Syousai;
}
@property (nonatomic) id<NormalNotice_ViewControllerDelegate> NormalNotice_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
@property (nonatomic) NSMutableArray* array_Id;
@property (nonatomic) NSMutableArray* array_Title;
@property (nonatomic) NSMutableArray* array_Read;
@property (nonatomic) NSMutableArray* array_Created_at;
@property (nonatomic) NSMutableArray* array_Syousai;

- (IBAction)LeftButton:(id)sender;

@end
