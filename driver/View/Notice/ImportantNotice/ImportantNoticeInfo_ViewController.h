//
//  ImportantNoticeInfo_ViewController.h
//  driver
//
//  Created by MacServer on 2016/02/17.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

@protocol ImportantNoticeInfo_ViewControllerDelegate <NSObject>
@end

@interface ImportantNoticeInfo_ViewController : UIViewController <ApiDelegate> {
    
    id<ImportantNoticeInfo_ViewControllerDelegate> _ImportantNoticeInfo_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    NSString* _str_Id;
    NSString* _str_Title;
    BOOL _bln_Read;
    NSString* _str_Created_at;
    NSString* _str_Syousai;
}
@property (nonatomic) id<ImportantNoticeInfo_ViewControllerDelegate> ImportantNoticeInfo_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
@property (nonatomic) NSString* str_Id;
@property (nonatomic) NSString* str_Title;
@property (nonatomic) BOOL bln_Read;
@property (nonatomic) NSString* str_Created_at;
@property (nonatomic) NSString* str_Syousai;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Dt;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UITextView *text_Syousai;

- (IBAction)RightButton:(id)sender;

@end
