//
//  ImportantNotice_ViewController.m
//  driver
//
//  Created by MacServer on 2016/02/04.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "ImportantNotice_ViewController.h"

@interface ImportantNotice_ViewController ()
@end

@implementation ImportantNotice_ViewController

@synthesize ImportantNotice_delegate = _ImportantNotice_delegate;
@synthesize rootView = _rootView;
@synthesize api = _api;

//引き継がれるパラメータ
@synthesize array_Id = _array_Id;
@synthesize array_Title = _array_Title;
@synthesize array_Read = _array_Read;
@synthesize array_Created_at = _array_Created_at;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([ImportantNotice_ViewController class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    //セル
    UINib *nib = [UINib nibWithNibName:@"ImportantNotice_Cell" bundle:nil];
    ImportantNotice_Cell *cell = [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
    _listTable.rowHeight = cell.frame.size.height;
    [_listTable registerNib:nib forCellReuseIdentifier:@"ImportantNotice_Cell"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    //重要通知取得
    [_api Api_InportantNotificationGetting:self];
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableViewControllerDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return _array_Created_at.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _listTable){
        // Instantiate or reuse cell
        ImportantNotice_Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImportantNotice_Cell"];
        
        NSUInteger row = (NSUInteger)indexPath.section;
        cell.lbl_Date.text = [_array_Created_at objectAtIndex:row];
        cell.lbl_Name.text = [_array_Title objectAtIndex:row];
        if([[_array_Read objectAtIndex:row] boolValue] == NO){
            cell.view_New.hidden = false;
        }else{
            cell.view_New.hidden = true;
        }
        
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld, %ld", (long)indexPath.section, (long)indexPath.row);
    
    //お知らせ詳細画面
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ImportantNotice_ViewController" bundle:[NSBundle mainBundle]];
    ImportantNoticeInfo_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"ImportantNoticeInfo"];
    initialViewController.ImportantNoticeInfo_delegate = self;
    initialViewController.rootView = self;
    initialViewController.api = _api;
    
    initialViewController.str_Id = [_array_Id objectAtIndex:indexPath.section];
    initialViewController.str_Title = [_array_Title objectAtIndex:indexPath.section];
    initialViewController.str_Created_at = [_array_Created_at objectAtIndex:indexPath.section];
    initialViewController.bln_Read = [[_array_Read objectAtIndex:indexPath.section] boolValue];
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (IBAction)LeftButton:(id)sender {
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:YES];
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"")
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//Login画面移動用
- (void)ImportantNoticeInfo_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}

//バックアクション
//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        if(arrayData.count > 0){
            
            long lng_ReadCount = 0;
            _array_Id = [NSMutableArray array];
            _array_Title = [NSMutableArray array];
            _array_Read = [NSMutableArray array];
            _array_Created_at = [NSMutableArray array];
            
            for(long c = 0;c < arrayData.count;c++){
                
                [_array_Id addObject:[[arrayData objectAtIndex:c] objectAtIndex:0]];
                [_array_Title addObject:[[arrayData objectAtIndex:c] objectAtIndex:1]];
                [_array_Read addObject:[[arrayData objectAtIndex:c] objectAtIndex:2]];
                //日付型に一時変換
                NSDateFormatter* formatter = [NSDateFormatter new];
                formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
                //12時間表記対策
                [formatter setLocale:[NSLocale systemLocale]];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
                NSDate* date = [formatter dateFromString:[[arrayData objectAtIndex:c] objectAtIndex:3]];
                //日付を文字変換
                [formatter setDateFormat:@"YYYY/MM/dd"];
                NSString* date_converted = [formatter stringFromDate:date];
                
                [_array_Created_at addObject:date_converted];
                
                NSNumber* num_read = [[arrayData objectAtIndex:c] objectAtIndex:2];
                if([num_read boolValue] == NO){
                    lng_ReadCount = lng_ReadCount + 1;
                }
            }
            
            [_listTable reloadData];
        }
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_InportantNotificationGetting:self];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
