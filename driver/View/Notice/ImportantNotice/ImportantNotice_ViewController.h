//
//  ImportantNotice_ViewController.h
//  driver
//
//  Created by MacServer on 2016/02/04.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "ImportantNotice_Cell.h"
#import "ImportantNoticeInfo_ViewController.h"

@protocol ImportantNotice_ViewControllerDelegate <NSObject>
@end

@interface ImportantNotice_ViewController : UIViewController <ApiDelegate, ImportantNoticeInfo_ViewControllerDelegate> {
    
    id<ImportantNotice_ViewControllerDelegate> _ImportantNotice_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    __weak IBOutlet UITableView *_listTable;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    NSMutableArray* _array_Id;
    NSMutableArray* _array_Title;
    NSMutableArray* _array_Read;
    NSMutableArray* _array_Created_at;
    NSMutableArray* _array_Syousai;
}
@property (nonatomic) id<ImportantNotice_ViewControllerDelegate> ImportantNotice_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
@property (nonatomic) NSMutableArray* array_Id;
@property (nonatomic) NSMutableArray* array_Title;
@property (nonatomic) NSMutableArray* array_Read;
@property (nonatomic) NSMutableArray* array_Created_at;
@property (nonatomic) NSMutableArray* array_Syousai;

- (IBAction)LeftButton:(id)sender;

@end

