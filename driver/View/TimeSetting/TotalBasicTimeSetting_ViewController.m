//
//  TotalBasicTimeSetting_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2015/12/27.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "TotalBasicTimeSetting_ViewController.h"
#import "MainMenu_ViewController.h"

@interface TotalBasicTimeSetting_ViewController ()
@end

@implementation TotalBasicTimeSetting_ViewController

@synthesize TotalBasicTimeSetting_delegate = _TotalBasicTimeSetting_delegate;
@synthesize rootView = _rootView;
@synthesize secondView = _secondView;
@synthesize api = _api;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIFont *font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/13];
    self.Day1Week_lbl.font = font;
    self.Day2Week_lbl.font = font;
    self.Day3Week_lbl.font = font;
    self.Day4Week_lbl.font = font;
    self.Day5Week_lbl.font = font;
    self.Day6Week_lbl.font = font;
    self.Day7Week_lbl.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W3" size:[Configuration getScreenWidth]/30];
    self.Day1TimeFrom_lbl.font = font;
    self.Day2TimeFrom_lbl.font = font;
    self.Day3TimeFrom_lbl.font = font;
    self.Day4TimeFrom_lbl.font = font;
    self.Day5TimeFrom_lbl.font = font;
    self.Day6TimeFrom_lbl.font = font;
    self.Day7TimeFrom_lbl.font = font;
    self.Day1TimeTo_lbl.font = font;
    self.Day2TimeTo_lbl.font = font;
    self.Day3TimeTo_lbl.font = font;
    self.Day4TimeTo_lbl.font = font;
    self.Day5TimeTo_lbl.font = font;
    self.Day6TimeTo_lbl.font = font;
    self.Day7TimeTo_lbl.font = font;
    
    //カレンダーライブラリ用初期化
    _calHoliday = [[Wheel_Callender_Library alloc]init];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    //基本時間取得
    [_api Api_BasicTimeWeekGetting:self];
}

//初期値の設定
- (void)setFirstValue {
    
    //値の初期値設定
    _str_WeekDay1TimeStart = @"";
    _str_WeekDay1TimeEnd = @"";
    _str_WeekDay2TimeStart = @"";
    _str_WeekDay2TimeEnd = @"";
    _str_WeekDay3TimeStart = @"";
    _str_WeekDay3TimeEnd = @"";
    _str_WeekDay4TimeStart = @"";
    _str_WeekDay4TimeEnd = @"";
    _str_WeekDay5TimeStart = @"";
    _str_WeekDay5TimeEnd = @"";
    _str_WeekDay6TimeStart = @"";
    _str_WeekDay6TimeEnd = @"";
    _str_WeekDay7TimeStart = @"";
    _str_WeekDay7TimeEnd = @"";
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

//時間の設定
- (void)setTime {
    
    if([_str_WeekDay1TimeStart isEqualToString:@""]){
        self.Day1TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay1TimeStart];
        self.Day1TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay1TimeEnd isEqualToString:@""]){
        self.Day1TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay1TimeEnd isEqualToString:@"08:00"]){
        self.Day1TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay1TimeEnd];
        self.Day1TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay2TimeStart isEqualToString:@""]){
        self.Day2TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay2TimeStart];
        self.Day2TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay2TimeEnd isEqualToString:@""]){
        self.Day2TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay2TimeEnd isEqualToString:@"08:00"]){
        self.Day2TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay2TimeEnd];
        self.Day2TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay3TimeStart isEqualToString:@""]){
        self.Day3TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay3TimeStart];
        self.Day3TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay3TimeEnd isEqualToString:@""]){
        self.Day3TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay3TimeEnd isEqualToString:@"08:00"]){
        self.Day3TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay3TimeEnd];
        self.Day3TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay4TimeStart isEqualToString:@""]){
        self.Day4TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay4TimeStart];
        self.Day4TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay4TimeEnd isEqualToString:@""]){
        self.Day4TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay4TimeEnd isEqualToString:@"08:00"]){
        self.Day4TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay4TimeEnd];
        self.Day4TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay5TimeStart isEqualToString:@""]){
        self.Day5TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay5TimeStart];
        self.Day5TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay5TimeEnd isEqualToString:@""]){
        self.Day5TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay5TimeEnd isEqualToString:@"08:00"]){
        self.Day5TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay5TimeEnd];
        self.Day5TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay6TimeStart isEqualToString:@""]){
        self.Day6TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay6TimeStart];
        self.Day6TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay6TimeEnd isEqualToString:@""]){
        self.Day6TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay6TimeEnd isEqualToString:@"08:00"]){
        self.Day6TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay6TimeEnd];
        self.Day6TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay7TimeStart isEqualToString:@""]){
        self.Day7TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay7TimeStart];
        self.Day7TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay7TimeEnd isEqualToString:@""]){
        self.Day7TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay7TimeEnd isEqualToString:@"08:00"]){
        self.Day7TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay7TimeEnd];
        self.Day7TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
}

- (NSString *)createStringAddedCommaFromInt:(long)number
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    [format setGroupingSeparator:@","];
    [format setGroupingSize:3];
    
    return [format stringForObjectValue:[NSNumber numberWithLong:number]];
}

- (IBAction)LeftButton:(id)sender {
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:YES];
}

- (IBAction)OneWeekSelect_Push:(id)sender {
    
    bln_Day1Check = true;
    self.Day1Check_image.image = [UIImage imageNamed:@"check_on.png"];
    bln_Day2Check = true;
    self.Day2Check_image.image = [UIImage imageNamed:@"check_on.png"];
    bln_Day3Check = true;
    self.Day3Check_image.image = [UIImage imageNamed:@"check_on.png"];
    bln_Day4Check = true;
    self.Day4Check_image.image = [UIImage imageNamed:@"check_on.png"];
    bln_Day5Check = true;
    self.Day5Check_image.image = [UIImage imageNamed:@"check_on.png"];
    bln_Day6Check = true;
    self.Day6Check_image.image = [UIImage imageNamed:@"check_on.png"];
    bln_Day7Check = true;
    self.Day7Check_image.image = [UIImage imageNamed:@"check_on.png"];
}

- (IBAction)OneWeekUnSelect_Push:(id)sender {
    
    bln_Day1Check = false;
    self.Day1Check_image.image = [UIImage imageNamed:@"check_off.png"];
    bln_Day2Check = false;
    self.Day2Check_image.image = [UIImage imageNamed:@"check_off.png"];
    bln_Day3Check = false;
    self.Day3Check_image.image = [UIImage imageNamed:@"check_off.png"];
    bln_Day4Check = false;
    self.Day4Check_image.image = [UIImage imageNamed:@"check_off.png"];
    bln_Day5Check = false;
    self.Day5Check_image.image = [UIImage imageNamed:@"check_off.png"];
    bln_Day6Check = false;
    self.Day6Check_image.image = [UIImage imageNamed:@"check_off.png"];
    bln_Day7Check = false;
    self.Day7Check_image.image = [UIImage imageNamed:@"check_off.png"];
}

- (BOOL)getOverTime:(NSDate*)dt {
    
    //指定日の０時を取得
    NSString* dt_dateString = [NSString stringWithFormat:@"%ld-%ld-%ld 00:00:00", [_calHoliday yearNoGet:dt], [_calHoliday monthNoGet:dt], [_calHoliday dayNoGet:dt]];
    
    NSDateFormatter* dt_formatter = [[NSDateFormatter alloc] init];
    [dt_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //12時間表記対策
    [dt_formatter setLocale:[NSLocale systemLocale]];
    //タイムゾーンの指定
    [dt_formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
    NSDate *dt_date = [dt_formatter dateFromString:dt_dateString];
    
    //８時間追加
    NSDate *re_dt_date = [dt_date initWithTimeInterval:8*60*60 sinceDate:dt_date];
    
    //タイムゾーン含めた現在時間
    NSDate *now_dt = [NSDate dateWithTimeIntervalSinceNow:(60*60*9)];
    NSLog(@"現在： %@　ー　区切り日時:%@", now_dt, re_dt_date);
    
    NSComparisonResult result = [re_dt_date compare:now_dt];
    switch (result) {
        case NSOrderedAscending:
            // 過去
            NSLog(@"NSOrderedAscending");
            return false;
            break;
        case NSOrderedDescending:
            // 未来日
            NSLog(@"NSOrderedDescending");
            return true;
            break;
        case NSOrderedSame:
            // 当日
            NSLog(@"NSOrderedSame");
            return true;
            break;
        default:
            return false;
            break;
    }
}

- (IBAction)Day1Time_Push:(id)sender {
    
    if(bln_Day1Check){
        
        bln_Day1Check = false;
        self.Day1Check_image.image = [UIImage imageNamed:@"check_off.png"];
    }else{
        
        bln_Day1Check = true;
        self.Day1Check_image.image = [UIImage imageNamed:@"check_on.png"];
    }
}

- (IBAction)Day2Time_Push:(id)sender {
    
    if(bln_Day2Check){
        
        bln_Day2Check = false;
        self.Day2Check_image.image = [UIImage imageNamed:@"check_off.png"];
    }else{
        
        bln_Day2Check = true;
        self.Day2Check_image.image = [UIImage imageNamed:@"check_on.png"];
    }
}

- (IBAction)Day3Time_Push:(id)sender {
    
    if(bln_Day3Check){
        
        bln_Day3Check = false;
        self.Day3Check_image.image = [UIImage imageNamed:@"check_off.png"];
    }else{
        
        bln_Day3Check = true;
        self.Day3Check_image.image = [UIImage imageNamed:@"check_on.png"];
    }
}

- (IBAction)Day4Time_Push:(id)sender {
    
    if(bln_Day4Check){
        
        bln_Day4Check = false;
        self.Day4Check_image.image = [UIImage imageNamed:@"check_off.png"];
    }else{
        
        bln_Day4Check = true;
        self.Day4Check_image.image = [UIImage imageNamed:@"check_on.png"];
    }
}

- (IBAction)Day5Time_Push:(id)sender {
    
    if(bln_Day5Check){
        
        bln_Day5Check = false;
        self.Day5Check_image.image = [UIImage imageNamed:@"check_off.png"];
    }else{
        
        bln_Day5Check = true;
        self.Day5Check_image.image = [UIImage imageNamed:@"check_on.png"];
    }
}

- (IBAction)Day6Time_Push:(id)sender {
    
    if(bln_Day6Check){
        
        bln_Day6Check = false;
        self.Day6Check_image.image = [UIImage imageNamed:@"check_off.png"];
    }else{
        
        bln_Day6Check = true;
        self.Day6Check_image.image = [UIImage imageNamed:@"check_on.png"];
    }
}

- (IBAction)Day7Time_Push:(id)sender {
    
    if(bln_Day7Check){
        
        bln_Day7Check = false;
        self.Day7Check_image.image = [UIImage imageNamed:@"check_off.png"];
    }else{
        
        bln_Day7Check = true;
        self.Day7Check_image.image = [UIImage imageNamed:@"check_on.png"];
    }
}

- (IBAction)backButton:(id)sender {
    
    // 前画面に戻る
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)OK_Push:(id)sender {
    
    BOOL bln_check = false;
    NSString* str_startTime = @"";
    NSString* str_endTime = @"";
    long lng_count = 0;
    
    if(bln_Day1Check){
        bln_check = true;
        
        str_startTime = _str_WeekDay1TimeStart;
        str_endTime = _str_WeekDay1TimeEnd;
        
        lng_count += 1;
    }
    if(bln_Day2Check){
        bln_check = true;
        
        str_startTime = _str_WeekDay2TimeStart;
        str_endTime = _str_WeekDay2TimeEnd;
        
        lng_count += 1;
    }
    if(bln_Day3Check){
        bln_check = true;
        
        str_startTime = _str_WeekDay3TimeStart;
        str_endTime = _str_WeekDay3TimeEnd;
        
        lng_count += 1;
    }
    if(bln_Day4Check){
        bln_check = true;
        
        str_startTime = _str_WeekDay4TimeStart;
        str_endTime = _str_WeekDay4TimeEnd;
        
        lng_count += 1;
    }
    if(bln_Day5Check){
        bln_check = true;
        
        str_startTime = _str_WeekDay5TimeStart;
        str_endTime = _str_WeekDay5TimeEnd;
        
        lng_count += 1;
    }
    if(bln_Day6Check){
        bln_check = true;
        
        str_startTime = _str_WeekDay6TimeStart;
        str_endTime = _str_WeekDay6TimeEnd;
        
        lng_count += 1;
    }
    if(bln_Day7Check){
        bln_check = true;
        
        str_startTime = _str_WeekDay7TimeStart;
        str_endTime = _str_WeekDay7TimeEnd;
        
        lng_count += 1;
    }
    
    if(lng_count > 1){
        
        str_startTime = @"";
        str_endTime = @"";
    }
    
    if(bln_check){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
        BasicTimeSetting_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"BasicTimeSetting"];
        initialViewController.BasicTimeSetting_delegate = self;
        initialViewController.rootView = _rootView;
        initialViewController.secondView = _secondView;
        initialViewController.api = _api;
        
        initialViewController.bln_Day1Select = bln_Day1Check;
        initialViewController.bln_Day2Select = bln_Day2Check;
        initialViewController.bln_Day3Select = bln_Day3Check;
        initialViewController.bln_Day4Select = bln_Day4Check;
        initialViewController.bln_Day5Select = bln_Day5Check;
        initialViewController.bln_Day6Select = bln_Day6Check;
        initialViewController.bln_Day7Select = bln_Day7Check;
        
        initialViewController.str_StartTime = str_startTime;
        initialViewController.str_EndTime = str_endTime;
        
        initialViewController.str_Day1StartTime = _str_WeekDay1TimeStart;
        initialViewController.str_Day1EndTime = _str_WeekDay1TimeEnd;
        initialViewController.str_Day2StartTime = _str_WeekDay2TimeStart;
        initialViewController.str_Day2EndTime = _str_WeekDay2TimeEnd;
        initialViewController.str_Day3StartTime = _str_WeekDay3TimeStart;
        initialViewController.str_Day3EndTime = _str_WeekDay3TimeEnd;
        initialViewController.str_Day4StartTime = _str_WeekDay4TimeStart;
        initialViewController.str_Day4EndTime = _str_WeekDay4TimeEnd;
        initialViewController.str_Day5StartTime = _str_WeekDay5TimeStart;
        initialViewController.str_Day5EndTime = _str_WeekDay5TimeEnd;
        initialViewController.str_Day6StartTime = _str_WeekDay6TimeStart;
        initialViewController.str_Day6EndTime = _str_WeekDay6TimeEnd;
        initialViewController.str_Day7StartTime = _str_WeekDay7TimeStart;
        initialViewController.str_Day7EndTime = _str_WeekDay7TimeEnd;
        
        [self.navigationController pushViewController:initialViewController animated:YES];
        
    }else{
        
        [self messageError:@"" message:@"変更したい曜日が選択されていません。"];
    }
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"")
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//バックアクション
- (void)BasicTimeSetting_BackActionView {
}

//Login画面移動用
- (void)BasicTimeSetting_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        //------------------------------- 日別データ格納 -------------------------------
        NSMutableArray* ary_dt1;
        NSMutableArray* ary_dt2;
        NSMutableArray* ary_dt3;
        NSMutableArray* ary_dt4;
        NSMutableArray* ary_dt5;
        NSMutableArray* ary_dt6;
        NSMutableArray* ary_dt7;
        NSMutableArray* ary_week = [arrayData valueForKey:@"week"];
        for(long c=0;c<ary_week.count;c++){

            //日別での格納
            switch ([[[ary_week valueForKey:@"wday"] objectAtIndex:c] longValue]) {
                case 0:
                    
                    ary_dt1 = [[arrayData valueForKey:@"week"] objectAtIndex:0];
                    break;
                case 1:

                    ary_dt2 = [[arrayData valueForKey:@"week"] objectAtIndex:1];
                    break;
                case 2:
                    
                    ary_dt3 = [[arrayData valueForKey:@"week"] objectAtIndex:2];
                    break;
                case 3:
                    
                    ary_dt4 = [[arrayData valueForKey:@"week"] objectAtIndex:3];
                    break;
                case 4:
                    
                    ary_dt5 = [[arrayData valueForKey:@"week"] objectAtIndex:4];
                    break;
                case 5:
                    
                    ary_dt6 = [[arrayData valueForKey:@"week"] objectAtIndex:5];
                    break;
                case 6:
                    
                    ary_dt7 = [[arrayData valueForKey:@"week"] objectAtIndex:6];
                    break;
            }
        }

        
        _str_WeekDay1TimeStart = [ary_dt1 valueForKey:@"start_at"];
        _str_WeekDay1TimeEnd = [ary_dt1 valueForKey:@"end_at"];
        _str_WeekDay2TimeStart = [ary_dt2 valueForKey:@"start_at"];
        _str_WeekDay2TimeEnd = [ary_dt2 valueForKey:@"end_at"];
        _str_WeekDay3TimeStart = [ary_dt3 valueForKey:@"start_at"];
        _str_WeekDay3TimeEnd = [ary_dt3 valueForKey:@"end_at"];
        _str_WeekDay4TimeStart = [ary_dt4 valueForKey:@"start_at"];
        _str_WeekDay4TimeEnd = [ary_dt4 valueForKey:@"end_at"];
        _str_WeekDay5TimeStart = [ary_dt5 valueForKey:@"start_at"];
        _str_WeekDay5TimeEnd = [ary_dt5 valueForKey:@"end_at"];
        _str_WeekDay6TimeStart = [ary_dt6 valueForKey:@"start_at"];
        _str_WeekDay6TimeEnd = [ary_dt6 valueForKey:@"end_at"];
        _str_WeekDay7TimeStart = [ary_dt7 valueForKey:@"start_at"];
        _str_WeekDay7TimeEnd = [ary_dt7 valueForKey:@"end_at"];
        
        //時間表示設定
        [self setTime];
    }
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
