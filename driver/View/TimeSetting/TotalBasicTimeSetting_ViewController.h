//
//  TotalBasicTimeSetting_ViewController.h
//  driver
//
//  Created by MacServer on 2016/05/12.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "BasicTimeSetting_ViewController.h"
#import "Wheel_Callender_Library.h"

@protocol TotalBasicTimeSetting_ViewControllerDelegate <NSObject>
- (void)TotalBasicTimeSetting_LoginBackActionView;
@end

@interface TotalBasicTimeSetting_ViewController : UIViewController <ApiDelegate, BasicTimeSetting_ViewControllerDelegate> {
    
    id<TotalBasicTimeSetting_ViewControllerDelegate> _TotalBasicTimeSetting_delegate;
    UIViewController* _rootView;
    UIViewController* _secondView;
    Api* _api;
    
    //カレンダーライブラリ用
    Wheel_Callender_Library* _calHoliday;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    
    //日にちの情報
    NSMutableArray* _ary_months;
    NSMutableArray* _ary_weeks;
    NSDate* _dt_NowDate;
    NSDate* _dt_wday1;
    NSDate* _dt_wday2;
    NSDate* _dt_wday3;
    NSDate* _dt_wday4;
    NSDate* _dt_wday5;
    NSDate* _dt_wday6;
    NSDate* _dt_wday7;
    
    //勤務時間設定
    NSString* _str_WeekDay1TimeStart;
    NSString* _str_WeekDay1TimeEnd;
    NSString* _str_WeekDay2TimeStart;
    NSString* _str_WeekDay2TimeEnd;
    NSString* _str_WeekDay3TimeStart;
    NSString* _str_WeekDay3TimeEnd;
    NSString* _str_WeekDay4TimeStart;
    NSString* _str_WeekDay4TimeEnd;
    NSString* _str_WeekDay5TimeStart;
    NSString* _str_WeekDay5TimeEnd;
    NSString* _str_WeekDay6TimeStart;
    NSString* _str_WeekDay6TimeEnd;
    NSString* _str_WeekDay7TimeStart;
    NSString* _str_WeekDay7TimeEnd;
    
    //チェック状態
    BOOL bln_Day1Check;
    BOOL bln_Day2Check;
    BOOL bln_Day3Check;
    BOOL bln_Day4Check;
    BOOL bln_Day5Check;
    BOOL bln_Day6Check;
    BOOL bln_Day7Check;
}
@property (nonatomic) id<TotalBasicTimeSetting_ViewControllerDelegate> TotalBasicTimeSetting_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) UIViewController* secondView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
- (IBAction)LeftButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *Day1Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Week_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7TimeFrom_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7TimeTo_lbl;

@property (weak, nonatomic) IBOutlet UIImageView *Day1Check_image;
@property (weak, nonatomic) IBOutlet UIImageView *Day2Check_image;
@property (weak, nonatomic) IBOutlet UIImageView *Day3Check_image;
@property (weak, nonatomic) IBOutlet UIImageView *Day4Check_image;
@property (weak, nonatomic) IBOutlet UIImageView *Day5Check_image;
@property (weak, nonatomic) IBOutlet UIImageView *Day6Check_image;
@property (weak, nonatomic) IBOutlet UIImageView *Day7Check_image;

- (IBAction)OneWeekSelect_Push:(id)sender;
- (IBAction)OneWeekUnSelect_Push:(id)sender;

- (IBAction)Day1Time_Push:(id)sender;
- (IBAction)Day2Time_Push:(id)sender;
- (IBAction)Day3Time_Push:(id)sender;
- (IBAction)Day4Time_Push:(id)sender;
- (IBAction)Day5Time_Push:(id)sender;
- (IBAction)Day6Time_Push:(id)sender;
- (IBAction)Day7Time_Push:(id)sender;

- (IBAction)backButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *OK_Push_lbl;
- (IBAction)OK_Push:(id)sender;

@end
