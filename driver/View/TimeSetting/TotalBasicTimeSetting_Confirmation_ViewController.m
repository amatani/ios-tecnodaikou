//
//  TotalBasicTimeSetting_Confirmation_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2015/12/27.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "TotalBasicTimeSetting_Confirmation_ViewController.h"
#import "MainMenu_ViewController.h"

@interface TotalBasicTimeSetting_Confirmation_ViewController ()
@end

@implementation TotalBasicTimeSetting_Confirmation_ViewController

@synthesize TotalBasicTimeSetting_Confirmation_delegate = _TotalBasicTimeSetting_Confirmation_delegate;
@synthesize rootView = _rootView;
@synthesize secondView = _secondView;
@synthesize api = _api;

@synthesize str_Day1StartTime = _str_Day1StartTime;
@synthesize str_Day1EndTime = _str_Day1EndTime;
@synthesize str_Day2StartTime = _str_Day2StartTime;
@synthesize str_Day2EndTime = _str_Day2EndTime;
@synthesize str_Day3StartTime = _str_Day3StartTime;
@synthesize str_Day3EndTime = _str_Day3EndTime;
@synthesize str_Day4StartTime = _str_Day4StartTime;
@synthesize str_Day4EndTime = _str_Day4EndTime;
@synthesize str_Day5StartTime = _str_Day5StartTime;
@synthesize str_Day5EndTime = _str_Day5EndTime;
@synthesize str_Day6StartTime = _str_Day6StartTime;
@synthesize str_Day6EndTime = _str_Day6EndTime;
@synthesize str_Day7StartTime = _str_Day7StartTime;
@synthesize str_Day7EndTime = _str_Day7EndTime;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIFont *font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/13];
    self.Day1Week_lbl.font = font;
    self.Day2Week_lbl.font = font;
    self.Day3Week_lbl.font = font;
    self.Day4Week_lbl.font = font;
    self.Day5Week_lbl.font = font;
    self.Day6Week_lbl.font = font;
    self.Day7Week_lbl.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W3" size:[Configuration getScreenWidth]/30];
    self.Day1TimeFrom_lbl.font = font;
    self.Day2TimeFrom_lbl.font = font;
    self.Day3TimeFrom_lbl.font = font;
    self.Day4TimeFrom_lbl.font = font;
    self.Day5TimeFrom_lbl.font = font;
    self.Day6TimeFrom_lbl.font = font;
    self.Day7TimeFrom_lbl.font = font;
    self.Day1TimeTo_lbl.font = font;
    self.Day2TimeTo_lbl.font = font;
    self.Day3TimeTo_lbl.font = font;
    self.Day4TimeTo_lbl.font = font;
    self.Day5TimeTo_lbl.font = font;
    self.Day6TimeTo_lbl.font = font;
    self.Day7TimeTo_lbl.font = font;
    
    //カレンダーライブラリ用初期化
    _calHoliday = [[Wheel_Callender_Library alloc]init];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    //時間設定
    [self setTime];
}

//初期値の設定
- (void)setFirstValue {
    
    //値の初期値設定
    _str_Day1StartTime = @"";
    _str_Day1EndTime = @"";
    _str_Day2StartTime = @"";
    _str_Day2EndTime = @"";
    _str_Day3StartTime = @"";
    _str_Day3EndTime = @"";
    _str_Day4StartTime = @"";
    _str_Day4EndTime = @"";
    _str_Day5StartTime = @"";
    _str_Day5EndTime = @"";
    _str_Day6StartTime = @"";
    _str_Day6EndTime = @"";
    _str_Day7StartTime = @"";
    _str_Day7EndTime = @"";
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

//時間の設定
- (void)setTime {
    
    if([_str_Day1StartTime isEqualToString:@""]){
        self.Day1TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day1StartTime];
        self.Day1TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_Day1EndTime isEqualToString:@""]){
        self.Day1TimeTo_lbl.text = @"未定";
    }else if([_str_Day1EndTime isEqualToString:@"08:00"]){
        self.Day1TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day1EndTime];
        self.Day1TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_Day2StartTime isEqualToString:@""]){
        self.Day2TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day2StartTime];
        self.Day2TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_Day2EndTime isEqualToString:@""]){
        self.Day2TimeTo_lbl.text = @"未定";
    }else if([_str_Day2EndTime isEqualToString:@"08:00"]){
        self.Day2TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day2EndTime];
        self.Day2TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_Day3StartTime isEqualToString:@""]){
        self.Day3TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day3StartTime];
        self.Day3TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_Day3EndTime isEqualToString:@""]){
        self.Day3TimeTo_lbl.text = @"未定";
    }else if([_str_Day3EndTime isEqualToString:@"08:00"]){
        self.Day3TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day3EndTime];
        self.Day3TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_Day4StartTime isEqualToString:@""]){
        self.Day4TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day4StartTime];
        self.Day4TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_Day4EndTime isEqualToString:@""]){
        self.Day4TimeTo_lbl.text = @"未定";
    }else if([_str_Day4EndTime isEqualToString:@"08:00"]){
        self.Day4TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day4EndTime];
        self.Day4TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_Day5StartTime isEqualToString:@""]){
        self.Day5TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day5StartTime];
        self.Day5TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_Day5EndTime isEqualToString:@""]){
        self.Day5TimeTo_lbl.text = @"未定";
    }else if([_str_Day5EndTime isEqualToString:@"08:00"]){
        self.Day5TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day5EndTime];
        self.Day5TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_Day6StartTime isEqualToString:@""]){
        self.Day6TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day6StartTime];
        self.Day6TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_Day6EndTime isEqualToString:@""]){
        self.Day6TimeTo_lbl.text = @"未定";
    }else if([_str_Day6EndTime isEqualToString:@"08:00"]){
        self.Day6TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day6EndTime];
        self.Day6TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_Day7StartTime isEqualToString:@""]){
        self.Day7TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day7StartTime];
        self.Day7TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_Day7EndTime isEqualToString:@""]){
        self.Day7TimeTo_lbl.text = @"未定";
    }else if([_str_Day7EndTime isEqualToString:@"08:00"]){
        self.Day7TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_Day7EndTime];
        self.Day7TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
}

- (NSString *)createStringAddedCommaFromInt:(long)number
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    [format setGroupingSeparator:@","];
    [format setGroupingSize:3];
    
    return [format stringForObjectValue:[NSNumber numberWithLong:number]];
}

- (IBAction)LeftButton:(id)sender {
    
    // メイン画面に戻る
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:nil
                                        message:@"今週の予定を確定させていません。このままホームに移動しますか？"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                
                                                // メイン画面に戻る
                                                [self.navigationController popToViewController:_rootView animated:YES];
                                            }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)getOverTime:(NSDate*)dt {
    
    //指定日の０時を取得
    NSString* dt_dateString = [NSString stringWithFormat:@"%ld-%ld-%ld 00:00:00", [_calHoliday yearNoGet:dt], [_calHoliday monthNoGet:dt], [_calHoliday dayNoGet:dt]];
    
    NSDateFormatter* dt_formatter = [[NSDateFormatter alloc] init];
    [dt_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //12時間表記対策
    [dt_formatter setLocale:[NSLocale systemLocale]];
    //タイムゾーンの指定
    [dt_formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
    NSDate *dt_date = [dt_formatter dateFromString:dt_dateString];
    
    //８時間追加
    NSDate *re_dt_date = [dt_date initWithTimeInterval:8*60*60 sinceDate:dt_date];
    
    //タイムゾーン含めた現在時間
    NSDate *now_dt = [NSDate dateWithTimeIntervalSinceNow:(60*60*9)];
    NSLog(@"現在： %@　ー　区切り日時:%@", now_dt, re_dt_date);
    
    NSComparisonResult result = [re_dt_date compare:now_dt];
    switch (result) {
        case NSOrderedAscending:
            // 過去
            NSLog(@"NSOrderedAscending");
            return false;
            break;
        case NSOrderedDescending:
            // 未来日
            NSLog(@"NSOrderedDescending");
            return true;
            break;
        case NSOrderedSame:
            // 当日
            NSLog(@"NSOrderedSame");
            return true;
            break;
        default:
            return false;
            break;
    }
}

- (IBAction)backButton:(id)sender {
    
    //値を更新させないように戻す
    [_TotalBasicTimeSetting_Confirmation_delegate TotalBasicTimeSetting_Confirmation_CanselBack];
    
    // 前画面に戻る
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)OK_Push:(id)sender {
    
    //値のセット
    NSMutableArray *root_bodypaper = [[NSMutableArray alloc] init];
    NSMutableDictionary *bodypaper1 = [[NSMutableDictionary alloc] init];
    [bodypaper1 setValue:[NSNumber numberWithLong:0] forKey:@"wday"];
    if([_str_Day1StartTime isEqualToString:@""]){
        [bodypaper1 setValue:[NSNull null] forKey:@"start_at"];
    }else{
        [bodypaper1 setValue:_str_Day1StartTime forKey:@"start_at"];
    }
    if([_str_Day1EndTime isEqualToString:@""]){
        [bodypaper1 setValue:[NSNull null] forKey:@"end_at"];
    }else{
        [bodypaper1 setValue:_str_Day1EndTime forKey:@"end_at"];
    }
    
    NSMutableDictionary *bodypaper2 = [[NSMutableDictionary alloc] init];
    [bodypaper2 setValue:[NSNumber numberWithLong:1] forKey:@"wday"];
    if([_str_Day2StartTime isEqualToString:@""]){
        [bodypaper2 setValue:[NSNull null] forKey:@"start_at"];
    }else{
        [bodypaper2 setValue:_str_Day2StartTime forKey:@"start_at"];
    }
    if([_str_Day2EndTime isEqualToString:@""]){
        [bodypaper2 setValue:[NSNull null] forKey:@"end_at"];
    }else{
        [bodypaper2 setValue:_str_Day2EndTime forKey:@"end_at"];
    }
    
    NSMutableDictionary *bodypaper3 = [[NSMutableDictionary alloc] init];
    [bodypaper3 setValue:[NSNumber numberWithLong:2] forKey:@"wday"];
    if([_str_Day3StartTime isEqualToString:@""]){
        [bodypaper3 setValue:[NSNull null] forKey:@"start_at"];
    }else{
        [bodypaper3 setValue:_str_Day3StartTime forKey:@"start_at"];
    }
    if([_str_Day3EndTime isEqualToString:@""]){
        [bodypaper3 setValue:[NSNull null] forKey:@"end_at"];
    }else{
        [bodypaper3 setValue:_str_Day3EndTime forKey:@"end_at"];
    }
    
    NSMutableDictionary *bodypaper4 = [[NSMutableDictionary alloc] init];
    [bodypaper4 setValue:[NSNumber numberWithLong:3] forKey:@"wday"];
    if([_str_Day4StartTime isEqualToString:@""]){
        [bodypaper4 setValue:[NSNull null] forKey:@"start_at"];
    }else{
        [bodypaper4 setValue:_str_Day4StartTime forKey:@"start_at"];
    }
    if([_str_Day4EndTime isEqualToString:@""]){
        [bodypaper4 setValue:[NSNull null] forKey:@"end_at"];
    }else{
        [bodypaper4 setValue:_str_Day4EndTime forKey:@"end_at"];
    }
    
    NSMutableDictionary *bodypaper5 = [[NSMutableDictionary alloc] init];
    [bodypaper5 setValue:[NSNumber numberWithLong:4] forKey:@"wday"];
    if([_str_Day5StartTime isEqualToString:@""]){
        [bodypaper5 setValue:[NSNull null] forKey:@"start_at"];
    }else{
        [bodypaper5 setValue:_str_Day5StartTime forKey:@"start_at"];
    }
    if([_str_Day5EndTime isEqualToString:@""]){
        [bodypaper5 setValue:[NSNull null] forKey:@"end_at"];
    }else{
        [bodypaper5 setValue:_str_Day5EndTime forKey:@"end_at"];
    }
    
    NSMutableDictionary *bodypaper6 = [[NSMutableDictionary alloc] init];
    [bodypaper6 setValue:[NSNumber numberWithLong:5] forKey:@"wday"];
    if([_str_Day6StartTime isEqualToString:@""]){
        [bodypaper6 setValue:[NSNull null] forKey:@"start_at"];
    }else{
        [bodypaper6 setValue:_str_Day6StartTime forKey:@"start_at"];
    }
    if([_str_Day6EndTime isEqualToString:@""]){
        [bodypaper6 setValue:[NSNull null] forKey:@"end_at"];
    }else{
        [bodypaper6 setValue:_str_Day6EndTime forKey:@"end_at"];
    }
    
    NSMutableDictionary *bodypaper7 = [[NSMutableDictionary alloc] init];
    [bodypaper7 setValue:[NSNumber numberWithLong:6] forKey:@"wday"];
    if([_str_Day7StartTime isEqualToString:@""]){
        [bodypaper7 setValue:[NSNull null] forKey:@"start_at"];
    }else{
        [bodypaper7 setValue:_str_Day7StartTime forKey:@"start_at"];
    }
    if([_str_Day7EndTime isEqualToString:@""]){
        [bodypaper7 setValue:[NSNull null] forKey:@"end_at"];
    }else{
        [bodypaper7 setValue:_str_Day7EndTime forKey:@"end_at"];
    }
    
    [root_bodypaper addObject:bodypaper1];
    [root_bodypaper addObject:bodypaper2];
    [root_bodypaper addObject:bodypaper3];
    [root_bodypaper addObject:bodypaper4];
    [root_bodypaper addObject:bodypaper5];
    [root_bodypaper addObject:bodypaper6];
    [root_bodypaper addObject:bodypaper7];
    
    //----------------------- 送信するパラメータの組み立て(確認画面用) ------------------------
    NSMutableDictionary *mutableDic_Confirmation = [NSMutableDictionary dictionary];
    [mutableDic_Confirmation setValue:root_bodypaper forKey:@"week"];
    
    //クエリパラメータ追加（BOOL型で直接は送れない）
    [mutableDic_Confirmation setValue:[Configuration getSessionTokenKey] forKey:@"token"];
    
    
    NSError *error = nil;
    data_Confirmation = nil;
    NSString *str_body;
    if([NSJSONSerialization isValidJSONObject:mutableDic_Confirmation]){
        data_Confirmation = [NSJSONSerialization dataWithJSONObject:mutableDic_Confirmation options:NSJSONWritingPrettyPrinted error:&error];
        str_body = [[NSString alloc] initWithData:data_Confirmation encoding:NSUTF8StringEncoding];
        NSLog(@"%@",str_body);
    }
    
    [_api Api_BasicTimeWeekSetting:self week_json:data_Confirmation];
}

-(void)messageError:(NSString*)typeName
              Title:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if([typeName isEqualToString:@""]){
        
        //OKのみのシングルアクション
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
    }else if([typeName isEqualToString:@"OK"]){
        
        //保存完了アクション
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self okSaveButtonPushed];
        }]];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)okSaveButtonPushed {
    // 保存完了後のアラートアクション
    // 前画面に戻る
    [self.navigationController popToViewController:_secondView animated:NO];
}

//バックアクション
- (void)BasicTimeSetting_BackActionView {
}

//Login画面移動用
- (void)BasicTimeSetting_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {

    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        [self messageError:NSLocalizedString(@"Dialog_Ok",@"") Title:@"" message:@"変更されました。"];
        
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //リトライ
                                                        [_api Api_BasicTimeWeekSetting:self week_json:data_Confirmation];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_422"]){
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@""
                                                message:@"この時間は選択できません。 正しい時間を入力してください。"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
