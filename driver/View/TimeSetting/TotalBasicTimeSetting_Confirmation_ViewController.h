//
//  TotalBasicTimeSetting_Confirmation.h
//  driver
//
//  Created by MacServer on 2016/05/12.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "Wheel_Callender_Library.h"

@protocol TotalBasicTimeSetting_Confirmation_ViewControllerDelegate <NSObject>
- (void)TotalBasicTimeSetting_Confirmation_CanselBack;
- (void)TotalBasicTimeSetting_Confirmation_LoginBackActionView;
@end

@interface TotalBasicTimeSetting_Confirmation_ViewController : UIViewController <ApiDelegate> {
    
    id<TotalBasicTimeSetting_Confirmation_ViewControllerDelegate> _TotalBasicTimeSetting_Confirmation_delegate;
    UIViewController* _rootView;
    UIViewController* _secondView;
    Api* _api;
    
    //カレンダーライブラリ用
    Wheel_Callender_Library* _calHoliday;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    
    //変更前時間
    NSString* _str_Day1StartTime;
    NSString* _str_Day1EndTime;
    NSString* _str_Day2StartTime;
    NSString* _str_Day2EndTime;
    NSString* _str_Day3StartTime;
    NSString* _str_Day3EndTime;
    NSString* _str_Day4StartTime;
    NSString* _str_Day4EndTime;
    NSString* _str_Day5StartTime;
    NSString* _str_Day5EndTime;
    NSString* _str_Day6StartTime;
    NSString* _str_Day6EndTime;
    NSString* _str_Day7StartTime;
    NSString* _str_Day7EndTime;
    
    //API保存用データ格納
    NSData* data_Confirmation;
}
@property (nonatomic) id<TotalBasicTimeSetting_Confirmation_ViewControllerDelegate> TotalBasicTimeSetting_Confirmation_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) UIViewController* secondView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
//変更前時間
@property (nonatomic) NSString* str_Day1StartTime;
@property (nonatomic) NSString* str_Day1EndTime;
@property (nonatomic) NSString* str_Day2StartTime;
@property (nonatomic) NSString* str_Day2EndTime;
@property (nonatomic) NSString* str_Day3StartTime;
@property (nonatomic) NSString* str_Day3EndTime;
@property (nonatomic) NSString* str_Day4StartTime;
@property (nonatomic) NSString* str_Day4EndTime;
@property (nonatomic) NSString* str_Day5StartTime;
@property (nonatomic) NSString* str_Day5EndTime;
@property (nonatomic) NSString* str_Day6StartTime;
@property (nonatomic) NSString* str_Day6EndTime;
@property (nonatomic) NSString* str_Day7StartTime;
@property (nonatomic) NSString* str_Day7EndTime;

- (IBAction)LeftButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *Day1Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Week_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7TimeFrom_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7TimeTo_lbl;

- (IBAction)backButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *OK_Push_lbl;
- (IBAction)OK_Push:(id)sender;

@end
