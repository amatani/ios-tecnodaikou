//
//  BasicTimeSetting_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/26.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "TotalBasicTimeSetting_Confirmation_ViewController.h"
#import "Wheel_Callender_Library.h"

@protocol BasicTimeSetting_ViewControllerDelegate <NSObject>
- (void)BasicTimeSetting_LoginBackActionView;
- (void)BasicTimeSetting_BackActionView;
@end

@interface BasicTimeSetting_ViewController : UIViewController <ApiDelegate ,TotalBasicTimeSetting_Confirmation_ViewControllerDelegate> {
    id<BasicTimeSetting_ViewControllerDelegate> _BasicTimeSetting_delegate;
    UIViewController* _rootView;
    UIViewController* _secondView;
    Api* _api;
    
    //カレンダーライブラリ用
    Wheel_Callender_Library* _calHoliday;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //選択時間保存用
    NSMutableArray* _array_StartTime;
    NSMutableArray* _array_EndTime;
    
    //開始終了のフラグセット（０＝開始、１＝終了）
    long _lng_TimeSetFlg;
    
    //入力フィールド用
    UITextField *_Set_TextField;
    
    //テキストフォーカス位置保存用
    UITextRange* _uiTextRange_TextFiledForcusCount;
    NSRange _range_carettoDelFlg;
    
    //AMPM変更フラグ
    BOOL _bln_StartPmFLG;
    BOOL _bln_EndPmFLG;
    
    //時間未定、最終フラグ
    BOOL _Start_NotTimeFLG;
    BOOL _End_LastTimeFLG;
    
    //定時、自由での出社時間扱い設定
    BOOL _bln_ScheduleKind;
    
    //確認から戻った時の再セット無効化フラグ
    BOOL _bln_reloadFlg;
    
    //時間変更の選択
    BOOL _bln_Day1Select;
    BOOL _bln_Day2Select;
    BOOL _bln_Day3Select;
    BOOL _bln_Day4Select;
    BOOL _bln_Day5Select;
    BOOL _bln_Day6Select;
    BOOL _bln_Day7Select;
    
    //変更前時間
    NSString* _str_StartTime;
    NSString* _str_EndTime;
    NSString* _str_Day1StartTime;
    NSString* _str_Day1EndTime;
    NSString* _str_Day2StartTime;
    NSString* _str_Day2EndTime;
    NSString* _str_Day3StartTime;
    NSString* _str_Day3EndTime;
    NSString* _str_Day4StartTime;
    NSString* _str_Day4EndTime;
    NSString* _str_Day5StartTime;
    NSString* _str_Day5EndTime;
    NSString* _str_Day6StartTime;
    NSString* _str_Day6EndTime;
    NSString* _str_Day7StartTime;
    NSString* _str_Day7EndTime;
    
    //API保存用データ格納
    NSData* data_Confirmation;
}

@property (nonatomic) id<BasicTimeSetting_ViewControllerDelegate> BasicTimeSetting_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) UIViewController* secondView;
@property (nonatomic) Api* api;

@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;
@property (weak, nonatomic) IBOutlet UIView *ScrollInView;

//引き継がれるパラメータ
//時間変更の選択
@property (nonatomic) BOOL bln_Day1Select;
@property (nonatomic) BOOL bln_Day2Select;
@property (nonatomic) BOOL bln_Day3Select;
@property (nonatomic) BOOL bln_Day4Select;
@property (nonatomic) BOOL bln_Day5Select;
@property (nonatomic) BOOL bln_Day6Select;
@property (nonatomic) BOOL bln_Day7Select;

//変更前時間
@property (nonatomic) NSString* str_StartTime;
@property (nonatomic) NSString* str_EndTime;
@property (nonatomic) NSString* str_Day1StartTime;
@property (nonatomic) NSString* str_Day1EndTime;
@property (nonatomic) NSString* str_Day2StartTime;
@property (nonatomic) NSString* str_Day2EndTime;
@property (nonatomic) NSString* str_Day3StartTime;
@property (nonatomic) NSString* str_Day3EndTime;
@property (nonatomic) NSString* str_Day4StartTime;
@property (nonatomic) NSString* str_Day4EndTime;
@property (nonatomic) NSString* str_Day5StartTime;
@property (nonatomic) NSString* str_Day5EndTime;
@property (nonatomic) NSString* str_Day6StartTime;
@property (nonatomic) NSString* str_Day6EndTime;
@property (nonatomic) NSString* str_Day7StartTime;
@property (nonatomic) NSString* str_Day7EndTime;

@property (weak, nonatomic) IBOutlet UILabel *Start_AmPmSelect_lbl;
@property (weak, nonatomic) IBOutlet UILabel *End_AmPmSelect_lbl;
@property (weak, nonatomic) IBOutlet UIButton *Start_AmPm_Push;
@property (weak, nonatomic) IBOutlet UIButton *End_AmPm_Push;

@property (weak, nonatomic) IBOutlet UITextField *Start_Time_txt;
@property (weak, nonatomic) IBOutlet UITextField *End_Time_txt;

@property (weak, nonatomic) IBOutlet UILabel *Start_NotTime_lbl;
@property (weak, nonatomic) IBOutlet UILabel *End_LastTime_lbl;
@property (weak, nonatomic) IBOutlet UIView *Start_NotTitleBack_view;
@property (weak, nonatomic) IBOutlet UIView *End_LastTimeBack_view;

@property (weak, nonatomic) IBOutlet UIView *Start_NotTime_view;

- (IBAction)Start_AmPm_Push:(id)sender;
- (IBAction)End_AmPm_Push:(id)sender;

- (IBAction)NotTime_Push:(id)sender;
- (IBAction)LastTime_Push:(id)sender;

- (IBAction)Cansel_push:(id)sender;
- (IBAction)Ok_push:(id)sender;

@end
