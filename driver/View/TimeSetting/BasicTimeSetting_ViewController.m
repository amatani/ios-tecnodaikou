//
//  BasicTimeSetting_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2016/01/26.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "BasicTimeSetting_ViewController.h"

@interface BasicTimeSetting_ViewController ()
@end

@implementation BasicTimeSetting_ViewController

@synthesize BasicTimeSetting_delegate = _BasicTimeSetting_delegate;
@synthesize rootView = _rootView;
@synthesize secondView = _secondView;
@synthesize api = _api;
@synthesize bln_Day1Select = _bln_Day1Select;
@synthesize bln_Day2Select = _bln_Day2Select;
@synthesize bln_Day3Select = _bln_Day3Select;
@synthesize bln_Day4Select = _bln_Day4Select;
@synthesize bln_Day5Select = _bln_Day5Select;
@synthesize bln_Day6Select = _bln_Day6Select;
@synthesize bln_Day7Select = _bln_Day7Select;

@synthesize str_StartTime = _str_StartTime;
@synthesize str_EndTime = _str_EndTime;

@synthesize str_Day1StartTime = _str_Day1StartTime;
@synthesize str_Day1EndTime = _str_Day1EndTime;
@synthesize str_Day2StartTime = _str_Day2StartTime;
@synthesize str_Day2EndTime = _str_Day2EndTime;
@synthesize str_Day3StartTime = _str_Day3StartTime;
@synthesize str_Day3EndTime = _str_Day3EndTime;
@synthesize str_Day4StartTime = _str_Day4StartTime;
@synthesize str_Day4EndTime = _str_Day4EndTime;
@synthesize str_Day5StartTime = _str_Day5StartTime;
@synthesize str_Day5EndTime = _str_Day5EndTime;
@synthesize str_Day6StartTime = _str_Day6StartTime;
@synthesize str_Day6EndTime = _str_Day6EndTime;
@synthesize str_Day7StartTime = _str_Day7StartTime;
@synthesize str_Day7EndTime = _str_Day7EndTime;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //カレンダーライブラリ用初期化
    _calHoliday = [[Wheel_Callender_Library alloc]init];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
    
    //定時、自由での出社時間扱い設定(0:定時 1:自由)
    long lng_Schedulekind = [Configuration getScheduleKind];
    if(lng_Schedulekind == 0){
        
        //定時
        _bln_ScheduleKind = false;
        
        self.Start_NotTime_view.hidden = true;
        
    }else if(lng_Schedulekind == 1){
        
        //自由
        _bln_ScheduleKind = true;
        
        self.Start_NotTime_view.hidden = false;
    }
    
    UIFont *font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/10];
    self.Start_Time_txt.font = font;
    self.End_Time_txt.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W3" size:[Configuration getScreenWidth]/24];
    self.Start_NotTime_lbl.font = font;
    self.End_LastTime_lbl.font = font;
    
    //確認から戻った時の再セット無効化フラグ
    _bln_reloadFlg = true;
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    //初期値の設定
    if(_bln_reloadFlg == true){
        
        //初期値設定
        //▶開始時間設定
        NSArray* ary_StartAm = [_calHoliday GetTime12:_str_StartTime];
        self.Start_Time_txt.text = [ary_StartAm objectAtIndex:1];
        
        long lng_TotalSet = 0;
        if(_bln_Day1Select){
            lng_TotalSet += 1;
        }
        if(_bln_Day2Select){
            lng_TotalSet += 1;
        }
        if(_bln_Day3Select){
            lng_TotalSet += 1;
        }
        if(_bln_Day4Select){
            lng_TotalSet += 1;
        }
        if(_bln_Day5Select){
            lng_TotalSet += 1;
        }
        if(_bln_Day6Select){
            lng_TotalSet += 1;
        }
        if(_bln_Day7Select){
            lng_TotalSet += 1;
        }
        if(lng_TotalSet == 1){
            
            //一個の日が選択されている場合
            if([self.Start_Time_txt.text isEqualToString:@""]){
                
                //時間設定無い場合
                _bln_StartPmFLG = false;
                
                //開始時間を未定設定
                _Start_NotTimeFLG = false;
            }else{
                
                //時間設定ある場合
                if([[ary_StartAm objectAtIndex:0] isEqualToString:@"AM"]){
                    
                    //午前指定
                    _bln_StartPmFLG = false;
                    
                }else if([[ary_StartAm objectAtIndex:0] isEqualToString:@"PM"]){
                    
                    //午後指定
                    _bln_StartPmFLG = true;
                }
            }
        }else{
            
            //それ以外が選択されている場合
            //午前指定
            _bln_StartPmFLG = false;
            
            //開始時間を未定設定
            _Start_NotTimeFLG = false;
        }
        

        
        //AMPM設定
        [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
        
        //時間未定ボタン色セット
        [self setNotTime];
        
        //▶終了時間設定
        NSArray* ary_EndAm = [_calHoliday GetTime12:_str_EndTime];
        self.End_Time_txt.text = [ary_EndAm objectAtIndex:1];
        
        if([self.End_Time_txt.text isEqualToString:@""]){
            
            //午前指定
            _bln_EndPmFLG = false;
            
        }else{
            
            if([[ary_EndAm objectAtIndex:0] isEqualToString:@"AM"]){
                
                if([self.End_Time_txt.text isEqualToString:@"08:00"]){
                    
                    //最終指定
                    _End_LastTimeFLG = true;
                }
                
                //午前指定
                _bln_EndPmFLG = false;
                
            }else if([[ary_EndAm objectAtIndex:0] isEqualToString:@"PM"]){
                
                //午後指定
                _bln_EndPmFLG = true;
            }
        }
        
        //AMPM設定
        [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
        
        //最終ボタン色セット
        [self setLastTime];
        
        
        
        
        //選択可能時間取得
        [_api Api_BasicPeriodTimeGetting:self];
        
        //ツールバーを生成（開始時間設定用）StartTime_toolBar_frontaction
        UIToolbar *StartTime_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
        StartTime_Text_toolBar.barStyle = UIBarStyleDefault;
        [StartTime_Text_toolBar sizeToFit];
        UIBarButtonItem *StartTime_Text_frontaction = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:101 target:self action:@selector(StartTime_toolBar_frontaction:)];
        UIBarButtonItem *StartTime_Text_backaction = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:102 target:self action:@selector(StartTime_toolBar_backaction:)];
        UIBarButtonItem *StartTime_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        UIBarButtonItem *_StartTime_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(StartTime_Text_toolBar_closeKeyboard:)];
        NSArray *StartTime_Text_toolBarItems = [NSArray arrayWithObjects:StartTime_Text_frontaction, StartTime_Text_backaction, StartTime_Text_spacer, _StartTime_Text_commitBtn, nil];
        [StartTime_Text_toolBar setItems:StartTime_Text_toolBarItems animated:YES];
        // ToolbarをTextViewのinputAccessoryViewに設定
        self.Start_Time_txt.inputAccessoryView = StartTime_Text_toolBar;
        //入力後のイベント設定
        [self.Start_Time_txt addTarget:self action:@selector(TextfieldChengeOver:) forControlEvents:UIControlEventEditingChanged];
        
        //ツールバーを生成（終了時間設定用）
        UIToolbar *EndTime_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
        EndTime_Text_toolBar.barStyle = UIBarStyleDefault;
        [EndTime_Text_toolBar sizeToFit];
        UIBarButtonItem *EndTime_Text_frontaction = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:101 target:self action:@selector(EndTime_toolBar_frontaction:)];
        UIBarButtonItem *EndTime_Text_backaction = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:102 target:self action:@selector(EndTime_toolBar_backaction:)];
        UIBarButtonItem *EndTime_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        UIBarButtonItem *_EndTime_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(EndTime_Text_toolBar_closeKeyboard:)];
        NSArray *EndTime_Text_toolBarItems = [NSArray arrayWithObjects:EndTime_Text_frontaction, EndTime_Text_backaction, EndTime_Text_spacer, _EndTime_Text_commitBtn, nil];
        [EndTime_Text_toolBar setItems:EndTime_Text_toolBarItems animated:YES];
        // ToolbarをTextViewのinputAccessoryViewに設定
        self.End_Time_txt.inputAccessoryView = EndTime_Text_toolBar;
        //入力後のイベント設定
        [self.End_Time_txt addTarget:self action:@selector(TextfieldChengeOver:) forControlEvents:UIControlEventEditingChanged];
        
        //テキストフィールド入力時の起動メソッド設定
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        
    }else{
        
        _bln_reloadFlg = true;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //スクロールを制限
    if(scrollView.contentOffset.y < 0){
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
    }
}

/////////////// ↓ テキスト入力関連 ↓ ////////////////////
//コピーペースト禁止
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController) {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //スペースは入力不可
    if (![string compare:@" "]) {
        return NO;
    }
    
    //削除処理確認用
    _range_carettoDelFlg = range;
    
    //編集前と編集中をくっつけて５文字以内チェック
    BOOL isLengthLimit;
    NSMutableString *strM_Time =[textField.text mutableCopy];
    [strM_Time replaceCharactersInRange:range withString:string];
    //入力桁数判断
    isLengthLimit = [strM_Time lengthOfBytesUsingEncoding:NSShiftJISStringEncoding] < 6;
    
    if([strM_Time lengthOfBytesUsingEncoding:NSShiftJISStringEncoding] == 5){
        
        NSString* str_hour = [strM_Time substringToIndex:2];
        
        //00:00 AM設定
        if([str_hour isEqualToString:@"00"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = false;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = false;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //01:00 AM設定
        if([str_hour isEqualToString:@"01"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = false;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = false;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //02:00 AM設定
        if([str_hour isEqualToString:@"02"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = false;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = false;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //03:00 AM設定
        if([str_hour isEqualToString:@"03"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = false;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = false;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //04:00 AM設定
        if([str_hour isEqualToString:@"04"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //05:00 AM設定
        if([str_hour isEqualToString:@"05"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //06:00 AM設定
        if([str_hour isEqualToString:@"06"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //07:00 AM設定
        if([str_hour isEqualToString:@"07"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //08:00 AM設定
        if([str_hour isEqualToString:@"08"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //09:00 AM設定
        if([str_hour isEqualToString:@"09"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //10:00 AM設定
        if([str_hour isEqualToString:@"10"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        //11:00 AM設定
        if([str_hour isEqualToString:@"11"]){
            if(textField == self.Start_Time_txt){
                
                _bln_StartPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
            }
            if(textField == self.End_Time_txt){
                
                _bln_EndPmFLG = true;
                //AMPM設定
                [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
            }
        }
        
        
    }
    
    return isLengthLimit;
}


-(void)TextfieldChengeOver:(UITextField*)textField{
    
    //フォーカス位置保存
    _uiTextRange_TextFiledForcusCount = textField.selectedTextRange;
    
    NSMutableString *strM_Time =[textField.text mutableCopy];
    
    //時間文字列削除
    [strM_Time replaceOccurrencesOfString:@":" withString:@"" options:0 range:NSMakeRange(0,[strM_Time length])];
    
    NSString* str_Hour = @"";
    NSString* str_Minuite = @"";
    NSString* str_time = @" ";
    
    //入力可能内での制御
    switch (strM_Time.length) {
        case 0:
            str_Hour = @"";
            str_Minuite = @"";
            str_time = @"";
            break;
        case 1:
            str_Hour = [strM_Time substringWithRange:NSMakeRange(0,1)];
            str_Minuite = @"";
            str_time = [NSString stringWithFormat:@"%@", str_Hour];
            break;
        case 2:
            str_Hour = [strM_Time substringWithRange:NSMakeRange(0,2)];
            str_Minuite = @"";
            str_time = [NSString stringWithFormat:@"%@", str_Hour];
            break;
        case 3:
            str_Hour = [strM_Time substringWithRange:NSMakeRange(0,2)];
            str_Minuite = [strM_Time substringWithRange:NSMakeRange(2,1)];
            str_time = [NSString stringWithFormat:@"%@:%@", str_Hour, str_Minuite];
            break;
        case 4:
            str_Hour = [strM_Time substringWithRange:NSMakeRange(0,2)];
            str_Minuite = [strM_Time substringWithRange:NSMakeRange(2,2)];
            str_time = [NSString stringWithFormat:@"%@:%@", str_Hour, str_Minuite];
            break;
        default:
            break;
    }
    
    textField.text = str_time;
    
    //キャレット位置設定
    if(_range_carettoDelFlg.length == 0){
        
        //入力時
        if(_range_carettoDelFlg.location == 2){
            
            //時間フラグ
            UITextPosition *position = [textField positionFromPosition:_uiTextRange_TextFiledForcusCount.start
                                                                offset:1];
            textField.selectedTextRange = [textField textRangeFromPosition:position
                                                                toPosition:position];
        }else{
            
            //時間フラグ外
            UITextPosition *position = [textField positionFromPosition:_uiTextRange_TextFiledForcusCount.start
                                                                offset:0];
            textField.selectedTextRange = [textField textRangeFromPosition:position
                                                                toPosition:position];
        }
    }else if(_range_carettoDelFlg.length == 1){
        
        //削除時
        //時間フラグ外
        UITextPosition *position = [textField positionFromPosition:_uiTextRange_TextFiledForcusCount.start
                                                            offset:0];
        textField.selectedTextRange = [textField textRangeFromPosition:position
                                                            toPosition:position];
    }
    
    if(textField == self.Start_Time_txt){
        self.Start_Time_txt.backgroundColor = [UIColor whiteColor];
    }
    if(textField == self.End_Time_txt){
        self.End_Time_txt.backgroundColor = [UIColor whiteColor];
    }
}

//キャレット位置取得
- (NSInteger)getCaretLocationForInput:(UITextField *) input
{
    UITextRange *selRange = input.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    return [input offsetFromPosition:input.beginningOfDocument toPosition:selStartPos];
}
- (void)selectTextForInput:(UITextField *)input atRange:(NSRange)range
{
    UITextPosition *start = [input positionFromPosition:[input beginningOfDocument] offset:range.location];
    UITextPosition *end = [input positionFromPosition:start offset:range.length];
    [input setSelectedTextRange:[input textRangeFromPosition:start toPosition:end]];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    _Set_TextField = textField;
    return YES;
}

//テキストフィールドを編集する直後に呼び出されます
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField == self.Start_Time_txt){
        
        //初期は入力済み削除
        self.Start_Time_txt.text = @"";
        
        _Start_NotTimeFLG = false;
        //時間未定ボタン色セット
        [self setNotTime];
    }
    if(textField == self.End_Time_txt){
        
        //初期は入力済み削除
        self.End_Time_txt.text = @"";
        
        _End_LastTimeFLG = false;
        //時間未定ボタン色セット
        [self setLastTime];
    }
}

-(void)keyboardWillShow:(NSNotification*)note {
    
    // キーボードの表示開始時の場所と大きさを取得します。
    CGRect keyboardFrameBegin = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if(_Set_TextField == self.Start_Time_txt){
        int pointY = ([Configuration getScreenHeight]/2 - self.ScrollInView.frame.size.height/2) + self.Start_Time_txt.frame.origin.y + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
    
    if(_Set_TextField == self.End_Time_txt){
        int pointY = ([Configuration getScreenHeight]/2 - self.ScrollInView.frame.size.height/2) + self.End_Time_txt.frame.origin.y + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    
    if(textField == self.Start_Time_txt){
        //キーボードを隠す
        [self.Start_Time_txt resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    if(textField == self.End_Time_txt){
        //キーボードを隠す
        [self.End_Time_txt resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    return NO;
}
/////////////// ↑ テキスト入力関連 ↑ ////////////////////

/////////////// ↓ テキスト入補助 ↓ ////////////////////
-(void)StartTime_toolBar_frontaction:(id)sender{
    
    //キャロットを１文字前にする
    UITextRange    *range    = self.Start_Time_txt.selectedTextRange;
    UITextPosition *position = [self.Start_Time_txt positionFromPosition:range.start
                                                                  offset:-1];
    
    self.Start_Time_txt.selectedTextRange = [self.Start_Time_txt textRangeFromPosition:position
                                                                            toPosition:position];
}

-(void)StartTime_toolBar_backaction:(id)sender{
    
    //キャロットを１文字後にする（最大値ループ解除）
    UITextRange *selRange = self.Start_Time_txt.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    long n = [self.Start_Time_txt offsetFromPosition:self.Start_Time_txt.beginningOfDocument toPosition:selStartPos];
    
    if(self.Start_Time_txt.text.length > n){
        UITextPosition *position = [self.Start_Time_txt positionFromPosition:selRange.start
                                                                      offset:1];
        
        self.Start_Time_txt.selectedTextRange = [self.Start_Time_txt textRangeFromPosition:position
                                                                                toPosition:position];
    }
}

-(void)StartTime_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.Start_Time_txt resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}


-(void)EndTime_toolBar_frontaction:(id)sender{
    
    //キャロットを１文字前にする
    UITextRange    *range    = self.End_Time_txt.selectedTextRange;
    UITextPosition *position = [self.End_Time_txt positionFromPosition:range.start
                                                                  offset:-1];
    
    self.End_Time_txt.selectedTextRange = [self.End_Time_txt textRangeFromPosition:position
                                                                            toPosition:position];
}

-(void)EndTime_toolBar_backaction:(id)sender{
    
    //キャロットを１文字後にする（最大値ループ解除）
    UITextRange *selRange = self.End_Time_txt.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    long n = [self.End_Time_txt offsetFromPosition:self.End_Time_txt.beginningOfDocument toPosition:selStartPos];
    
    if(self.End_Time_txt.text.length > n){
        UITextPosition *position = [self.End_Time_txt positionFromPosition:selRange.start
                                                                      offset:1];
        
        self.End_Time_txt.selectedTextRange = [self.End_Time_txt textRangeFromPosition:position
                                                                                toPosition:position];
    }
}
-(void)EndTime_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.End_Time_txt resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}
/////////////// ↑ テキスト入力補助 ↑ ////////////////////

- (IBAction)Start_AmPm_Push:(id)sender {
    
    if(_Start_NotTimeFLG){
        
        _Start_NotTimeFLG = false;
        
        //時間未定ボタン色セット
        [self setNotTime];
        
    }else{
        
        if(_bln_StartPmFLG == false){
            
            _bln_StartPmFLG = true;
        }else{
            
            _bln_StartPmFLG = false;
        }
        //AMPM設定
        [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
    }
}

- (IBAction)End_AmPm_Push:(id)sender {
    
    if(_End_LastTimeFLG){
        
        _End_LastTimeFLG = false;
        
        //最終ボタン色セット
        [self setLastTime];
        
    }else{
        
        if(_bln_EndPmFLG == false){
            
            _bln_EndPmFLG = true;
        }else{
            
            _bln_EndPmFLG = false;
        }
        //AMPM設定
        [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
    }
}

- (IBAction)NotTime_Push:(id)sender {
    
    // キーボードを閉じる
    [self.Start_Time_txt resignFirstResponder];
    
    self.Start_Time_txt.backgroundColor = [UIColor whiteColor];
    
    if(_Start_NotTimeFLG == false){
        
        _Start_NotTimeFLG = true;
    }else{
        
        _Start_NotTimeFLG = false;
    }
    
    //時間未定ボタン色セット
    [self setNotTime];
    
    _bln_StartPmFLG = false;
    //AMPM設定
    [self setAmPmSwitch:false PmFlg:_bln_StartPmFLG];
}

- (void)setNotTime {
    
    if(_Start_NotTimeFLG){
        
        _Start_NotTime_lbl.textColor = [UIColor whiteColor];
        self.Start_NotTitleBack_view.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
        
        self.Start_AmPmSelect_lbl.textColor = [UIColor grayColor];
        self.Start_Time_txt.textColor = [UIColor grayColor];
        
//        self.Start_AmPm_Push.enabled = false;
//        self.Start_Time_txt.enabled = false;
    }else{
        
        _Start_NotTime_lbl.textColor = [UIColor blackColor];
        self.Start_NotTitleBack_view.backgroundColor = [UIColor whiteColor];
        
        self.Start_AmPmSelect_lbl.textColor = [UIColor blackColor];
        self.Start_Time_txt.textColor = [UIColor blackColor];
        
//        self.Start_AmPm_Push.enabled = true;
//        self.Start_Time_txt.enabled = true;
    }
}

- (IBAction)LastTime_Push:(id)sender {
    
    // キーボードを閉じる
    [self.End_Time_txt resignFirstResponder];
    
    self.End_Time_txt.backgroundColor = [UIColor whiteColor];
    
    if(_End_LastTimeFLG == false){
        
        _bln_EndPmFLG = false;
        //AMPM設定
        [self setAmPmSwitch:true PmFlg:_bln_EndPmFLG];
        
        _End_LastTimeFLG = true;
    }else{
        
        _End_LastTimeFLG = false;
    }
    
    //最終ボタン色セット
    [self setLastTime];
}

- (void)setLastTime {
    
    if(_End_LastTimeFLG){
        
        _End_LastTime_lbl.textColor = [UIColor whiteColor];
        self.End_LastTimeBack_view.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
        
        self.End_AmPmSelect_lbl.textColor = [UIColor grayColor];
        self.End_Time_txt.textColor = [UIColor grayColor];
        
//        self.End_AmPm_Push.enabled = false;
//        self.End_Time_txt.enabled = false;
    }else{
        
        _End_LastTime_lbl.textColor = [UIColor blackColor];
        self.End_LastTimeBack_view.backgroundColor = [UIColor whiteColor];
        
        self.End_AmPmSelect_lbl.textColor = [UIColor blackColor];
        self.End_Time_txt.textColor = [UIColor blackColor];
        
//        self.End_AmPm_Push.enabled = true;
//        self.End_Time_txt.enabled = true;
    }
}

- (IBAction)Cansel_push:(id)sender {
    
    // 前画面に戻る
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)Ok_push:(id)sender {

    
    BOOL bln_startCheck = false;
    BOOL bln_endCheck = false;
    
    NSString* str_StartTime = @"";
    NSString* str_EndTime = @"";
    BOOL bln_StartChk = false;
    BOOL bln_EndChk = false;
    
    //最終時
    if(_End_LastTimeFLG){
        
        _bln_EndPmFLG = false;
        self.End_Time_txt.text = @"08:00";
    }
    
    if(self.End_Time_txt.text.length == 5){
        
        str_EndTime = [_calHoliday GetTime24:_bln_EndPmFLG Time12:self.End_Time_txt.text];
        
        if(str_EndTime != nil){
            
            for(int d = 0;d < _array_EndTime.count;d++){
                
                if([_array_EndTime[d] isEqualToString:str_EndTime]){
                    
                    bln_EndChk = true;
                }
            }
            
            if(_End_LastTimeFLG == true){
                
                str_EndTime = @"08:00";
                bln_EndChk = true;
            }
            
            if(!bln_EndChk){
                
                [self.End_Time_txt becomeFirstResponder];
                self.End_Time_txt.backgroundColor = [UIColor redColor];
                [self messageError:@"" Title:@"エラー" message:@"この時間は選択できません。正しい時間を入力して下さい。"];
            }else{
                
                //値の整合チェックOK
                bln_endCheck = true;
            }
        }else{
            
            if(str_EndTime == nil){
                [self.End_Time_txt becomeFirstResponder];
                self.End_Time_txt.backgroundColor = [UIColor redColor];
            }
            
            [self messageError:@"" Title:@"エラー" message:@"この時間は選択できません。 正しい時間を入力してください。"];
        }
    }
    
    if(self.Start_Time_txt.text.length == 5){
        
        str_StartTime = [_calHoliday GetTime24:_bln_StartPmFLG Time12:self.Start_Time_txt.text];
        
        if((str_StartTime != nil)){
            
            for(int c = 0;c < _array_StartTime.count;c++){
                
                if([_array_StartTime[c] isEqualToString:str_StartTime]){
                    
                    bln_StartChk = true;
                }
            }
            
            if(_Start_NotTimeFLG == true){
                
                str_StartTime = @"";
                bln_StartChk = true;
            }
            
            if(!bln_StartChk){
                
                [self.Start_Time_txt becomeFirstResponder];
                self.Start_Time_txt.backgroundColor = [UIColor redColor];
                [self messageError:@"" Title:@"エラー" message:@"この時間は選択できません。正しい時間を入力して下さい。"];
            }else{
                
                //値の整合チェックOK
                bln_startCheck = true;
            }
        }else{
            
            if(str_StartTime == nil){
                [self.Start_Time_txt becomeFirstResponder];
                self.Start_Time_txt.backgroundColor = [UIColor redColor];
            }
            
            [self messageError:@"" Title:@"エラー" message:@"この時間は選択できません。 正しい時間を入力してください。"];
        }
    }
    
    //出社時間未定時
    if(_Start_NotTimeFLG){
        
        str_StartTime = @"";
        bln_startCheck = true;
    }
    
    //定時、自由での出社時間扱い設定(0:定時 1:自由)
    long lng_Schedulekind = [Configuration getScheduleKind];
    if(lng_Schedulekind == 0){
        
        if(self.Start_Time_txt.text.length < 5){
        
            //定時の時の未入力でエラー扱い
            bln_startCheck = false;
        }
    }
    
    //値の整合性OKでの保存
    if((bln_startCheck == true) && (bln_endCheck == true)){
        
        //変更対象のセット
        if(_bln_Day1Select){
            
            _str_Day1StartTime = str_StartTime;
            _str_Day1EndTime = str_EndTime;
        }
        if(_bln_Day2Select){
            
            _str_Day2StartTime = str_StartTime;
            _str_Day2EndTime = str_EndTime;
        }
        if(_bln_Day3Select){
            
            _str_Day3StartTime = str_StartTime;
            _str_Day3EndTime = str_EndTime;
        }
        if(_bln_Day4Select){
            
            _str_Day4StartTime = str_StartTime;
            _str_Day4EndTime = str_EndTime;
        }
        if(_bln_Day5Select){
            
            _str_Day5StartTime = str_StartTime;
            _str_Day5EndTime = str_EndTime;
        }
        if(_bln_Day6Select){
            
            _str_Day6StartTime = str_StartTime;
            _str_Day6EndTime = str_EndTime;
        }
        if(_bln_Day7Select){
            
            _str_Day7StartTime = str_StartTime;
            _str_Day7EndTime = str_EndTime;
        }
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
        TotalBasicTimeSetting_Confirmation_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"TotalBasicTimeSetting_Confirmation"];
        initialViewController.TotalBasicTimeSetting_Confirmation_delegate = self;
        initialViewController.rootView = _rootView;
        initialViewController.secondView = _secondView;
        initialViewController.api = _api;
        
        initialViewController.str_Day1StartTime = _str_Day1StartTime;
        initialViewController.str_Day1EndTime = _str_Day1EndTime;
        initialViewController.str_Day2StartTime = _str_Day2StartTime;
        initialViewController.str_Day2EndTime = _str_Day2EndTime;
        initialViewController.str_Day3StartTime = _str_Day3StartTime;
        initialViewController.str_Day3EndTime = _str_Day3EndTime;
        initialViewController.str_Day4StartTime = _str_Day4StartTime;
        initialViewController.str_Day4EndTime = _str_Day4EndTime;
        initialViewController.str_Day5StartTime = _str_Day5StartTime;
        initialViewController.str_Day5EndTime = _str_Day5EndTime;
        initialViewController.str_Day6StartTime = _str_Day6StartTime;
        initialViewController.str_Day6EndTime = _str_Day6EndTime;
        initialViewController.str_Day7StartTime = _str_Day7StartTime;
        initialViewController.str_Day7EndTime = _str_Day7EndTime;

        [self.navigationController pushViewController:initialViewController animated:YES];
        
    }else{
        
        if(bln_startCheck == false){
            
            [self.Start_Time_txt becomeFirstResponder];
            self.Start_Time_txt.backgroundColor = [UIColor redColor];
        }else{
            self.Start_Time_txt.backgroundColor = [UIColor whiteColor];
        }
        if(bln_endCheck == false){
            
            [self.End_Time_txt becomeFirstResponder];
            self.End_Time_txt.backgroundColor = [UIColor redColor];
        }else{
            self.End_Time_txt.backgroundColor = [UIColor whiteColor];
        }
        
        [self messageError:@"" Title:@"エラー" message:@"この時間は選択できません。 正しい時間を入力してください。"];
    }
}

-(void)messageError:(NSString*)typeName
              Title:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if([typeName isEqualToString:@""]){
        
        //OKのみのシングルアクション
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
    }else if([typeName isEqualToString:@"OK"]){
        
        //保存完了アクション
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self okSaveButtonPushed];
        }]];
    }

    [self presentViewController:alert animated:YES completion:nil];
}
- (void)okSaveButtonPushed {
    // 保存完了後のアラートアクション
    // 前画面に戻る
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setAmPmSwitch:(BOOL)endFlg
                PmFlg:(BOOL)pmflg {
    
    //開始時間
    if(endFlg == false){
        
        //AM
        if(pmflg == false){
            
            self.Start_AmPmSelect_lbl.text = @"AM";
            //PM
        }if(pmflg == true){
            
            self.Start_AmPmSelect_lbl.text = @"PM";
        }
        
        //終了時間
    }if(endFlg == true){
        
        //AM
        if(pmflg == false){
            
            self.End_AmPmSelect_lbl.text = @"AM";
            //PM
        }if(pmflg == true){
            
            self.End_AmPmSelect_lbl.text = @"PM";
        }
    }
}

//バックアクション
- (void)TotalBasicTimeSetting_Confirmation_CanselBack {
    
    //APIからの再読み込み設定
    _bln_reloadFlg = false;
}
//Login画面移動用
- (void)TotalBasicTimeSetting_Confirmation_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {

    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        if(arrayData.count > 0){
            
            NSArray* ary_Start = [arrayData valueForKey:@"start_at_candidates"];
            _array_StartTime = [NSMutableArray array];
            for(int c=0;c < ary_Start.count;c++){
                
                [_array_StartTime addObject:ary_Start[c]];
            }
            
            NSArray* ary_End = [arrayData valueForKey:@"end_at_candidates"];
            _array_EndTime = [NSMutableArray array];
            for(int c=0;c < ary_End.count;c++){
                
                [_array_EndTime addObject:ary_End[c]];
            }
        }
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //リトライ
                                                        [_api Api_BasicPeriodTimeGetting:self];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
        TotalBasicTimeSetting_Confirmation_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"TotalBasicTimeSetting_Confirmation"];
        initialViewController.TotalBasicTimeSetting_Confirmation_delegate = self;
        initialViewController.rootView = _rootView;
        initialViewController.secondView = _secondView;
        initialViewController.api = _api;
        
        initialViewController.str_Day1StartTime = _str_Day1StartTime;
        initialViewController.str_Day1EndTime = _str_Day1EndTime;
        initialViewController.str_Day2StartTime = _str_Day2StartTime;
        initialViewController.str_Day2EndTime = _str_Day2EndTime;
        initialViewController.str_Day3StartTime = _str_Day3StartTime;
        initialViewController.str_Day3EndTime = _str_Day3EndTime;
        initialViewController.str_Day4StartTime = _str_Day4StartTime;
        initialViewController.str_Day4EndTime = _str_Day4EndTime;
        initialViewController.str_Day5StartTime = _str_Day5StartTime;
        initialViewController.str_Day5EndTime = _str_Day5EndTime;
        initialViewController.str_Day6StartTime = _str_Day6StartTime;
        initialViewController.str_Day6EndTime = _str_Day6EndTime;
        initialViewController.str_Day7StartTime = _str_Day7StartTime;
        initialViewController.str_Day7EndTime = _str_Day7EndTime;
        
        [self.navigationController pushViewController:initialViewController animated:YES];
        
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //リトライ
                                                        [_api Api_BasicTimeWeekSetting:self week_json:data_Confirmation];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_422"]){
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@""
                                                message:@"この時間は選択できません。 正しい時間を入力してください。"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
