//
//  OneDayTimeSetting_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/18.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "Wheel_Callender_Library.h"

@protocol OneDayTimeSetting_ViewControllerDelegate <NSObject>
- (void)OneDayTimeSetting_LoginBackActionView;
- (void)OneDayTimeSetting_backActionView:(long)DayPotion
                               StartTime:(NSString*)stratTime
                                 EndTime:(NSString*)endTime;
- (void)OneDayTimeSetting_canselBackActionView;
@end

@interface OneDayTimeSetting_ViewController : UIViewController <ApiDelegate> {
    id<OneDayTimeSetting_ViewControllerDelegate> _OneDayTimeSetting_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    //カレンダーライブラリ用
    Wheel_Callender_Library* _calHoliday;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    long _daypotition;
    NSString* _str_day;
    NSString* _str_startTime;
    NSString* _str_endTime;
    
    //選択時間保存用
    NSMutableArray* _array_StartTime;
    NSMutableArray* _array_EndTime;
    
    //開始終了のフラグセット（０＝開始、１＝終了）
    long _lng_TimeSetFlg;
    
    //入力フィールド用
    UITextField *_Set_TextField;
    
    //テキストフォーカス位置保存用
    UITextRange* _uiTextRange_TextFiledForcusCount;
    NSRange _range_carettoDelFlg;
    
    //AMPM変更フラグ
    BOOL _bln_StartPmFLG;
    BOOL _bln_EndPmFLG;
    
    //時間未定、最終フラグ
    BOOL _Start_NotTimeFLG;
    BOOL _End_LastTimeFLG;
    
    //定時、自由での出社時間扱い設定
    BOOL _bln_ScheduleKind;
}

@property (nonatomic) id<OneDayTimeSetting_ViewControllerDelegate> OneDayTimeSetting_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
@property (nonatomic) long daypotition;
@property (nonatomic) NSString* str_day;
@property (nonatomic) NSString* str_startTime;
@property (nonatomic) NSString* str_endTime;

@property (weak, nonatomic) IBOutlet UILabel *day_lbl;

@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;
@property (weak, nonatomic) IBOutlet UIView *ScrollInView;

@property (weak, nonatomic) IBOutlet UILabel *Start_AmPmSelect_lbl;
@property (weak, nonatomic) IBOutlet UILabel *End_AmPmSelect_lbl;
@property (weak, nonatomic) IBOutlet UIButton *Start_AmPm_Push;
@property (weak, nonatomic) IBOutlet UIButton *End_AmPm_Push;

@property (weak, nonatomic) IBOutlet UITextField *Start_Time_txt;
@property (weak, nonatomic) IBOutlet UITextField *End_Time_txt;

@property (weak, nonatomic) IBOutlet UILabel *Start_NotTime_lbl;
@property (weak, nonatomic) IBOutlet UILabel *End_LastTime_lbl;
@property (weak, nonatomic) IBOutlet UIView *Start_NotTitleBack_view;
@property (weak, nonatomic) IBOutlet UIView *End_LastTimeBack_view;

@property (weak, nonatomic) IBOutlet UIView *Start_NotTime_view;

- (IBAction)Start_AmPm_Push:(id)sender;
- (IBAction)End_AmPm_Push:(id)sender;

- (IBAction)NotTime_Push:(id)sender;
- (IBAction)LastTime_Push:(id)sender;

- (IBAction)Cansel_push:(id)sender;
- (IBAction)Ok_push:(id)sender;

@end
