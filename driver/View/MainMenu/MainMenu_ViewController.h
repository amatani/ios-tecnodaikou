//
//  MainMenu_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2015/12/23.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "SyussyaYoyakuWeek_ViewController.h"
#import "SettingMenu_ViewController.h"
#import "NormalNotice_ViewController.h"
#import "ImportantNotice_ViewController.h"
#import "AttendanceFromNow_ViewController.h"
#import "TotalBasicTimeSetting_ViewController.h"
#import "PushSetting_ViewController.h"
#import "MailAdressChenge_ViewController.h"
#import "PasswordReset_ViewController.h"
#import "LostAcount_ViewController.h"

@protocol MainMenu_ViewControllerDelegate <NSObject>
- (void)mainMenu_BackActionView;
- (void)mainMenu_LoginBackAction;
@end

@interface MainMenu_ViewController : UIViewController <ApiDelegate, SyussyaYoyakuWeek_ViewControllerDelegate, NormalNotice_ViewControllerDelegate, ImportantNotice_ViewControllerDelegate, SettingMenu_ViewControllerDelegate ,AttendanceFromNow_ViewControllerDelegate, TotalBasicTimeSetting_ViewControllerDelegate, PushSetting_ViewControllerDelegate, MailAdressChenge_ViewControllerDelegate, PasswordReset_ViewControllerDelegate, LostAcount_ViewControllerDelegate, UsedInfomation_ViewControllerDelegate> {
    
    id<MainMenu_ViewControllerDelegate> _MainMenu_delegate;
    MainMenu_ViewController* _loginrootView;
    Api* _api;
    
    NSMutableArray *_array_NormalNoti_Id;
    NSMutableArray *_array_NormalNoti_Title;
    NSMutableArray *_array_NormalNoti_Read;
    NSMutableArray *_array_NormalNoti_Created_at;
    
    NSMutableArray *_array_ImportantNoti_Id;
    NSMutableArray *_array_ImportantNoti_Title;
    NSMutableArray *_array_ImportantNoti_Read;
    NSMutableArray *_array_ImportantNoti_Created_at;
    
    //シャドー用view
    UIView* _view_Shadow;
    
    NSTimer* _timer_time;
    NSTimer* _timer_notification;
}
@property (nonatomic) id<MainMenu_ViewControllerDelegate> MainMenu_delegate;
@property (nonatomic) UIViewController* loginrootView;

@property (weak, nonatomic) IBOutlet UILabel *txt_chiku;
@property (weak, nonatomic) IBOutlet UILabel *txt_name;

@property (weak, nonatomic) IBOutlet UILabel *txt_date;
@property (weak, nonatomic) IBOutlet UILabel *txt_time;

@property (weak, nonatomic) IBOutlet UIView *view_jyuyouoshirase;
@property (weak, nonatomic) IBOutlet UIView *view_oshirase;

@property (weak, nonatomic) IBOutlet UILabel *txt_jyuyouoshirase;
@property (weak, nonatomic) IBOutlet UILabel *txt_oshirase;

@property (weak, nonatomic) IBOutlet UILabel *lbl_menu1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_menu2;

@property (weak, nonatomic) IBOutlet UILabel *lbl_KonsyuYotei;
@property (weak, nonatomic) IBOutlet UILabel *lbl_YokusyuYotei;

@property (weak, nonatomic) IBOutlet UIImageView *img_yajirushi_Jyuyoushirase;
@property (weak, nonatomic) IBOutlet UIView *view_count_Jyuyoushirase;
@property (weak, nonatomic) IBOutlet UILabel *lbl_jyuyouoshirase;

@property (weak, nonatomic) IBOutlet UIImageView *img_yajirushi_Oshirase;
@property (weak, nonatomic) IBOutlet UIView *view_count_Oshirase;
@property (weak, nonatomic) IBOutlet UILabel *lbl_oshirase;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Setting;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NowWork;

- (IBAction)menu_push:(id)sender;

- (IBAction)KonsyuYotei_push:(id)sender;
- (void)konsyuYotei;
- (IBAction)YokusyuYotei_push:(id)sender;
- (void)YokusyuYotei;
- (IBAction)jyuyouoshirase_push:(id)sender;
- (IBAction)oshirase_push:(id)sender;

- (IBAction)setting_push:(id)sender;
- (IBAction)nowsyussya_push:(id)sender;

- (void)shadowKill;

- (void)defaultTime;
- (void)pushSetting;
- (void)mailChecge;
- (void)passwordReset;
- (void)apliInfo;
- (void)logout;

- (void)jyuyouoshirase_viewSet;
- (void)oshirase_viewSet;

@end