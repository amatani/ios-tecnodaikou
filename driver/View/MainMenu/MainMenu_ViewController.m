//
//  Main_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2015/12/23.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "MainMenu_ViewController.h"
#import "SideMenu_ViewController.h"

@interface MainMenu_ViewController ()
@end

@implementation MainMenu_ViewController

@synthesize MainMenu_delegate = _MainMenu_delegate;
@synthesize loginrootView = _loginrootView;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([MainMenu_ViewController class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //画面再開時の処理(viewWillAppear実行で再開)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewWillAppear:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    // api設定
    _api = [[Api alloc]init];
    
    self.txt_chiku.text = @"";
    self.txt_name.text = @"";
    
    UIFont *font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/28];
    self.lbl_menu1.font = font;
    self.lbl_menu2.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/26];
    self.lbl_KonsyuYotei.font = font;
    self.lbl_YokusyuYotei.font = font;
    self.lbl_jyuyouoshirase.font = font;
    self.lbl_oshirase.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/26];
    self.lbl_Setting.font = font;
    self.lbl_NowWork.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/18];
    self.txt_jyuyouoshirase.font = font;
    self.txt_oshirase.font = font;
    self.txt_jyuyouoshirase.text = [NSString stringWithFormat:@"%ld",[Configuration getImportantNoticeCount]];
    self.txt_oshirase.text = [NSString stringWithFormat:@"%ld",[Configuration getNormalNoticeCount]];

    font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/22];
    self.txt_chiku.font = font;
    self.txt_name.font = font;
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([MainMenu_ViewController class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //apiDelegate設定
    _api.apidelegate = self;
    
    //シャドーを削除
    [self shadowKill];
    
    //プロファイルの取得
    [_api Api_ProfileGetting:self];
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([MainMenu_ViewController class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
    
    [self timer_date];
    _timer_time = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                     target:self
                                   selector:@selector(timer_date)
                                   userInfo:nil
                                    repeats:YES];
    
    _timer_notification = [NSTimer scheduledTimerWithTimeInterval:15.0f
                                     target:self
                                   selector:@selector(notificationContGet)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([MainMenu_ViewController class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
    
    //タイマー停止
    [_timer_time invalidate];
    [_timer_notification invalidate];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([MainMenu_ViewController class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([MainMenu_ViewController class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([MainMenu_ViewController class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

-(void)timer_date{
    
    NSDateFormatter *format_date = [[NSDateFormatter alloc] init];
    [format_date setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
    [format_date setDateFormat:@"yyyy/M/d EEE"];
    
    NSDateFormatter *format_time = [[NSDateFormatter alloc] init];
    [format_time setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
    [format_time setDateFormat:@"HH:mm"];
    
    self.txt_date.text = [format_date stringFromDate:[NSDate date]];
    self.txt_time.text = [format_time stringFromDate:[NSDate date]];
}

-(void)notificationContGet{
    
    long lng_count = [UIApplication sharedApplication].applicationIconBadgeNumber;
    long lng_setcount = [self.txt_jyuyouoshirase.text intValue] + [self.txt_oshirase.text intValue];
    
    if(lng_count != lng_setcount && lng_setcount > 0){
        
        //通常通知取得
        [_api Api_NormalNotificationGetting:self];
    }
}

- (IBAction)menu_push:(id)sender {
    
    //スライドメニューの起動
    [_MainMenu_delegate mainMenu_BackActionView];
    
    // UIViewの生成
    _view_Shadow = [[UIView alloc] initWithFrame:CGRectMake(0,20,[Configuration getScreenWidth],[Configuration getScreenHeight])];
    _view_Shadow.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_view_Shadow];
    
    //シャドーを入れる
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"shadow.png"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    _view_Shadow.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
}

- (IBAction)KonsyuYotei_push:(id)sender {
    
    [self konsyuYotei];
}
- (void)konsyuYotei {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SyussyaYoyaku_ViewController" bundle:[NSBundle mainBundle]];
    SyussyaYoyakuWeek_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"SyussyaYoyakuWeek"];
    initialViewController.SyussyaYoyakuWeek_delegate = self;
    initialViewController.rootView = self;
    
    initialViewController.api = _api;
    
    initialViewController.lng_WeekType = 1;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (IBAction)YokusyuYotei_push:(id)sender {
    
    [self YokusyuYotei];
}
- (void)YokusyuYotei {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SyussyaYoyaku_ViewController" bundle:[NSBundle mainBundle]];
    SyussyaYoyakuWeek_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"SyussyaYoyakuWeek"];
    initialViewController.SyussyaYoyakuWeek_delegate = self;
    initialViewController.rootView = self;
    
    initialViewController.api = _api;
    
    initialViewController.lng_WeekType = 2;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (IBAction)jyuyouoshirase_push:(id)sender {
    
    [self jyuyouoshirase_viewSet];
}
- (void)jyuyouoshirase_viewSet {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ImportantNotice_ViewController" bundle:[NSBundle mainBundle]];
    ImportantNotice_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"ImportantNotice"];
    initialViewController.ImportantNotice_delegate = self;
    initialViewController.rootView = self;
    initialViewController.api = _api;
    
    initialViewController.array_Id = _array_ImportantNoti_Id;
    initialViewController.array_Title = _array_ImportantNoti_Title;
    initialViewController.array_Read = _array_ImportantNoti_Read;
    initialViewController.array_Created_at = _array_ImportantNoti_Created_at;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (IBAction)oshirase_push:(id)sender {
    
    [self oshirase_viewSet];
}
- (void)oshirase_viewSet {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"NormalNotice_ViewController" bundle:[NSBundle mainBundle]];
    NormalNotice_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"NormalNotice"];
    initialViewController.NormalNotice_delegate = self;
    initialViewController.rootView = self;
    initialViewController.api = _api;
    
    initialViewController.array_Id = _array_NormalNoti_Id;
    initialViewController.array_Title = _array_NormalNoti_Title;
    initialViewController.array_Read = _array_NormalNoti_Read;
    initialViewController.array_Created_at = _array_NormalNoti_Created_at;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (IBAction)setting_push:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SettingMenu_ViewController" bundle:[NSBundle mainBundle]];
    SettingMenu_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"SettingMenu"];
    initialViewController.SettingMenu_delegate = self;
    initialViewController.rootView = self;
    initialViewController.api = _api;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (IBAction)nowsyussya_push:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AttendanceFromNow_ViewController" bundle:[NSBundle mainBundle]];
    AttendanceFromNow_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"AttendanceFromNow"];
    initialViewController.AttendanceFromNow_delegate = self;
    initialViewController.rootView = self;
    initialViewController.api = _api;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

//シャドー削除のみ
- (void)shadowKill {
    
    //シャドーを削除
    [_view_Shadow removeFromSuperview];
}

- (void)defaultTime {
    
    //基本時間設定画面
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
    TotalBasicTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"TotalBasicTimeSetting"];
    initialViewController.TotalBasicTimeSetting_delegate = self;
    initialViewController.secondView = self;
    initialViewController.rootView = self;
    initialViewController.api = _api;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (void)pushSetting {
    
    //プッシュ通知設定画面
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PushSetting_ViewController" bundle:[NSBundle mainBundle]];
    PushSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"PushSetting"];
    initialViewController.PushSetting_delegate = self;
    initialViewController.rootView = self;
    initialViewController.api = _api;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (void)mailChecge {
    
    //メールアドレス変更画面
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MailAdressChenge_ViewController" bundle:[NSBundle mainBundle]];
    MailAdressChenge_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"MailAdressChenge"];
    initialViewController.MailAdressChenge_delegate = self;
    initialViewController.rootView = self;
    initialViewController.api = _api;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (void)passwordReset {
    
    //アカウント再設定画面
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TopLogin_ViewController" bundle:[NSBundle mainBundle]];
    LostAcount_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"LostAcount"];
    initialViewController.LostAcount_delegate = self;
    initialViewController.loginrootView = self;
    initialViewController.loginSetView = self;
    initialViewController.api = _api;
    initialViewController.str_BackButtonText = @"戻る";
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (void)apliInfo {
    
    //アプリの使い方画面
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"UsedInfomation_ViewController" bundle:[NSBundle mainBundle]];
    UsedInfomation_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"UsedInfomation"];
    initialViewController.UsedInfomation_delegate = self;
    initialViewController.rootView = self;
    
    [self.navigationController pushViewController:initialViewController animated:YES];
}

- (void)logout {
    
    //ログアウト
    [_api Api_Logout:self];
}

//バックアクション
- (void)SettingMenu_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}

//Login画面移動用
- (void)MailAdressChenge_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}
- (void)AttendanceFromNow_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}
- (void)SyussyaYoyakuWeek_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}
- (void)TotalBasicTimeSetting_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}
- (void)PasswordReset_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}
- (void)PushSetting_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}
- (void)NormalNotice_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}
- (void)ImportantNotice_LoginBackActionView {
    
    //ログイン画面へ戻る
    [_MainMenu_delegate mainMenu_LoginBackAction];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    //ログアウト
    if(FLG == YES){
        
        //ログイン画面へ戻る
        [_MainMenu_delegate mainMenu_LoginBackAction];
    }else{
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            //ログイン画面へ戻る
            [_MainMenu_delegate mainMenu_LoginBackAction];
        }
    }
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent  num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
    
    if(FLG == YES){
        
        NSString* str_area = @"";
        switch (area.integerValue) {
            case 0:
                str_area =@"高松";
                break;
            case 1:
                str_area =@"丸亀";
                break;
            case 2:
                str_area =@"観音寺";
                break;
        }
        
        NSString* str_parent = @"";
        if(parent == true){
            str_parent = @"親";
        }else{
            str_parent = @"子";
        }
        
        self.txt_chiku.text = [NSString stringWithFormat:@"%@　%@", str_area, str_parent];
        self.txt_name.text = username;
        
        //時間設定の定時・自由を設定(0:定時 1:自由)
        [Configuration setScheduleKind:(long)num_schedule_kind];
        
        //通常通知取得
        [_api Api_NormalNotificationGetting:self];
        
    }else{
        
        //通信中解除
        [SVProgressHUD dismiss];
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //重要通知取得
                                                        [_api Api_ProfileGetting:self];
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else{
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Dialog_API_IntenetNotConnectErrTitleMsg",@"")
                                                message:NSLocalizedString(@"Dialog_API_IntenetNotConnectErrMsg",@"")
                                         preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_API_IntenetNotConnectErrOK",@"")
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //ログイン画面へ戻る
                                                        [_MainMenu_delegate mainMenu_LoginBackAction];
                                                    }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    if(FLG == YES){
        
        if(arrayData.count > 0){
            
            _array_NormalNoti_Id = [NSMutableArray array];
            _array_NormalNoti_Title = [NSMutableArray array];
            _array_NormalNoti_Read = [NSMutableArray array];
            _array_NormalNoti_Created_at = [NSMutableArray array];
            
            for(long c = 0;c < arrayData.count;c++){
                
                [_array_NormalNoti_Id addObject:[[arrayData objectAtIndex:c] objectAtIndex:0]];
                [_array_NormalNoti_Title addObject:[[arrayData objectAtIndex:c] objectAtIndex:1]];
                [_array_NormalNoti_Read addObject:[[arrayData objectAtIndex:c] objectAtIndex:2]];
                //日付型に一時変換
                NSDateFormatter* formatter = [NSDateFormatter new];
                formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
                formatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
                //12時間表記対策
                [formatter setLocale:[NSLocale systemLocale]];
                NSDate* date = [formatter dateFromString:[[arrayData objectAtIndex:c] objectAtIndex:3]];
                //日付を文字変換
                [formatter setDateFormat:@"YYYY/MM/dd"];
                NSString* date_converted = [formatter stringFromDate:date];
                
                [_array_NormalNoti_Created_at addObject:date_converted];
            }
            
        }else{
            self.view_count_Oshirase.hidden = YES;
            self.txt_oshirase.text = [NSString stringWithFormat:@"%d",0];
        }
        
        //重要通知取得
        [_api Api_InportantNotificationGetting:self];
        
    }else{
        
        //通信中解除
        [SVProgressHUD dismiss];
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //重要通知取得
                                                        [_api Api_NormalNotificationGetting:self];
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            //ログイン画面へ戻る
            [_MainMenu_delegate mainMenu_LoginBackAction];
        }
    }
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    if(FLG == YES){
        
        if(arrayData.count > 0){
            
            _array_ImportantNoti_Id = [NSMutableArray array];
            _array_ImportantNoti_Title = [NSMutableArray array];
            _array_ImportantNoti_Read = [NSMutableArray array];
            _array_ImportantNoti_Created_at = [NSMutableArray array];
            
            for(long c = 0;c < arrayData.count;c++){
                
                [_array_ImportantNoti_Id addObject:[[arrayData objectAtIndex:c] objectAtIndex:0]];
                [_array_ImportantNoti_Title addObject:[[arrayData objectAtIndex:c] objectAtIndex:1]];
                [_array_ImportantNoti_Read addObject:[[arrayData objectAtIndex:c] objectAtIndex:2]];
                //日付型に一時変換
                NSDateFormatter* formatter = [NSDateFormatter new];
                formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
                formatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
                //12時間表記対策
                [formatter setLocale:[NSLocale systemLocale]];
                NSDate* date = [formatter dateFromString:[[arrayData objectAtIndex:c] objectAtIndex:3]];
                //日付を文字変換
                [formatter setDateFormat:@"YYYY/MM/dd"];
                NSString* date_converted = [formatter stringFromDate:date];
                
                [_array_ImportantNoti_Created_at addObject:date_converted];
            }
            
        }else{
            self.view_count_Jyuyoushirase.hidden = YES;
            self.txt_jyuyouoshirase.text = [NSString stringWithFormat:@"%d",0];
        }
        
        //通知カウント取得
        [_api Api_NotificationCountGetting:self];
        
    }else{
        
        //通信中解除
        [SVProgressHUD dismiss];
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //重要通知取得
                                                        [_api Api_InportantNotificationGetting:self];
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            //ログイン画面へ戻る
            [_MainMenu_delegate mainMenu_LoginBackAction];
        }
    }
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        long lng_normalcount = 0;
        long lng_importantcount = 0;
        long lng_unreadcount = 0;
        
        if(arrayData.count > 0){

            lng_normalcount = [[arrayData valueForKey:@"number_of_unread_normal"] longValue];
            lng_importantcount = [[arrayData valueForKey:@"number_of_unread_important"] longValue];
            lng_unreadcount = [[arrayData valueForKey:@"number_of_unread"] longValue];

                
            if(lng_normalcount == 0){
                self.view_count_Oshirase.hidden = YES;
                self.txt_oshirase.text = [NSString stringWithFormat:@"%d",0];
            }else{
                self.view_count_Oshirase.hidden = NO;
                self.txt_oshirase.text = [NSString stringWithFormat:@"%ld", lng_normalcount];
            }
            
            if(lng_importantcount == 0){
                self.view_count_Jyuyoushirase.hidden = YES;
                self.txt_jyuyouoshirase.text = [NSString stringWithFormat:@"%d",0];
            }else{
                self.view_count_Jyuyoushirase.hidden = NO;
                self.txt_jyuyouoshirase.text = [NSString stringWithFormat:@"%ld", lng_importantcount];
            }
            
        }else{
            self.view_count_Oshirase.hidden = YES;
            self.txt_oshirase.text = [NSString stringWithFormat:@"%d",0];
            self.view_count_Jyuyoushirase.hidden = YES;
            self.txt_jyuyouoshirase.text = [NSString stringWithFormat:@"%d",0];
        }
        
        //お知らせ件数保存
        [Configuration setNormalNoticeCount:lng_normalcount];
        [Configuration setImportantNoticeCount:lng_importantcount];
        [UIApplication sharedApplication].applicationIconBadgeNumber = lng_normalcount + lng_importantcount;
        
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //重要通知取得
                                                        [_api Api_InportantNotificationGetting:self];
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            //ログイン画面へ戻る
            [_MainMenu_delegate mainMenu_LoginBackAction];
        }
    }
}

@end
