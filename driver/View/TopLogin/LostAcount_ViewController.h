//
//  LostAcount_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/08.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "LostAcountSet_ViewController.h"

@protocol LostAcount_ViewControllerDelegate<NSObject>
@end

@interface LostAcount_ViewController : UIViewController <ApiDelegate, LostAcountSet_ViewControllerDelegate> {
    
    id<LostAcount_ViewControllerDelegate> _LostAcount_delegate;
    UIViewController* _loginrootView;
    UIViewController* _loginSetView;
    Api* _api;
    
    //キャリアドメイン名
    NSMutableArray* _array_CarrierDomains;
    
    //戻るボタン名設定
    NSString* _str_BackButtonText;
}
@property (nonatomic) id<LostAcount_ViewControllerDelegate> LostAcount_delegate;
@property (nonatomic) UIViewController* loginrootView;
@property (nonatomic) UIViewController* loginSetView;
@property (nonatomic) Api* api;

//戻るボタン名設定
@property (nonatomic) NSString* str_BackButtonText;

@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;

@property (weak, nonatomic) IBOutlet UITextField *meil_Text;
@property (weak, nonatomic) IBOutlet UITextField *DriverNo_Text;
@property (weak, nonatomic) IBOutlet UITextField *Tel_Text;
@property (weak, nonatomic) IBOutlet UIDatePicker *Picker_Date;

@property (weak, nonatomic) IBOutlet UILabel *lbl_BackButton;

- (IBAction)Cansel_push:(id)sender;
- (IBAction)Check_push:(id)sender;

@end
