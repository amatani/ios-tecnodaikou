//
//  Login_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/08.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "LostAcount_ViewController.h"

@protocol Login_ViewControllerDelegate<NSObject>
- (void)login_LoginFromMain_backActionView;
@end

@interface Login_ViewController : UIViewController <ApiDelegate, LostAcount_ViewControllerDelegate> {
    
    id<Login_ViewControllerDelegate> _Login_delegate;
    UIViewController* _loginrootView;
    Api* _api;
}
@property (nonatomic) id<Login_ViewControllerDelegate> Login_delegate;
@property (nonatomic) UIViewController* loginrootView;
@property (nonatomic) Api* api;

@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;
@property (weak, nonatomic) IBOutlet UIView *ScrollInView;
@property (weak, nonatomic) IBOutlet UIView *ModalFrameView;

- (IBAction)IDSaveSwitch:(id)sender;
- (IBAction)LoginPush:(id)sender;
- (IBAction)PasswordLostPush:(id)sender;
- (IBAction)CanselPush:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *MailOrDriverNO_Text;
@property (weak, nonatomic) IBOutlet UITextField *Password_Text;
@property (weak, nonatomic) IBOutlet UISwitch *IDSave_Sw;

@property (weak, nonatomic) IBOutlet UILabel *version_lbl;

@end
