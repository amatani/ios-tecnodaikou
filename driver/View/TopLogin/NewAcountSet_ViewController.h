//
//  NewAcountSet_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/07.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

@protocol NewAcountSet_ViewControllerDelegate<NSObject>
@end

@interface NewAcountSet_ViewController : UIViewController <ApiDelegate> {
    
    id<NewAcountSet_ViewControllerDelegate> _NewAcountSet_delegate;
    UIViewController* _loginrootView;
    Api* _api;
    
    //引き継がれるパラメータ
    NSString* _str_Mail;
    NSString* _str_BirthdayYear;
    NSString* _str_BirthdayMonth;
    NSString* _str_BirthdayDay;
    NSString* _str_DriveNo;
    NSString* _str_Tel;
    NSString* _str_Password;
}
@property (nonatomic) id<NewAcountSet_ViewControllerDelegate> NewAcountSet_delegate;
@property (nonatomic) UIViewController* loginrootView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
@property (nonatomic) NSString* str_Mail;
@property (nonatomic) NSString* str_BirthdayYear;
@property (nonatomic) NSString* str_BirthdayMonth;
@property (nonatomic) NSString* str_BirthdayDay;
@property (nonatomic) NSString* str_DriveNo;
@property (nonatomic) NSString* str_Tel;
@property (nonatomic) NSString* str_Password;

@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;

@property (weak, nonatomic) IBOutlet UITextField *meil_Text;
@property (weak, nonatomic) IBOutlet UITextField *DriverNo_Text;
@property (weak, nonatomic) IBOutlet UITextField *Tel_Text;
@property (weak, nonatomic) IBOutlet UITextField *BirthdayYear_Text;
@property (weak, nonatomic) IBOutlet UITextField *BirthdayMonth_Text;
@property (weak, nonatomic) IBOutlet UITextField *BirthdayDay_Text;
@property (weak, nonatomic) IBOutlet UITextField *password_Text;

- (IBAction)Cansel_push:(id)sender;
- (IBAction)Save_push:(id)sender;
@end
