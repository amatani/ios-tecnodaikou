//
//  NewAcount_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/06.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "NewAcountSet_ViewController.h"

@protocol NewAcount_ViewControllerDelegate<NSObject>
@end

@interface NewAcount_ViewController : UIViewController <ApiDelegate, NewAcountSet_ViewControllerDelegate> {
    
    id<NewAcount_ViewControllerDelegate> _NewAcount_delegate;
    UIViewController* _loginrootView;
    Api* _api;
    
    //キャリアドメイン名
    NSMutableArray* _array_CarrierDomains;
}
@property (nonatomic) id<NewAcount_ViewControllerDelegate> NewAcount_delegate;
@property (nonatomic) UIViewController* loginrootView;
@property (nonatomic) Api* api;

@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;

@property (weak, nonatomic) IBOutlet UITextField *meil_Text;
@property (weak, nonatomic) IBOutlet UITextField *DriverNo_Text;
@property (weak, nonatomic) IBOutlet UITextField *Tel_Text;
@property (weak, nonatomic) IBOutlet UIDatePicker *Picker_Date;
@property (weak, nonatomic) IBOutlet UITextField *password_Text;

- (IBAction)Cansel_push:(id)sender;
- (IBAction)Check_push:(id)sender;

@end
