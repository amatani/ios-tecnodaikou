//
//  LostAcount_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2016/01/08.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "LostAcount_ViewController.h"

@interface LostAcount_ViewController () {
    
    UITextField *Set_TextField;
}
@end

@implementation LostAcount_ViewController

@synthesize LostAcount_delegate = _LostAcount_delegate;
@synthesize loginrootView = _loginrootView;
@synthesize loginSetView = _loginSetView;
@synthesize api = _api;
@synthesize str_BackButtonText = _str_BackButtonText;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //データピッカーの初期日設定
    NSCalendar* calendar = [NSCalendar currentCalendar];
    //12時間表記対策
    [calendar setLocale:[NSLocale systemLocale]];
    //タイムゾーンの指定
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
    NSDateComponents* components = [[NSDateComponents alloc] init];
    
    components.year = 2000;
    components.month = 1;
    components.weekday = 1;
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    
    NSDate* fromFormatDate = [calendar dateFromComponents:components];
    self.Picker_Date.date = fromFormatDate;
    
    //データピッカーの最大値設定
    self.Picker_Date.maximumDate = [NSDate date];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_loginrootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_loginrootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    //戻るボタンの名前設定
    self.lbl_BackButton.text = _str_BackButtonText;
    
    //メールドメイン一覧取得
    [_api Api_CarrierDomainsGetting:self];
    
    //ツールバーを生成（メールアドレス）
    UIToolbar *meil_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    meil_Text_toolBar.barStyle = UIBarStyleDefault;
    [meil_Text_toolBar sizeToFit];
    UIBarButtonItem *meil_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_meil_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(meil_Text_toolBar_closeKeyboard:)];
    NSArray *meil_Text_toolBarItems = [NSArray arrayWithObjects:meil_Text_spacer, _meil_Text_commitBtn, nil];
    [meil_Text_toolBar setItems:meil_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.meil_Text.inputAccessoryView = meil_Text_toolBar;
    
    //ツールバーを生成（ドライバーナンバー）
    UIToolbar *DriverNo_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    DriverNo_Text_toolBar.barStyle = UIBarStyleDefault;
    [DriverNo_Text_toolBar sizeToFit];
    UIBarButtonItem *DriverNo_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_DriverNo_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DriverNo_Text_toolBar_closeKeyboard:)];
    NSArray *DriverNo_Text_toolBarItems = [NSArray arrayWithObjects:DriverNo_Text_spacer, _DriverNo_Text_commitBtn, nil];
    [DriverNo_Text_toolBar setItems:DriverNo_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.DriverNo_Text.inputAccessoryView = DriverNo_Text_toolBar;
    
    //ツールバーを生成（電話番号）
    UIToolbar *Tel_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    Tel_Text_toolBar.barStyle = UIBarStyleDefault;
    [Tel_Text_toolBar sizeToFit];
    UIBarButtonItem *Tel_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_Tel_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(Tel_Text_toolBar_closeKeyboard:)];
    NSArray *Tel_Text_toolBarItems = [NSArray arrayWithObjects:Tel_Text_spacer, _Tel_Text_commitBtn, nil];
    [Tel_Text_toolBar setItems:Tel_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.Tel_Text.inputAccessoryView = Tel_Text_toolBar;
    
    //テキストフィールド入力時の起動メソッド設定
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //スクロールを制限
    if(scrollView.contentOffset.y < 0){
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
    }
}

/////////////// ↓ テキスト入力関連 ↓ ////////////////////
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    Set_TextField = textField;
    return YES;
}

//テキストフィールドを編集する直後に呼び出されます
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    NSLog(@"textFieldDidBeginEditing");
}

//入力開始時の制御
-(void)keyboardWillShow:(NSNotification*)note {
    
    // キーボードの表示開始時の場所と大きさを取得します。
    CGRect keyboardFrameBegin = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if(Set_TextField == self.meil_Text){
        int pointY = self.meil_Text.frame.origin.y + 100 + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
    
    if(Set_TextField == self.DriverNo_Text){
        int pointY = self.DriverNo_Text.frame.origin.y + 100 + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
    
    if(Set_TextField == self.Tel_Text){
        int pointY = self.Tel_Text.frame.origin.y + 100 + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    
    if(textField == self.meil_Text){
        //キーボードを隠す
        [self.meil_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    if(textField == self.DriverNo_Text){
        //キーボードを隠す
        [self.DriverNo_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    if(textField == self.Tel_Text){
        //キーボードを隠す
        [self.Tel_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    return NO;
}

-(void)textFieldDidEndEditing:(UITextField*)textField {
    
    if(textField == self.DriverNo_Text){
        if(![self.DriverNo_Text.text isEqualToString:@""]){
            NSRange range = [self.DriverNo_Text.text rangeOfString:@"@"];
            if (range.location == NSNotFound) {
                //@マーク無しで指定文字以下の場合に０を付ける
                for(long c = self.DriverNo_Text.text.length;c < [app loginNoColumCount];c++){
                    
                    self.DriverNo_Text.text = [NSString stringWithFormat:@"%@%@", @"0", self.DriverNo_Text.text];
                }
            }
        }
    }
}
/////////////// ↑ テキスト入力関連 ↑ ////////////////////

/////////////// ↓ テキスト入力補助 ↓ ////////////////////
-(void)meil_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.meil_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}

-(void)DriverNo_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.DriverNo_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}

-(void)Tel_Text_toolBar_closeKeyboard:(id)sender{
    
    [self.Tel_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}
/////////////// ↑ テキスト入力補助 ↑ ////////////////////

- (IBAction)Cansel_push:(id)sender {
    
    //フォームのクローズ
    [self.navigationController popToViewController:_loginrootView animated:NO];
}

- (IBAction)Check_push:(id)sender {
    
    [self.meil_Text resignFirstResponder];
    [self.DriverNo_Text resignFirstResponder];
    [self.Tel_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
    
    if(![self.meil_Text.text isEqualToString:@""]){
        if(![self.DriverNo_Text.text isEqualToString:@""]){
            if(![self.Tel_Text.text isEqualToString:@""]){
                if(self.DriverNo_Text.text.length == 3){
                    if([self checkMailAdressCheck]){
                        if((self.Tel_Text.text.length == 11)){
                            
                            //確認画面へ移動
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TopLogin_ViewController" bundle:[NSBundle mainBundle]];
                            LostAcountSet_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"LostAcountSet"];
                            initialViewController.LostAcountSet_delegate = self;
                            initialViewController.loginrootView = _loginrootView;
                            initialViewController.loginSetView = _loginSetView;
                            initialViewController.api = _api;
                            
                            initialViewController.str_Mail = self.meil_Text.text;
                            
                            NSCalendar *calendar = [NSCalendar currentCalendar];
                            //12時間表記対策
                            [calendar setLocale:[NSLocale systemLocale]];
                            //タイムゾーンの指定
                            [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
                            NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear |
                                                           NSCalendarUnitMonth  |
                                                           NSCalendarUnitDay
                                                                      fromDate:self.Picker_Date.date];
                            
                            initialViewController.str_BirthdayYear = [NSString stringWithFormat : @"%ld", (long)dateComps.year];
                            initialViewController.str_BirthdayMonth = [NSString stringWithFormat : @"%ld", (long)dateComps.month];
                            initialViewController.str_BirthdayDay = [NSString stringWithFormat : @"%ld", (long)dateComps.day];
                            
                            initialViewController.str_DriveNo = self.DriverNo_Text.text;
                            initialViewController.str_Tel = self.Tel_Text.text;
                            
                            [self.navigationController pushViewController:initialViewController animated:NO];
                        }else{
                            [self messageError:@"" message:@"携帯電話番号の入力は11桁指定です。再入力お願いします。"];
                        }
                    }else{
                        [self messageError:@"" message:@"メールアドレスはキャリアメールを入力してください。"];
                    }
                }else{
                    [self messageError:@"" message:@"ドライバーナンバーの入力は3桁指定です。再入力お願いします。"];
                }
            }else{
                [self messageError:@"" message:@"未入力項目があります。\n再入力お願いします。"];
            }
        }else{
            [self messageError:@"" message:@"未入力項目があります。\n再入力お願いします。"];
        }
    }else{
        [self messageError:@"" message:@"未入力項目があります。\n再入力お願いします。"];
    }
}

//メールアドレスドメインチェック
- (BOOL)checkMailAdressCheck {
    
    BOOL bln_check = false;
    for (long i = 0; i < [_array_CarrierDomains count]; i++) {
        
        NSRange rng_chk = [self.meil_Text.text rangeOfString:[_array_CarrierDomains objectAtIndex:i] options:NSBackwardsSearch];
        
        if (rng_chk.location == NSNotFound) {
        }else{
            bln_check = true;
        }
    }
    return bln_check;
}

- (void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"")
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//バックアクション
- (void)lostAcountSet_totalBackActionView {
    
    //フォームのクローズ
    [self.navigationController popToViewController:_loginrootView animated:NO];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        //キャリアメール一覧取得
        _array_CarrierDomains = arrayData;
    }else{
        
        //フォームのクローズ
        [self.navigationController popToViewController:_loginrootView animated:NO];
    }
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
