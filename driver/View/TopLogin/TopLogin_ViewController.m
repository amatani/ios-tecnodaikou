//
//  TopLogin_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2015/12/04.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "TopLogin_ViewController.h"

@interface TopLogin_ViewController ()
@end

@implementation TopLogin_ViewController

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ナビゲーション非表示
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    //api初期化
    _api = [[Api alloc]init];

    txt_token.text = [Configuration getDeviceTokenKey];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    self.version_lbl.text = [NSString stringWithFormat:@"Ver.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"BundleUseInfoVersion"]];
    
    [super viewWillAppear:animated];
    
    //通知件数初期化
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    //apiDelegate設定
    _api.apidelegate = self;
    
    //アカウント状態チェック
    if(![[Configuration getSessionTokenKey] isEqualToString:@""]){
        [_api Api_LoginSessionCheck:self];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

- (IBAction)LoginInputPush:(id)sender {
    
    //ログイン画面へ
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TopLogin_ViewController" bundle:[NSBundle mainBundle]];
    Login_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"Login"];
    initialViewController.Login_delegate = self;
    initialViewController.loginrootView = self;
    initialViewController.api = _api;
    
    [self.navigationController pushViewController:initialViewController animated:NO];
}

- (IBAction)NewLoginPush:(id)sender {
    
    //新規アカウント作成画面へ
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TopLogin_ViewController" bundle:[NSBundle mainBundle]];
    NewAcount_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"NewAcount"];
    initialViewController.NewAcount_delegate = self;
    initialViewController.loginrootView = self;
    initialViewController.api = _api;
    
    [self.navigationController pushViewController:initialViewController animated:NO];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"Login"]) {
        Login_ViewController *viewCon = segue.destinationViewController;
        viewCon.Login_delegate = self;
        viewCon.loginrootView = self;
    }
    
    if ([segue.identifier isEqualToString:@"NewAcount"]) {
        NewAcount_ViewController *viewCon = segue.destinationViewController;
        viewCon.NewAcount_delegate = self;
    }
}

//バックアクション
//ログイン画面からのメイン画面遷移アクション
- (void)login_LoginFromMain_backActionView {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SideMenu_ViewController" bundle:[NSBundle mainBundle]];
    SideMenu_ViewController *initialViewController = [storyboard instantiateInitialViewController];
    initialViewController.loginrootView = self;
    
    [self.navigationController pushViewController:initialViewController animated:NO];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];

    if(FLG == YES){
        
        //ログインチェックでの自動ログイン
        [self login_LoginFromMain_backActionView];
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_LoginSessionCheck:self];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
