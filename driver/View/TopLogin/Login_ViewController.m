//
//  Login_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2016/01/08.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "Login_ViewController.h"

@interface Login_ViewController () {
    
    UITextField *Set_TextField;
}
@end

@implementation Login_ViewController

@synthesize Login_delegate = _Login_delegate;
@synthesize loginrootView = _loginrootView;
@synthesize api = _api;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    self.version_lbl.text = [NSString stringWithFormat:@"Ver.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"BundleUseInfoVersion"]];
    
    //親Viewセットチェック
    if(_loginrootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_loginrootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    if([Configuration getIDSave] == YES){
        self.MailOrDriverNO_Text.text = [Configuration getID];
    }
    
    //ツールバーを生成（メールアドレス）
    UIToolbar *MailOrDriverNO_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    MailOrDriverNO_Text_toolBar.barStyle = UIBarStyleDefault;
    [MailOrDriverNO_Text_toolBar sizeToFit];
    UIBarButtonItem *MailOrDriverNO_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_MailOrDriverNO_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(MailOrDriverNO_Text_toolBar_toolBar_closeKeyboard:)];
    NSArray *MailOrDriverNO_Text_toolBar_toolBarItems = [NSArray arrayWithObjects:MailOrDriverNO_Text_spacer, _MailOrDriverNO_Text_commitBtn, nil];
    [MailOrDriverNO_Text_toolBar setItems:MailOrDriverNO_Text_toolBar_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.MailOrDriverNO_Text.inputAccessoryView = MailOrDriverNO_Text_toolBar;
    
    //ツールバーを生成（メールアドレス）
    UIToolbar *Password_Text_toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Configuration getScreenWidth], 44)];
    Password_Text_toolBar.barStyle = UIBarStyleDefault;
    [Password_Text_toolBar sizeToFit];
    UIBarButtonItem *Password_Text_spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *_Password_Text_commitBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(Password_Text_toolBar_toolBar_closeKeyboard:)];
    NSArray *Password_Text_toolBarItems = [NSArray arrayWithObjects:Password_Text_spacer, _Password_Text_commitBtn, nil];
    [Password_Text_toolBar setItems:Password_Text_toolBarItems animated:YES];
    // ToolbarをTextViewのinputAccessoryViewに設定
    self.Password_Text.inputAccessoryView = Password_Text_toolBar;
    
    //テキストフィールド入力時の起動メソッド設定
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    //IDSave設定
    [self setIDSaveForm];
}

- (void)viewDidAppear:(BOOL)animated {

    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //スクロールを制限
    if(scrollView.contentOffset.y < 0){
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
    }
}

-(void)setIDSaveForm {
    
    self.IDSave_Sw.on = [Configuration getIDSave];
}

/////////////// ↓ テキスト入力関連 ↓ ////////////////////
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    Set_TextField = textField;
    return YES;
}

//テキストフィールドを編集する直後に呼び出されます
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    NSLog(@"textFieldDidBeginEditing");
}

-(void)keyboardWillShow:(NSNotification*)note {
    
    // キーボードの表示開始時の場所と大きさを取得します。
    CGRect keyboardFrameBegin = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if(Set_TextField == self.MailOrDriverNO_Text){
        int pointY = ([Configuration getScreenHeight]/2 - self.ModalFrameView.frame.size.height/2) + self.MailOrDriverNO_Text.frame.origin.y + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
    
    if(Set_TextField == self.Password_Text){
        int pointY = ([Configuration getScreenHeight]/2 - self.ModalFrameView.frame.size.height/2) + self.Password_Text.frame.origin.y + 30 + 44 - ([Configuration getScreenHeight] - keyboardFrameBegin.size.height);
        if(pointY > 0){
            self.MainScrollView.contentOffset = CGPointMake( 0, pointY);
        }else{
            self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    
    if(textField == self.MailOrDriverNO_Text){
        //キーボードを隠す
        [self.MailOrDriverNO_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    if(textField == self.Password_Text){
        //キーボードを隠す
        [self.Password_Text resignFirstResponder];
        self.MainScrollView.contentOffset = CGPointMake( 0, 0);
        return YES;
    }
    
    return NO;
}

-(void)textFieldDidEndEditing:(UITextField*)textField {
    
    if(textField == self.MailOrDriverNO_Text){
        if(![self.MailOrDriverNO_Text.text isEqualToString:@""]){
            NSRange range = [self.MailOrDriverNO_Text.text rangeOfString:@"@"];
            if (range.location == NSNotFound) {
                //@マーク無しで指定文字以下の場合に０を付ける
                for(long c = self.MailOrDriverNO_Text.text.length;c < [app loginNoColumCount];c++){
                    
                    self.MailOrDriverNO_Text.text = [NSString stringWithFormat:@"%@%@", @"0", self.MailOrDriverNO_Text.text];
                }
            }
        }
    }
}
/////////////// ↑ テキスト入力関連 ↑ ////////////////////

/////////////// ↓ テキスト入補助 ↓ ////////////////////
-(void)MailOrDriverNO_Text_toolBar_toolBar_closeKeyboard:(id)sender{
    
    [self.MailOrDriverNO_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}

-(void)Password_Text_toolBar_toolBar_closeKeyboard:(id)sender{
    
    [self.Password_Text resignFirstResponder];
    self.MainScrollView.contentOffset = CGPointMake( 0, 0);
}
/////////////// ↑ テキスト入力補助 ↑ ////////////////////

- (IBAction)LoginPush:(id)sender {
    
    if(![self.MailOrDriverNO_Text.text isEqualToString:@""]){
        if([InputCheck check_Password:self.Password_Text.text]){
            
            //メールアドレスでログイン
            if(![_api Api_LoginCheck:self loginID:self.MailOrDriverNO_Text.text password:self.Password_Text.text]){
                
                //エラーメッセージ用トースト
                [self.view makeToast:@"Not Set ViewController."];
            }

        }else{
            
            [self messageError:@"" message:@"PWは英数字4桁指定です。再入力お願いします。"];
        }
    }else{
        
        [self messageError:@"" message:@"未入力項目があります。\n再入力お願いします。"];
    }
}

- (IBAction)PasswordLostPush:(id)sender {
    
    //アカウント再設定画面
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TopLogin_ViewController" bundle:[NSBundle mainBundle]];
    LostAcount_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"LostAcount"];
    initialViewController.LostAcount_delegate = self;
    initialViewController.loginrootView = self;
    initialViewController.loginSetView = self;
    initialViewController.api = _api;
    initialViewController.str_BackButtonText = @"スタートページに戻る";
    
    [self.navigationController pushViewController:initialViewController animated:NO];
}

- (IBAction)CanselPush:(id)sender {
    
    //前の画面に戻る
    [self.navigationController popToViewController:_loginrootView animated: NO];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"LostAcount"]) {
        LostAcount_ViewController *viewCon = segue.destinationViewController;
        viewCon.LostAcount_delegate = self;
        viewCon.str_BackButtonText = @"スタートページに戻る";
        
        //画面の表示を消す
        self.MainScrollView.hidden = true;
    }
}

- (IBAction)IDSaveSwitch:(id)sender {
    
    if([Configuration getIDSave] == true){
        [Configuration setIDSave:false];
    }else{
        [Configuration setIDSave:true];
    }
    
    [self setIDSaveForm];
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//バックアクション
//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        //ログインID保存
        [Configuration setID:self.MailOrDriverNO_Text.text];
        //パスワード保存
        [Configuration setPassword:self.Password_Text.text];
        
        if ([_Login_delegate respondsToSelector:@selector(login_LoginFromMain_backActionView)]){
            [_Login_delegate login_LoginFromMain_backActionView];
        }
        [self dismissViewControllerAnimated:NO completion:nil];
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_LoginCheck:self loginID:self.MailOrDriverNO_Text.text password:self.Password_Text.text];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Dialog_API_LoginErr401TitleMsg",@"")
                                                message:NSLocalizedString(@"Dialog_API_LoginErr401Msg",@"")
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_API_LoginErr401OK",@"")
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
