//
//  TopLogin_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2015/12/04.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "NewAcount_ViewController.h"
#import "Login_ViewController.h"
#import "SideMenu_ViewController.h"

@interface TopLogin_ViewController : UIViewController <ApiDelegate, NewAcount_ViewControllerDelegate, Login_ViewControllerDelegate>
{
    Api* _api;

    __weak IBOutlet UITextField *txt_token;
}
- (IBAction)LoginInputPush:(id)sender;
- (IBAction)NewLoginPush:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *version_lbl;

@end
