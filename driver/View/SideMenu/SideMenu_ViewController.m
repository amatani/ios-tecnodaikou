//
//  SideMenu_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2015/12/23.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "SideMenu_ViewController.h"

@interface SideMenu_ViewController ()
{
    float flt_ScreenWidth;
    float flt_ScreenHeight;
    UITableViewController *menuTableViewController;
    BOOL bln_menu1flg;
    BOOL bln_menu2flg;
    BOOL bln_menu6flg;
}
@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, retain) IBOutlet UIView *dummyView;
@end

@implementation SideMenu_ViewController

@synthesize SideMenu_delegate = _SideMenu_delegate;
@synthesize loginrootView = _loginrootView;

@synthesize mainMenu_ViewController_vc = _mainMenu_ViewController_vc;

@synthesize isOpen =_isOpen;
@synthesize dummyView = _dummyView;

- (void)viewDidLoad {

    NSString* className = NSStringFromClass([SideMenu_ViewController class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    bln_menu1flg = false;
    bln_menu2flg = false;
    bln_menu6flg = false;
    
    //画面取得
    UIScreen *sc = [UIScreen mainScreen];
    //ステータスバー込みのサイズ
    CGRect rect = sc.applicationFrame;
    NSLog(@"%.1f, %.1f", rect.size.width, rect.size.height);
    //ipad width 768
    //iphone width 320
    flt_ScreenWidth = rect.size.width;
    flt_ScreenHeight = rect.size.height;
    
    menuTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KTMenuTalbeViewController"];
    menuTableViewController.tableView.delegate = self;
    [menuTableViewController.view setFrame:_dummyView.frame];
    [self addChildViewController:menuTableViewController];
    [menuTableViewController didMoveToParentViewController:self];
    menuTableViewController.view.frame = CGRectMake(0,63,flt_ScreenWidth * 0.6,flt_ScreenHeight - 43);
    [self.view addSubview:menuTableViewController.view];
    
    //navigationController
    UINavigationController *navigationController = [[UINavigationController alloc] init];
    [self addChildViewController:navigationController];
    [navigationController didMoveToParentViewController:self];
    [self.view addSubview:navigationController.view];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainMenu_ViewController" bundle:[NSBundle mainBundle]];
    _mainMenu_ViewController_vc = [storyboard instantiateViewControllerWithIdentifier:@"MainMenu_ViewController"];
    _mainMenu_ViewController_vc.loginrootView = _loginrootView;
    [self setFirstViewController:_mainMenu_ViewController_vc];
    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([SideMenu_ViewController class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([SideMenu_ViewController class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([SideMenu_ViewController class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([SideMenu_ViewController class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([SideMenu_ViewController class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([SideMenu_ViewController class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - setFirstViewController
-(void)setFirstViewController:(UIViewController *)viewController {
    
    UINavigationController *navigationController = [self.childViewControllers lastObject];
    [navigationController setViewControllers:[NSArray arrayWithObjects:viewController, nil]];
}

-(void)removeFirstViewController {
    
    UINavigationController *navigationController = [self.childViewControllers lastObject];
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:navigationController.viewControllers];
    [viewControllers removeAllObjects];
    navigationController.viewControllers = viewControllers;
}

#pragma mark - UITableViewControllerDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    if(bln_menu1flg == true){
                        return 0;
                    }else{
                        return 44;
                    }
                    break;
                case 1:
                    if(bln_menu1flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 2:
                    if(bln_menu1flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 3:
                    if(bln_menu1flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 4:
                    if(bln_menu1flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
            }
            break;
            
        case 1:
            return 44;
            break;
            
        case 2:
            return 44;
            break;
            
        case 3:

            switch (indexPath.row) {
                case 0:
                    if(bln_menu6flg == true){
                        return 0;
                    }else{
                        return 44;
                    }
                    break;
                case 1:
                    if(bln_menu6flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 2:
                    if(bln_menu6flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 3:
                    if(bln_menu6flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 4:
                    if(bln_menu6flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 5:
                    if(bln_menu6flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 6:
                    if(bln_menu6flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
                case 7:
                    if(bln_menu6flg == true){
                        return 44;
                    }else{
                        return 0;
                    }
                    break;
            }
            break;
            
        case 4:
            return 44;
            break;
    }
    
    
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    bln_menu1flg = true;
                    [menuTableViewController.tableView reloadData];
                    break;
                case 1:
                    bln_menu1flg = false;
                    [menuTableViewController.tableView reloadData];
                    break;
                case 2:
                    
                    //メインメニューをセット
                    [self setFirstViewController:_mainMenu_ViewController_vc];
                    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
                    
                    //スライドメニューを初期値へ
                    [self slide:nil];
                    
                    //今週画面遷移
                    [_mainMenu_ViewController_vc konsyuYotei];
                    
                    break;
                case 3:

                    //メインメニューをセット
                    [self setFirstViewController:_mainMenu_ViewController_vc];
                    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
                    
                    //スライドメニューを初期値へ
                    [self slide:nil];
                    
                    //出社予定登録画面設定
                    [_mainMenu_ViewController_vc YokusyuYotei];
                    
                    break;
                case 4:
                    
                    //メインメニューをセット
                    [self setFirstViewController:_mainMenu_ViewController_vc];
                    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
                    
                    //スライドメニューを初期値へ
                    [self slide:nil];
                    
                    //基本時間設定画面
                    [_mainMenu_ViewController_vc defaultTime];
                    
                    break;
            }
            break;
        case 1:
            
            //メインメニューをセット
            [self setFirstViewController:_mainMenu_ViewController_vc];
            [_mainMenu_ViewController_vc setMainMenu_delegate:self];
            
            //スライドメニューを初期値へ
            [self slide:nil];

            //重要お知らせ表示
            [_mainMenu_ViewController_vc jyuyouoshirase_viewSet];
            break;
            
        case 2:
            
            //メインメニューをセット
            [self setFirstViewController:_mainMenu_ViewController_vc];
            [_mainMenu_ViewController_vc setMainMenu_delegate:self];
            
            //スライドメニューを初期値へ
            [self slide:nil];
            
            //お知らせ表示
            [_mainMenu_ViewController_vc oshirase_viewSet];
            break;
            
        case 3:

            switch (indexPath.row) {
                case 0:
                    bln_menu6flg = true;
                    [menuTableViewController.tableView reloadData];
                    break;
                case 1:
                    bln_menu6flg = false;
                    [menuTableViewController.tableView reloadData];
                    break;
                case 2:
                    
                    //メインメニューをセット
                    [self setFirstViewController:_mainMenu_ViewController_vc];
                    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
                    
                    //スライドメニューを初期値へ
                    [self slide:nil];
                    
                    //基本時間設定画面
                    [_mainMenu_ViewController_vc defaultTime];
                    
                    break;
                case 3:
                    
                    //メインメニューをセット
                    [self setFirstViewController:_mainMenu_ViewController_vc];
                    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
                    
                    //スライドメニューを初期値へ
                    [self slide:nil];
                    
                    //プッシュ設定画面
                    [_mainMenu_ViewController_vc pushSetting];
                    
                    break;
                case 4:
                    
                    //メインメニューをセット
                    [self setFirstViewController:_mainMenu_ViewController_vc];
                    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
                    
                    //スライドメニューを初期値へ
                    [self slide:nil];
                    
                    //メールアドレス変更設定画面
                    [_mainMenu_ViewController_vc mailChecge];
                    
                    break;
                case 5:
                    
                    //メインメニューをセット
                    [self setFirstViewController:_mainMenu_ViewController_vc];
                    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
                    
                    //スライドメニューを初期値へ
                    [self slide:nil];
                    
                    //パスワード変更設定画面
                    [_mainMenu_ViewController_vc passwordReset];
                    
                    break;
                case 6:
                    
                    //メインメニューをセット
                    [self setFirstViewController:_mainMenu_ViewController_vc];
                    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
                    
                    //スライドメニューを初期値へ
                    [self slide:nil];
                    
                    //パスワード変更設定画面
                    [_mainMenu_ViewController_vc apliInfo];
                    
                    break;
                case 7:
                    
                    break;
            }
            break;
            
        case 4:
            
            //メインメニューをセット
            [self setFirstViewController:_mainMenu_ViewController_vc];
            [_mainMenu_ViewController_vc setMainMenu_delegate:self];
            
            //スライドメニューを初期値へ
            [self slide:nil];
            
            // ログイン画面に戻る
            [_mainMenu_ViewController_vc logout];
            
            break;
    }
}

#pragma mark - slide
-(void)slide:(id)sender {
    
    [self moveAction_Home];
}

- (IBAction)btn_home:(id)sender {
    
    //メインメニューをセット
    [self setFirstViewController:_mainMenu_ViewController_vc];
    [_mainMenu_ViewController_vc setMainMenu_delegate:self];
    
    //メイン画面のシャドーを削除
    [_mainMenu_ViewController_vc shadowKill];
    
    [self slide:nil];
}

- (void)mainMenu_BackActionView {

    [self moveAction_Home];
}

- (void)moveAction_Home {
    
    _isOpen = !_isOpen;
    
    UINavigationController *firstNavigationController = [self.childViewControllers objectAtIndex:1];
    UIView *firstView = firstNavigationController.view;
    [UIView animateWithDuration:0.6 animations:^{
        CGFloat originX = _isOpen ? flt_ScreenWidth * 0.6 : 0;
        CGRect frame = firstView.frame;
        frame.origin.x = originX;
        firstView.frame = frame;
    } completion:^(BOOL finished){
        
    }];
}

//バックアクション
- (void)mainMenu_LoginBackAction {
 
    // ログイン画面に戻る
    [self.navigationController popToViewController:_loginrootView animated:YES];
}
- (void)BasicTimeSetting_backActionView:(long)startCount EndCount:(long)endCount {
    
    NSLog(@"%ld" , startCount);
    NSLog(@"%ld" , endCount);
}

@end
