//
//  SideMenu_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2015/12/23.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "MainMenu_ViewController.h"

@protocol SideMenu_ViewControllerDelegate<NSObject>
- (void)SideMenu_backActionView;
@end

@interface SideMenu_ViewController : UIViewController <UITableViewDelegate, MainMenu_ViewControllerDelegate>
{
    id<SideMenu_ViewControllerDelegate> _SideMenu_delegate;
    UIViewController* _loginrootView;
}
@property (nonatomic) id<SideMenu_ViewControllerDelegate> SideMenu_delegate;
@property (nonatomic) UIViewController* loginrootView;

@property (nonatomic) MainMenu_ViewController* mainMenu_ViewController_vc;

- (IBAction)btn_home:(id)sender;

@end
