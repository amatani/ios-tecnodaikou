//
//  SyussyaYoyakuKakuteiWeek_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/27.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

@protocol SyussyaYoyakuKakuteiWeek_ViewControllerDelegate <NSObject>
- (void)SyussyaYoyakuKakuteiWeek_LoginBackActionView;
@end

@interface SyussyaYoyakuKakuteiWeek_ViewController : UIViewController <ApiDelegate> {
    
    id<SyussyaYoyakuKakuteiWeek_ViewControllerDelegate> _SyussyaYoyakuKakuteiWeek_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    __weak IBOutlet UITextView* _Kekka_txt;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    long _lng_WeekType;
}
@property (nonatomic) id<SyussyaYoyakuKakuteiWeek_ViewControllerDelegate> SyussyaYoyakuKakuteiWeek_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
@property (nonatomic) long lng_WeekType;

- (IBAction)LeftButton:(id)sender;

@end
