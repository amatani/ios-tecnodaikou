//
//  SyussyaYoyaku_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2015/12/27.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "SyussyaYoyakuWeek_ViewController.h"
#import "MainMenu_ViewController.h"

@interface SyussyaYoyakuWeek_ViewController ()
@end

@implementation SyussyaYoyakuWeek_ViewController

@synthesize SyussyaYoyakuWeek_delegate = _SyussyaYoyakuWeek_delegate;
@synthesize rootView = _rootView;
@synthesize api = _api;

//引き継がれるパラメータ
@synthesize lng_WeekType = _lng_WeekType;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
    
    //時間セットから戻った時の再セット無効化フラグ
    _bln_reloadFlg = true;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIFont *font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/25];
    self.Day1Day_lbl.font = font;
    self.Day2Day_lbl.font = font;
    self.Day3Day_lbl.font = font;
    self.Day4Day_lbl.font = font;
    self.Day5Day_lbl.font = font;
    self.Day6Day_lbl.font = font;
    self.Day7Day_lbl.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/13];
    self.Day1Week_lbl.font = font;
    self.Day2Week_lbl.font = font;
    self.Day3Week_lbl.font = font;
    self.Day4Week_lbl.font = font;
    self.Day5Week_lbl.font = font;
    self.Day6Week_lbl.font = font;
    self.Day7Week_lbl.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/12];
    self.Day1Work_lbl.font = font;
    self.Day2Work_lbl.font = font;
    self.Day3Work_lbl.font = font;
    self.Day4Work_lbl.font = font;
    self.Day5Work_lbl.font = font;
    self.Day6Work_lbl.font = font;
    self.Day7Work_lbl.font = font;
    self.Day1Holiday_lbl.font = font;
    self.Day2Holiday_lbl.font = font;
    self.Day3Holiday_lbl.font = font;
    self.Day4Holiday_lbl.font = font;
    self.Day5Holiday_lbl.font = font;
    self.Day6Holiday_lbl.font = font;
    self.Day7Holiday_lbl.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W3" size:[Configuration getScreenWidth]/30];
    self.Day1TimeFrom_lbl.font = font;
    self.Day2TimeFrom_lbl.font = font;
    self.Day3TimeFrom_lbl.font = font;
    self.Day4TimeFrom_lbl.font = font;
    self.Day5TimeFrom_lbl.font = font;
    self.Day6TimeFrom_lbl.font = font;
    self.Day7TimeFrom_lbl.font = font;
    self.Day1TimeTo_lbl.font = font;
    self.Day2TimeTo_lbl.font = font;
    self.Day3TimeTo_lbl.font = font;
    self.Day4TimeTo_lbl.font = font;
    self.Day5TimeTo_lbl.font = font;
    self.Day6TimeTo_lbl.font = font;
    self.Day7TimeTo_lbl.font = font;
    
    //カレンダーライブラリ用初期化
    _calHoliday = [[Wheel_Callender_Library alloc]init];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    //初期値の設定
    if(_bln_reloadFlg == true){
        
        //初期値設定
        [self setFirstValue];
        
    }else{
        
        _bln_reloadFlg = true;
    }
}

//初期値の設定
- (void)setFirstValue {
    
    //値の初期値設定
    _bln_WeekDay1State = true;
    _bln_WeekDay2State = true;
    _bln_WeekDay3State = true;
    _bln_WeekDay4State = true;
    _bln_WeekDay5State = true;
    _bln_WeekDay6State = true;
    _bln_WeekDay7State = true;
    
    _str_WeekDay1TimeStart = @"";
    _str_WeekDay1TimeEnd = @"";
    _str_WeekDay2TimeStart = @"";
    _str_WeekDay2TimeEnd = @"";
    _str_WeekDay3TimeStart = @"";
    _str_WeekDay3TimeEnd = @"";
    _str_WeekDay4TimeStart = @"";
    _str_WeekDay4TimeEnd = @"";
    _str_WeekDay5TimeStart = @"";
    _str_WeekDay5TimeEnd = @"";
    _str_WeekDay6TimeStart = @"";
    _str_WeekDay6TimeEnd = @"";
    _str_WeekDay7TimeStart = @"";
    _str_WeekDay7TimeEnd = @"";
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    //現在の日付保存
    _dt_NowDate = [NSDate dateWithTimeIntervalSinceNow:(60*60*9)];
    
    //今週来週の切り替え
    switch (_lng_WeekType) {
        case 0:
            self.SetWeekTitle_lbl.text = @"";
            self.NextWeek_View.hidden = true;
            self.BackWeek_View.hidden = true;
            self.OK_Push_lbl.text = @"";
            
            break;
            
        case 1:
            self.SetWeekTitle_lbl.text = @"今週の予定登録";
            self.NextWeek_View.hidden = false;
            self.BackWeek_View.hidden = true;
            self.OK_Push_lbl.text = @"入力内容確認";
            
            //12時間表記対策
            [format setLocale:[NSLocale systemLocale]];
            //タイムゾーンの指定
            [format setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
            [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
            [format setDateFormat:@"yyyy-MM-dd"];
        
            [_api Api_ScheduleCallenderDayGetting:self StarDate:[format stringFromDate:_dt_NowDate]];
            break;
            
        case 2:
            self.SetWeekTitle_lbl.text = @"翌週の予定登録";
            self.NextWeek_View.hidden = true;
            self.BackWeek_View.hidden = false;
            self.OK_Push_lbl.text = @"入力内容確認";
            
            //日付取得
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *comp = [[NSDateComponents alloc] init];
            comp.day = 7;
            NSDate *up7Date = [calendar dateByAddingComponents:comp toDate:_dt_NowDate options:0];
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            //12時間表記対策
            [format setLocale:[NSLocale systemLocale]];
            //タイムゾーンの指定
            [format setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
            [format setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
            [format setDateFormat:@"yyyy-MM-dd"];
            
            [_api Api_ScheduleCallenderDayGetting:self StarDate:[format stringFromDate:up7Date]];
            break;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

//日付の設定
- (void)setDay {
    
//    self.lbl_Syuu.text = [_calHoliday getWeek:[_calHoliday yearNoGet:_dt_wday1] tMonth:[_calHoliday monthNoGet:_dt_wday1] tDay:[_calHoliday dayNoGet:_dt_wday1]];
    if(_ary_months.count == 1){
        
        long lng_month = [[_ary_months objectAtIndex:0] longValue];
        long lng_week = [[_ary_weeks objectAtIndex:0] longValue];
        self.lbl_Syuu.text = [NSString stringWithFormat : @"%ld月第%ld週目", lng_month, lng_week];
    }else if(_ary_months.count == 2){
        
        long lng_month1 = [[_ary_months objectAtIndex:0] longValue];
        long lng_week1 = [[_ary_weeks objectAtIndex:0] longValue];
        long lng_month2 = [[_ary_months objectAtIndex:1] longValue];
        long lng_week2 = [[_ary_weeks objectAtIndex:1] longValue];
        self.lbl_Syuu.text = [NSString stringWithFormat : @"%ld月第%ld週目\n%ld月第%ld週目", lng_month1, lng_week1, lng_month2, lng_week2];
    }
    
    self.Day1Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday1]];
    self.Day2Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday2]];
    self.Day3Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday3]];
    self.Day4Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday4]];
    self.Day5Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday5]];
    self.Day6Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday6]];
    self.Day7Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday7]];
    
    NSString *str_Backcolor_Weekday = @"ffffff";
    NSString *str_Backcolor_Holiday = @"FF33FF";
    NSString *str_Character_Weekday = @"FF33FF";
    NSString *str_Character_Holiday = @"ffffff";
    
    //曜日設定
    //１日目
    switch ([_calHoliday weekdayNoGet:_dt_wday1]) {
        case 1:
            _Day1Week_lbl.text = @"日";
            break;
        case 2:
            _Day1Week_lbl.text = @"月";
            break;
        case 3:
            _Day1Week_lbl.text = @"火";
            break;
        case 4:
            _Day1Week_lbl.text = @"水";
            break;
        case 5:
            _Day1Week_lbl.text = @"木";
            break;
        case 6:
            _Day1Week_lbl.text = @"金";
            break;
        case 7:
            _Day1Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday1] tMonth:[_calHoliday monthNoGet:_dt_wday1] tDay:[_calHoliday dayNoGet:_dt_wday1]]) || ([_calHoliday weekdayNoGet:_dt_wday1] == 1)){
        //日曜のみ色を変える
        _Day1Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day1DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day1Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day1Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day1DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day1Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //２日目
    switch ([_calHoliday weekdayNoGet:_dt_wday2]) {
        case 1:
            _Day2Week_lbl.text = @"日";
            break;
        case 2:
            _Day2Week_lbl.text = @"月";
            break;
        case 3:
            _Day2Week_lbl.text = @"火";
            break;
        case 4:
            _Day2Week_lbl.text = @"水";
            break;
        case 5:
            _Day2Week_lbl.text = @"木";
            break;
        case 6:
            _Day2Week_lbl.text = @"金";
            break;
        case 7:
            _Day2Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday2] tMonth:[_calHoliday monthNoGet:_dt_wday2] tDay:[_calHoliday dayNoGet:_dt_wday2]]) || ([_calHoliday weekdayNoGet:_dt_wday2] == 1)){
        //日曜のみ色を変える
        _Day2Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day2DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day2Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day2Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day2DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day2Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //３日目
    switch ([_calHoliday weekdayNoGet:_dt_wday3]) {
        case 1:
            _Day3Week_lbl.text = @"日";
            break;
        case 2:
            _Day3Week_lbl.text = @"月";
            break;
        case 3:
            _Day3Week_lbl.text = @"火";
            break;
        case 4:
            _Day3Week_lbl.text = @"水";
            break;
        case 5:
            _Day3Week_lbl.text = @"木";
            break;
        case 6:
            _Day3Week_lbl.text = @"金";
            break;
        case 7:
            _Day3Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday3] tMonth:[_calHoliday monthNoGet:_dt_wday3] tDay:[_calHoliday dayNoGet:_dt_wday3]]) || ([_calHoliday weekdayNoGet:_dt_wday3] == 1)){
        //日曜のみ色を変える
        _Day3Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day3DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day3Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day3Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day3DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day3Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //４日目
    switch ([_calHoliday weekdayNoGet:_dt_wday4]) {
        case 1:
            _Day4Week_lbl.text = @"日";
            break;
        case 2:
            _Day4Week_lbl.text = @"月";
            break;
        case 3:
            _Day4Week_lbl.text = @"火";
            break;
        case 4:
            _Day4Week_lbl.text = @"水";
            break;
        case 5:
            _Day4Week_lbl.text = @"木";
            break;
        case 6:
            _Day4Week_lbl.text = @"金";
            break;
        case 7:
            _Day4Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday4] tMonth:[_calHoliday monthNoGet:_dt_wday4] tDay:[_calHoliday dayNoGet:_dt_wday4]]) || ([_calHoliday weekdayNoGet:_dt_wday4] == 1)){
        //日曜のみ色を変える
        _Day4Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day4DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day4Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day4Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day4DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day4Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //５日目
    switch ([_calHoliday weekdayNoGet:_dt_wday5]) {
        case 1:
            _Day5Week_lbl.text = @"日";
            break;
        case 2:
            _Day5Week_lbl.text = @"月";
            break;
        case 3:
            _Day5Week_lbl.text = @"火";
            break;
        case 4:
            _Day5Week_lbl.text = @"水";
            break;
        case 5:
            _Day5Week_lbl.text = @"木";
            break;
        case 6:
            _Day5Week_lbl.text = @"金";
            break;
        case 7:
            _Day5Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday5] tMonth:[_calHoliday monthNoGet:_dt_wday5] tDay:[_calHoliday dayNoGet:_dt_wday5]]) || ([_calHoliday weekdayNoGet:_dt_wday5] == 1)){
        //日曜のみ色を変える
        _Day5Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day5DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day5Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day5Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day5DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day5Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //６日目
    switch ([_calHoliday weekdayNoGet:_dt_wday6]) {
        case 1:
            _Day6Week_lbl.text = @"日";
            break;
        case 2:
            _Day6Week_lbl.text = @"月";
            break;
        case 3:
            _Day6Week_lbl.text = @"火";
            break;
        case 4:
            _Day6Week_lbl.text = @"水";
            break;
        case 5:
            _Day6Week_lbl.text = @"木";
            break;
        case 6:
            _Day6Week_lbl.text = @"金";
            break;
        case 7:
            _Day6Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday6] tMonth:[_calHoliday monthNoGet:_dt_wday6] tDay:[_calHoliday dayNoGet:_dt_wday6]]) || ([_calHoliday weekdayNoGet:_dt_wday6] == 1)){
        //日曜のみ色を変える
        _Day6Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day6DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day6Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day6Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day6DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day6Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //７日目
    switch ([_calHoliday weekdayNoGet:_dt_wday7]) {
        case 1:
            _Day7Week_lbl.text = @"日";
            break;
        case 2:
            _Day7Week_lbl.text = @"月";
            break;
        case 3:
            _Day7Week_lbl.text = @"火";
            break;
        case 4:
            _Day7Week_lbl.text = @"水";
            break;
        case 5:
            _Day7Week_lbl.text = @"木";
            break;
        case 6:
            _Day7Week_lbl.text = @"金";
            break;
        case 7:
            _Day7Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday7] tMonth:[_calHoliday monthNoGet:_dt_wday7] tDay:[_calHoliday dayNoGet:_dt_wday7]]) || ([_calHoliday weekdayNoGet:_dt_wday7] == 1)){
        //日曜のみ色を変える
        _Day7Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day7DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day7Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day7Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day7DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day7Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
}

//日別の勤務状態設定
-(void)setDayWook {
    
    [self setDayWook:1 flg:_bln_WeekDay1State];
    [self setDayWook:2 flg:_bln_WeekDay2State];
    [self setDayWook:3 flg:_bln_WeekDay3State];
    [self setDayWook:4 flg:_bln_WeekDay4State];
    [self setDayWook:5 flg:_bln_WeekDay5State];
    [self setDayWook:6 flg:_bln_WeekDay6State];
    [self setDayWook:7 flg:_bln_WeekDay7State];
}

//日別の勤務状態背景色設定
-(void)setDayWook:(long)dayCount
              flg:(BOOL)flag {
    switch (dayCount) {
        case 1:
            if(_bln_WeekDay1Registration){
                
                if(flag == false){
                    self.Day1Work_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day1Work_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                    self.Day1Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day1Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                }else{
                    self.Day1Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day1Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                    self.Day1Holiday_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day1Holiday_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                }
            }else{
                self.Day1Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day1Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                self.Day1Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day1Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
            }
            break;
        case 2:
            if(_bln_WeekDay2Registration){
                if(flag == false){
                    self.Day2Work_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day2Work_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                    self.Day2Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day2Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                }else{
                    self.Day2Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day2Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                    self.Day2Holiday_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day2Holiday_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                }
            }else{
                self.Day2Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day2Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                self.Day2Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day2Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
            }
            break;
        case 3:
            if(_bln_WeekDay3Registration){
                if(flag == false){
                    self.Day3Work_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day3Work_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                    self.Day3Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day3Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                }else{
                    self.Day3Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day3Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                    self.Day3Holiday_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day3Holiday_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                }
            }else{
                self.Day3Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day3Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                self.Day3Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day3Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
            }
            break;
        case 4:
            if(_bln_WeekDay4Registration){
                if(flag == false){
                    self.Day4Work_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day4Work_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                    self.Day4Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day4Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                }else{
                    self.Day4Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day4Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                    self.Day4Holiday_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day4Holiday_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                }
            }else{
                self.Day4Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day4Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                self.Day4Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day4Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
            }
            break;
        case 5:
            if(_bln_WeekDay5Registration){
                if(flag == false){
                    self.Day5Work_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day5Work_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                    self.Day5Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day5Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                }else{
                    self.Day5Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day5Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                    self.Day5Holiday_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day5Holiday_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                }
            }else{
                self.Day5Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day5Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                self.Day5Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day5Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
            }
            break;
        case 6:
            if(_bln_WeekDay6Registration){
                if(flag == false){
                    self.Day6Work_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day6Work_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                    self.Day6Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day6Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                }else{
                    self.Day6Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day6Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                    self.Day6Holiday_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day6Holiday_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                }
            }else{
                self.Day6Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day6Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                self.Day6Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day6Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
            }
            break;
        case 7:
            if(_bln_WeekDay7Registration){
                if(flag == false){
                    self.Day7Work_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day7Work_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                    self.Day7Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day7Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                }else{
                    self.Day7Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                    self.Day7Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                    self.Day7Holiday_View.backgroundColor = [UIColor colorWithHex:@"0F5B86"];
                    self.Day7Holiday_lbl.textColor = [UIColor colorWithHex:@"FFFFFF"];
                }
            }else{
                self.Day7Work_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day7Work_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
                self.Day7Holiday_View.backgroundColor = [UIColor colorWithHex:@"ececec"];
                self.Day7Holiday_lbl.textColor = [UIColor colorWithHex:@"c2c2c2"];
            }
            break;
    }
}

//時間の設定
- (void)setTime {

    if([_str_WeekDay1TimeStart isEqualToString:@""]){
        self.Day1TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay1TimeStart];
        self.Day1TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay1TimeEnd isEqualToString:@""]){
        self.Day1TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay1TimeEnd isEqualToString:@"08:00"]){
        self.Day1TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay1TimeEnd];
        self.Day1TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay2TimeStart isEqualToString:@""]){
        self.Day2TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay2TimeStart];
        self.Day2TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay2TimeEnd isEqualToString:@""]){
        self.Day2TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay2TimeEnd isEqualToString:@"08:00"]){
        self.Day2TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay2TimeEnd];
        self.Day2TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay3TimeStart isEqualToString:@""]){
        self.Day3TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay3TimeStart];
        self.Day3TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay3TimeEnd isEqualToString:@""]){
        self.Day3TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay3TimeEnd isEqualToString:@"08:00"]){
        self.Day3TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay3TimeEnd];
        self.Day3TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay4TimeStart isEqualToString:@""]){
        self.Day4TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay4TimeStart];
        self.Day4TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay4TimeEnd isEqualToString:@""]){
        self.Day4TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay4TimeEnd isEqualToString:@"08:00"]){
        self.Day4TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay4TimeEnd];
        self.Day4TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay5TimeStart isEqualToString:@""]){
        self.Day5TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay5TimeStart];
        self.Day5TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay5TimeEnd isEqualToString:@""]){
        self.Day5TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay5TimeEnd isEqualToString:@"08:00"]){
        self.Day5TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay5TimeEnd];
        self.Day5TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay6TimeStart isEqualToString:@""]){
        self.Day6TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay6TimeStart];
        self.Day6TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay6TimeEnd isEqualToString:@""]){
        self.Day6TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay6TimeEnd isEqualToString:@"08:00"]){
        self.Day6TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay6TimeEnd];
        self.Day6TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
    
    if([_str_WeekDay7TimeStart isEqualToString:@""]){
        self.Day7TimeFrom_lbl.text = @"未定";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay7TimeStart];
        self.Day7TimeFrom_lbl.text = [ary_time objectAtIndex:1];
    }
    if([_str_WeekDay7TimeEnd isEqualToString:@""]){
        self.Day7TimeTo_lbl.text = @"未定";
    }else if([_str_WeekDay7TimeEnd isEqualToString:@"08:00"]){
        self.Day7TimeTo_lbl.text = @"最終";
    }else{
        NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay7TimeEnd];
        self.Day7TimeTo_lbl.text = [ary_time objectAtIndex:1];
    }
}

- (NSString *)createStringAddedCommaFromInt:(long)number
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    [format setGroupingSeparator:@","];
    [format setGroupingSize:3];
    
    return [format stringForObjectValue:[NSNumber numberWithLong:number]];
}

- (IBAction)LeftButton:(id)sender {
    
    BOOL bln_CheckOnFLG = false;
    //勤務を変更した場合のチェック
    if(_bln_initial_WeekDay1Registration != _bln_WeekDay1Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay2Registration != _bln_WeekDay2Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay3Registration != _bln_WeekDay3Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay4Registration != _bln_WeekDay4Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay5Registration != _bln_WeekDay5Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay6Registration != _bln_WeekDay6Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay7Registration != _bln_WeekDay7Registration){
        bln_CheckOnFLG = true;
    }
    
    //休み・勤務の方法変更チェック
    if(_bln_WeekDay1State != _bln_initial_WeekDay1State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay2State != _bln_initial_WeekDay2State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay3State != _bln_initial_WeekDay3State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay4State != _bln_initial_WeekDay4State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay5State != _bln_initial_WeekDay5State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay6State != _bln_initial_WeekDay6State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay7State != _bln_initial_WeekDay7State){
        bln_CheckOnFLG = true;
    }
    
    //時間の変更チェック
    if(![_str_WeekDay1TimeStart isEqualToString:_str_initial_WeekDay1TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay2TimeStart isEqualToString:_str_initial_WeekDay2TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay3TimeStart isEqualToString:_str_initial_WeekDay3TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay4TimeStart isEqualToString:_str_initial_WeekDay4TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay5TimeStart isEqualToString:_str_initial_WeekDay5TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay6TimeStart isEqualToString:_str_initial_WeekDay6TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay7TimeStart isEqualToString:_str_initial_WeekDay7TimeStart]){
        bln_CheckOnFLG = true;
    }
    
    if(bln_CheckOnFLG == true){
        
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:nil
                                            message:@"今週の予定を確定させていません。このままホームに移動しますか？"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    
                                                    // メイン画面に戻る
                                                    [self.navigationController popToViewController:_rootView animated:YES];
                                                }]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        // メイン画面に戻る
        [self.navigationController popToViewController:_rootView animated:YES];
    }
}

- (IBAction)RightButton:(id)sender {
    
    BOOL bln_CheckOnFLG = false;
    //勤務を変更した場合のチェック
    if(_bln_initial_WeekDay1Registration != _bln_WeekDay1Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay2Registration != _bln_WeekDay2Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay3Registration != _bln_WeekDay3Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay4Registration != _bln_WeekDay4Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay5Registration != _bln_WeekDay5Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay6Registration != _bln_WeekDay6Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay7Registration != _bln_WeekDay7Registration){
        bln_CheckOnFLG = true;
    }
    
    //休み・勤務の方法変更チェック
    if(_bln_WeekDay1State != _bln_initial_WeekDay1State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay2State != _bln_initial_WeekDay2State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay3State != _bln_initial_WeekDay3State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay4State != _bln_initial_WeekDay4State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay5State != _bln_initial_WeekDay5State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay6State != _bln_initial_WeekDay6State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay7State != _bln_initial_WeekDay7State){
        bln_CheckOnFLG = true;
    }
    
    //時間の変更チェック
    if(![_str_WeekDay1TimeStart isEqualToString:_str_initial_WeekDay1TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay2TimeStart isEqualToString:_str_initial_WeekDay2TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay3TimeStart isEqualToString:_str_initial_WeekDay3TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay4TimeStart isEqualToString:_str_initial_WeekDay4TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay5TimeStart isEqualToString:_str_initial_WeekDay5TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay6TimeStart isEqualToString:_str_initial_WeekDay6TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay7TimeStart isEqualToString:_str_initial_WeekDay7TimeStart]){
        bln_CheckOnFLG = true;
    }
    
    if(bln_CheckOnFLG == true){
        
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:nil
                                            message:@"今週の予定を確定させていません。このまま基本時間変更ページに移動しますか？"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    
                                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                                                    TotalBasicTimeSetting_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"TotalBasicTimeSetting"];
                                                    initialViewController.TotalBasicTimeSetting_delegate = self;
                                                    initialViewController.rootView = _rootView;
                                                    initialViewController.secondView = self;
                                                    initialViewController.api = _api;
                                                    
                                                    [self.navigationController pushViewController:initialViewController animated:YES];
                                                }]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
        TotalBasicTimeSetting_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"TotalBasicTimeSetting"];
        initialViewController.TotalBasicTimeSetting_delegate = self;
        initialViewController.rootView = _rootView;
        initialViewController.secondView = self;
        initialViewController.api = _api;
        
        [self.navigationController pushViewController:initialViewController animated:YES];
    }
}

- (IBAction)OneWeekHoliday_Push:(id)sender {
    
    //一週間を変更済みとする
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday1 Registration:_bln_initial_WeekDay1Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay1State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay1Registration = true;
            
            _bln_WeekDay1State = true;
            [self setDayWook:1 flg:_bln_WeekDay1State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay1Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay1State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay1Registration = true;
            
            _bln_WeekDay1State = true;
            [self setDayWook:1 flg:_bln_WeekDay1State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay1Registration){
            
            self.Day1Cansel_View.hidden = true;
        }
    }
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday2 Registration:_bln_initial_WeekDay2Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay2State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay2Registration = true;
            
            _bln_WeekDay2State = true;
            [self setDayWook:2 flg:_bln_WeekDay2State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay2Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay2State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay2Registration = true;
            
            _bln_WeekDay2State = true;
            [self setDayWook:2 flg:_bln_WeekDay2State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay2Registration){
            
            self.Day2Cansel_View.hidden = true;
        }
    }
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday3 Registration:_bln_initial_WeekDay3Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay3State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay3Registration = true;
            
            _bln_WeekDay3State = true;
            [self setDayWook:3 flg:_bln_WeekDay3State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay3Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay3State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay3Registration = true;
            
            _bln_WeekDay3State = true;
            [self setDayWook:3 flg:_bln_WeekDay3State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay3Registration){
            
            self.Day3Cansel_View.hidden = true;
        }
    }
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday4 Registration:_bln_initial_WeekDay4Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay4State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay4Registration = true;
            
            _bln_WeekDay4State = true;
            [self setDayWook:4 flg:_bln_WeekDay4State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay4Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay4State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay4Registration = true;
            
            _bln_WeekDay4State = true;
            [self setDayWook:4 flg:_bln_WeekDay4State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay4Registration){
            
            self.Day4Cansel_View.hidden = true;
        }
    }
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday5 Registration:_bln_initial_WeekDay5Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay5State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay5Registration = true;
            
            _bln_WeekDay5State = true;
            [self setDayWook:5 flg:_bln_WeekDay5State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay5Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay5State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay5Registration = true;
            
            _bln_WeekDay5State = true;
            [self setDayWook:5 flg:_bln_WeekDay5State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay5Registration){
            
            self.Day5Cansel_View.hidden = true;
        }
    }
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday6 Registration:_bln_initial_WeekDay6Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay6State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay6Registration = true;
            
            _bln_WeekDay6State = true;
            [self setDayWook:6 flg:_bln_WeekDay6State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay6Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay6State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay6Registration = true;
            
            _bln_WeekDay6State = true;
            [self setDayWook:6 flg:_bln_WeekDay6State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay6Registration){
            
            self.Day6Cansel_View.hidden = true;
        }
    }
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday7 Registration:_bln_initial_WeekDay7Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay7State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay7Registration = true;
            
            _bln_WeekDay7State = true;
            [self setDayWook:7 flg:_bln_WeekDay7State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay7Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay7State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay7Registration = true;
            
            _bln_WeekDay7State = true;
            [self setDayWook:7 flg:_bln_WeekDay7State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay7Registration){
            
            self.Day7Cansel_View.hidden = true;
        }
    }
}

- (IBAction)NextWeek_Push:(id)sender {
    
    BOOL bln_CheckOnFLG = false;
    //勤務を変更した場合のチェック
    if(_bln_initial_WeekDay1Registration != _bln_WeekDay1Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay2Registration != _bln_WeekDay2Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay3Registration != _bln_WeekDay3Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay4Registration != _bln_WeekDay4Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay5Registration != _bln_WeekDay5Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay6Registration != _bln_WeekDay6Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay7Registration != _bln_WeekDay7Registration){
        bln_CheckOnFLG = true;
    }
    
    //休み・勤務の方法変更チェック
    if(_bln_WeekDay1State != _bln_initial_WeekDay1State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay2State != _bln_initial_WeekDay2State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay3State != _bln_initial_WeekDay3State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay4State != _bln_initial_WeekDay4State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay5State != _bln_initial_WeekDay5State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay6State != _bln_initial_WeekDay6State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay7State != _bln_initial_WeekDay7State){
        bln_CheckOnFLG = true;
    }
    
    //時間の変更チェック
    if(![_str_WeekDay1TimeStart isEqualToString:_str_initial_WeekDay1TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay2TimeStart isEqualToString:_str_initial_WeekDay2TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay3TimeStart isEqualToString:_str_initial_WeekDay3TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay4TimeStart isEqualToString:_str_initial_WeekDay4TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay5TimeStart isEqualToString:_str_initial_WeekDay5TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay6TimeStart isEqualToString:_str_initial_WeekDay6TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay7TimeStart isEqualToString:_str_initial_WeekDay7TimeStart]){
        bln_CheckOnFLG = true;
    }
    
    if(bln_CheckOnFLG == true){
        
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:nil
                                            message:@"今週の予定を確定させていません。このまま翌週の予定登録ページに移動しますか？"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    
                                                    //翌週へ変更
                                                    _lng_WeekType = 2;
                                                    
                                                    //初期値の設定
                                                    [self setFirstValue];
                                                }]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        //翌週へ変更
        _lng_WeekType = 2;
        
        //初期値の設定
        [self setFirstValue];
    }
}

- (IBAction)BackWeek_Push:(id)sender {
    
    BOOL bln_CheckOnFLG = false;
    //勤務を変更した場合のチェック
    if(_bln_initial_WeekDay1Registration != _bln_WeekDay1Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay2Registration != _bln_WeekDay2Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay3Registration != _bln_WeekDay3Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay4Registration != _bln_WeekDay4Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay5Registration != _bln_WeekDay5Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay6Registration != _bln_WeekDay6Registration){
        bln_CheckOnFLG = true;
    }
    if(_bln_initial_WeekDay7Registration != _bln_WeekDay7Registration){
        bln_CheckOnFLG = true;
    }
    
    //休み・勤務の方法変更チェック
    if(_bln_WeekDay1State != _bln_initial_WeekDay1State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay2State != _bln_initial_WeekDay2State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay3State != _bln_initial_WeekDay3State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay4State != _bln_initial_WeekDay4State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay5State != _bln_initial_WeekDay5State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay6State != _bln_initial_WeekDay6State){
        bln_CheckOnFLG = true;
    }
    if(_bln_WeekDay7State != _bln_initial_WeekDay7State){
        bln_CheckOnFLG = true;
    }
    
    //時間の変更チェック
    if(![_str_WeekDay1TimeStart isEqualToString:_str_initial_WeekDay1TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay2TimeStart isEqualToString:_str_initial_WeekDay2TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay3TimeStart isEqualToString:_str_initial_WeekDay3TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay4TimeStart isEqualToString:_str_initial_WeekDay4TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay5TimeStart isEqualToString:_str_initial_WeekDay5TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay6TimeStart isEqualToString:_str_initial_WeekDay6TimeStart]){
        bln_CheckOnFLG = true;
    }
    if(![_str_WeekDay7TimeStart isEqualToString:_str_initial_WeekDay7TimeStart]){
        bln_CheckOnFLG = true;
    }
    
    if(bln_CheckOnFLG == true){
        
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:nil
                                            message:@"翌週の予定を確定させていません。このまま今週の予定追加ページに移動しますか？"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    
                                                    //今週へ変更
                                                    _lng_WeekType = 1;
                                                    
                                                    //初期値の設定
                                                    [self setFirstValue];
                                                }]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        //今週へ変更
        _lng_WeekType = 1;
        
        //初期値の設定
        [self setFirstValue];
    }
}

- (IBAction)Day1Work_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday1 Registration:_bln_initial_WeekDay1Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay1State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay1Registration = true;
            
            _bln_WeekDay1State = false;
            [self setDayWook:1 flg:_bln_WeekDay1State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay1Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay1State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay1Registration = true;
            
            _bln_WeekDay1State = false;
            [self setDayWook:1 flg:_bln_WeekDay1State];
        }
    }
}

- (IBAction)Day2Work_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday2 Registration:_bln_initial_WeekDay2Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay2State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay2Registration = true;
            
            _bln_WeekDay2State = false;
            [self setDayWook:2 flg:_bln_WeekDay2State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay2Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay2State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay2Registration = true;
            
            _bln_WeekDay2State = false;
            [self setDayWook:2 flg:_bln_WeekDay2State];
        }
    }
    
}

- (IBAction)Day3Work_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday3 Registration:_bln_initial_WeekDay3Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay3State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay3Registration = true;
            
            _bln_WeekDay3State = false;
            [self setDayWook:3 flg:_bln_WeekDay3State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay3Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay3State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay3Registration = true;
            
            _bln_WeekDay3State = false;
            [self setDayWook:3 flg:_bln_WeekDay3State];
        }
    }
    
}

- (IBAction)Day4Work_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday4 Registration:_bln_initial_WeekDay4Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay4State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay4Registration = true;
            
            _bln_WeekDay4State = false;
            [self setDayWook:4 flg:_bln_WeekDay4State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay4Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay4State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay4Registration = true;
            
            _bln_WeekDay4State = false;
            [self setDayWook:4 flg:_bln_WeekDay4State];
        }
    }
}

- (IBAction)Day5Work_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday5 Registration:_bln_initial_WeekDay5Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay5State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay5Registration = true;
            
            _bln_WeekDay5State = false;
            [self setDayWook:5 flg:_bln_WeekDay5State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay5Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay5State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay5Registration = true;
            
            _bln_WeekDay5State = false;
            [self setDayWook:5 flg:_bln_WeekDay5State];
        }
    }
}

- (IBAction)Day6Work_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday6 Registration:_bln_initial_WeekDay6Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay6State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay6Registration = true;
            
            _bln_WeekDay6State = false;
            [self setDayWook:6 flg:_bln_WeekDay6State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay6Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay6State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay6Registration = true;
            
            _bln_WeekDay6State = false;
            [self setDayWook:6 flg:_bln_WeekDay6State];
        }
    }
    
}

- (IBAction)Day7Work_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday7 Registration:_bln_initial_WeekDay7Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay7State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay7Registration = true;
            
            _bln_WeekDay7State = false;
            [self setDayWook:7 flg:_bln_WeekDay7State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay7Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay7State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay7Registration = true;
            
            _bln_WeekDay7State = false;
            [self setDayWook:7 flg:_bln_WeekDay7State];
        }
    }
}

- (IBAction)Day1Holiday_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday1 Registration:_bln_initial_WeekDay1Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay1State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay1Registration = true;
            
            _bln_WeekDay1State = true;
            [self setDayWook:1 flg:_bln_WeekDay1State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay1Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay1State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay1Registration = true;
            
            _bln_WeekDay1State = true;
            [self setDayWook:1 flg:_bln_WeekDay1State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay1Registration){
            
            self.Day1Cansel_View.hidden = true;
        }
    }
}

- (IBAction)Day2Holiday_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday2 Registration:_bln_initial_WeekDay2Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay2State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay2Registration = true;
            
            _bln_WeekDay2State = true;
            [self setDayWook:2 flg:_bln_WeekDay2State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay2Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay2State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay2Registration = true;
            
            _bln_WeekDay2State = true;
            [self setDayWook:2 flg:_bln_WeekDay2State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay2Registration){
            
            self.Day2Cansel_View.hidden = true;
        }
    }
}

- (IBAction)Day3Holiday_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday3 Registration:_bln_initial_WeekDay3Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay3State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay3Registration = true;
            
            _bln_WeekDay3State = true;
            [self setDayWook:3 flg:_bln_WeekDay3State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay3Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay3State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay3Registration = true;
            
            _bln_WeekDay3State = true;
            [self setDayWook:3 flg:_bln_WeekDay3State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay3Registration){
            
            self.Day3Cansel_View.hidden = true;
        }
    }
}

- (IBAction)Day4Holiday_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday4 Registration:_bln_initial_WeekDay4Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay4State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay4Registration = true;
            
            _bln_WeekDay4State = true;
            [self setDayWook:4 flg:_bln_WeekDay4State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay4Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay4State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay4Registration = true;
            
            _bln_WeekDay4State = true;
            [self setDayWook:4 flg:_bln_WeekDay4State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay4Registration){
            
            self.Day4Cansel_View.hidden = true;
        }
    }
}

- (IBAction)Day5Holiday_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday5 Registration:_bln_initial_WeekDay5Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay5State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay5Registration = true;
            
            _bln_WeekDay5State = true;
            [self setDayWook:5 flg:_bln_WeekDay5State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay5Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay5State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay5Registration = true;
            
            _bln_WeekDay5State = true;
            [self setDayWook:5 flg:_bln_WeekDay5State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay5Registration){
            
            self.Day5Cansel_View.hidden = true;
        }
    }
}

- (IBAction)Day6Holiday_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday6 Registration:_bln_initial_WeekDay6Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay6State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay6Registration = true;
            
            _bln_WeekDay6State = true;
            [self setDayWook:6 flg:_bln_WeekDay6State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay6Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay6State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay6Registration = true;
            
            _bln_WeekDay6State = true;
            [self setDayWook:6 flg:_bln_WeekDay6State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay6Registration){
            
            self.Day6Cansel_View.hidden = true;
        }
    }
}

- (IBAction)Day7Holiday_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday7 Registration:_bln_initial_WeekDay7Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay7State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay7Registration = true;
            
            _bln_WeekDay7State = true;
            [self setDayWook:7 flg:_bln_WeekDay7State];
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay7Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay7State MsgShow:true]){
            
            //勤務設定フラグON
            _bln_WeekDay7Registration = true;
            
            _bln_WeekDay7State = true;
            [self setDayWook:7 flg:_bln_WeekDay7State];
        }
        
        //キャンセル表示を消す
        if(_bln_initial_WeekDay7Registration){
            
            self.Day7Cansel_View.hidden = true;
        }
    }
}

//勤務ボタンの押せる押せない設定
- (BOOL)getDayRemake:(NSDate*)dt
        Registration:(BOOL)registration
       SetWorkButton:(BOOL)setWorkButton
        WorkingState:(BOOL)workingState
             MsgShow:(BOOL)msgShow {
    
    //registration true:保存済み false:未保存
    //SetWorkButton true:出社　false:休み
    
    if(_lng_WeekType == 2){
        
        //来週の場合は,解禁日チェック
        long lng_day = [_calHoliday weekdayNoGet:_dt_NowDate]-1;
        
        //今週の制限内
        if(lng_day < _lng_ReceptionDay){
            
            //前日は不可とする
            if(msgShow){
                [self messageError:@"" message:@"予定解禁前なので、まだ翌週の予定登録はできません。"];
            }
            return false;
        }else{
            
            //期限の日
            if(lng_day == _lng_ReceptionDay){
                
                //時間での判別
                long lng_hour = [_calHoliday hourNoGet:dt];
                
                //時間での判別
                if(lng_hour >= _lng_ReceptionHour){
                    return true;
                }else{
                    //前日は不可とする
                    if(msgShow){
                        [self messageError:@"" message:@"予定解禁前なので、まだ翌週の予定登録はできません。"];
                    }
                    return false;
                }
            }else{
                
                //制限日以降は制限しない
                return true;
            }
        }

    }else{
        
        //前日以降は変更出来ない
        if([self getOverTime:dt] == false){
            
            if(msgShow){
                [self messageError:@"" message:@"ご指定日の予定変更受付は終了しています。\nセンター員までご連絡ください。"];
            }
            return false;
        }
        
        //未保存は編集可能
        if(registration){
            
            //曜日取得
            long lng_day = [_calHoliday weekdayNoGet:dt]-1;
            
            if(lng_day < _lng_limitwday){
                
                //指定前
                return true;
            }else if(lng_day == _lng_limitwday){
                    
                //指定日当日
                long lng_hour = [_calHoliday hourNoGet:dt];
                
                //時間での判別
                if(lng_hour < _lng_limithour){
                        
                    //休みのみ出社設定可能
                    return true;

                }else{
                    
                    //時間外
                    if(setWorkButton == true){
                        
                        //休みのみ出社設定可能
                        return true;
                    }else{
                        
                        //休みに変更出来ない
                        if(msgShow){
                            [self messageError:@"" message:@"今週の出社を休みに変更する際は、センター員までご連絡ください。"];
                        }
                        return false;
                    }
                }
                
            }else if(lng_day > _lng_limitwday){
                
                //指定後
                if(setWorkButton == true){
                    
                    //休みのみ出社設定可能
                    return true;
                }else{
                    
                    //休みだったかの確認
                    if(workingState == true){
                        
                        return true;
                    }else{
                        
                        //休みに変更出来ない
                        if(msgShow){
                            [self messageError:@"" message:@"今週の出社を休みに変更する際は、センター員までご連絡ください。"];
                        }
                        return false;
                    }
                }
            }
        }else{
            
            //未登録は制限なしに登録できる
            return true;
        }
    }
    
    return false;
}

- (BOOL)getOverTime:(NSDate*)dt {
    
    //指定日の０時を取得
    NSString* dt_dateString = [NSString stringWithFormat:@"%ld-%ld-%ld 00:00:00", [_calHoliday yearNoGet:dt], [_calHoliday monthNoGet:dt], [_calHoliday dayNoGet:dt]];
    
    NSDateFormatter* dt_formatter = [[NSDateFormatter alloc] init];
    [dt_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //12時間表記対策
    [dt_formatter setLocale:[NSLocale systemLocale]];
    //タイムゾーンの指定
    [dt_formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:9]];
    NSDate *dt_date = [dt_formatter dateFromString:dt_dateString];
    
    //８時間追加
    NSDate *re_dt_date = [dt_date initWithTimeInterval:8*60*60 sinceDate:dt_date];
    
    //タイムゾーン含めた現在時間
    NSDate *now_dt = [NSDate dateWithTimeIntervalSinceNow:(60*60*9)];
    NSLog(@"現在： %@　ー　区切り日時:%@", now_dt, re_dt_date);
    
    NSComparisonResult result = [re_dt_date compare:now_dt];
    switch (result) {
        case NSOrderedAscending:
            // 過去
            NSLog(@"NSOrderedAscending");
            return false;
            break;
        case NSOrderedDescending:
            // 未来日
            NSLog(@"NSOrderedDescending");
            return true;
            break;
        case NSOrderedSame:
            // 当日
            NSLog(@"NSOrderedSame");
            return true;
            break;
        default:
            return false;
            break;
    }
}

- (IBAction)Day1Time_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday1 Registration:_bln_initial_WeekDay1Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay1State MsgShow:false]){
            
            if(_bln_WeekDay1Registration){
                if(_bln_WeekDay1State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 1;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day1Day_lbl.text, self.Day1Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay1TimeStart;
                    initialViewController.str_endTime = _str_WeekDay1TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay1Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay1State MsgShow:false]){
            
            if(_bln_WeekDay1Registration){
                if(_bln_WeekDay1State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 1;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day1Day_lbl.text, self.Day1Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay1TimeStart;
                    initialViewController.str_endTime = _str_WeekDay1TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }
}

- (IBAction)Day2Time_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday2 Registration:_bln_initial_WeekDay2Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay2State MsgShow:false]){
            
            if(_bln_WeekDay2Registration){
                if(_bln_WeekDay2State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 2;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day2Day_lbl.text, self.Day2Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay2TimeStart;
                    initialViewController.str_endTime = _str_WeekDay2TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }

        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay2Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay2State MsgShow:false]){
            
            if(_bln_WeekDay2Registration){
                if(_bln_WeekDay2State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 2;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day2Day_lbl.text, self.Day2Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay2TimeStart;
                    initialViewController.str_endTime = _str_WeekDay2TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }

        }
    }
}

- (IBAction)Day3Time_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday3 Registration:_bln_initial_WeekDay3Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay3State MsgShow:false]){
            
            if(_bln_WeekDay3Registration){
                if(_bln_WeekDay3State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 3;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day3Day_lbl.text, self.Day3Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay3TimeStart;
                    initialViewController.str_endTime = _str_WeekDay3TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay3Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay3State MsgShow:false]){
            
            if(_bln_WeekDay3Registration){
                if(_bln_WeekDay3State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 3;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day3Day_lbl.text, self.Day3Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay3TimeStart;
                    initialViewController.str_endTime = _str_WeekDay3TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }
}

- (IBAction)Day4Time_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday4 Registration:_bln_initial_WeekDay4Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay4State MsgShow:false]){
            
            if(_bln_WeekDay4Registration){
                if(_bln_WeekDay4State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 4;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day4Day_lbl.text, self.Day4Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay4TimeStart;
                    initialViewController.str_endTime = _str_WeekDay4TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay4Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay4State MsgShow:false]){
            
            if(_bln_WeekDay4Registration){
                if(_bln_WeekDay4State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 4;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day4Day_lbl.text, self.Day4Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay4TimeStart;
                    initialViewController.str_endTime = _str_WeekDay4TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }
}

- (IBAction)Day5Time_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday5 Registration:_bln_initial_WeekDay5Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay5State MsgShow:false]){
            
            if(_bln_WeekDay5Registration){
                if(_bln_WeekDay5State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 5;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day5Day_lbl.text, self.Day5Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay5TimeStart;
                    initialViewController.str_endTime = _str_WeekDay5TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay5Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay5State MsgShow:false]){
            
            if(_bln_WeekDay5Registration){
                if(_bln_WeekDay5State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 5;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day5Day_lbl.text, self.Day5Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay5TimeStart;
                    initialViewController.str_endTime = _str_WeekDay5TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }
}

- (IBAction)Day6Time_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday6 Registration:_bln_initial_WeekDay6Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay6State MsgShow:false]){
            
            if(_bln_WeekDay6Registration){
                if(_bln_WeekDay6State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 6;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day6Day_lbl.text, self.Day6Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay6TimeStart;
                    initialViewController.str_endTime = _str_WeekDay6TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay6Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay6State MsgShow:false]){
            
            if(_bln_WeekDay6Registration){
                if(_bln_WeekDay6State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 6;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day6Day_lbl.text, self.Day6Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay6TimeStart;
                    initialViewController.str_endTime = _str_WeekDay6TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }
}

- (IBAction)Day7Time_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if([self getDayRemake:_dt_wday7 Registration:_bln_initial_WeekDay7Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay7State MsgShow:false]){
            
            if(_bln_WeekDay7Registration){
                if(_bln_WeekDay7State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 7;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day7Day_lbl.text, self.Day7Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay7TimeStart;
                    initialViewController.str_endTime = _str_WeekDay7TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay7Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay7State MsgShow:false]){
            
            if(_bln_WeekDay7Registration){
                if(_bln_WeekDay7State == true){
                    [self messageError:@"" message:@"休み選択時は\n時間変更出来ません。"];
                }else{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                    OneDayTimeSetting_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"OneDayTimeSetting"];
                    initialViewController.OneDayTimeSetting_delegate = self;
                    initialViewController.rootView = self;
                    initialViewController.api = _api;
                    
                    initialViewController.daypotition = 7;
                    initialViewController.str_day = [NSString stringWithFormat:@"%@%@", self.Day7Day_lbl.text, self.Day7Week_lbl.text];
                    initialViewController.str_startTime = _str_WeekDay7TimeStart;
                    initialViewController.str_endTime = _str_WeekDay7TimeEnd;
                    
                    [self.navigationController pushViewController:initialViewController animated:YES];
                }
            }else{
                [self messageError:@"" message:@"未選択時は\n時間変更出来ません。"];
            }
        }
    }
}

- (IBAction)OK_Push:(id)sender {
    
    if(_lng_WeekType == 1){
        
        //今週設定で日毎の行日付で考慮
        if(_bln_WeekDay1Registration || ([self getDayRemake:_dt_wday1 Registration:_bln_initial_WeekDay1Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay1State MsgShow:false] == false)){
            if(_bln_WeekDay2Registration || ([self getDayRemake:_dt_wday2 Registration:_bln_initial_WeekDay2Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay2State MsgShow:false] == false)){
                if(_bln_WeekDay3Registration || ([self getDayRemake:_dt_wday3 Registration:_bln_initial_WeekDay3Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay3State MsgShow:false] == false)){
                    if(_bln_WeekDay4Registration || ([self getDayRemake:_dt_wday4 Registration:_bln_initial_WeekDay4Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay4State MsgShow:false] == false)){
                        if(_bln_WeekDay5Registration || ([self getDayRemake:_dt_wday5 Registration:_bln_initial_WeekDay5Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay5State MsgShow:false] == false)){
                            if(_bln_WeekDay6Registration || ([self getDayRemake:_dt_wday6 Registration:_bln_initial_WeekDay6Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay6State MsgShow:false] == false)){
                                if(_bln_WeekDay7Registration || ([self getDayRemake:_dt_wday7 Registration:_bln_initial_WeekDay7Registration SetWorkButton:true WorkingState:_bln_initial_WeekDay7State MsgShow:false] == false)){
                                    
                                    NSMutableArray *root_bodypaper = [[NSMutableArray alloc] init];
                                    NSMutableDictionary *bodypaper1 = [[NSMutableDictionary alloc] init];
                                    [bodypaper1 setValue:[NSNumber numberWithLong:0] forKey:@"wday"];
                                    if(_bln_WeekDay1Registration == false){
                                        [bodypaper1 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper1 setValue:[NSNumber numberWithBool:_bln_WeekDay1State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay1TimeStart isEqualToString:@""]){
                                        [bodypaper1 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper1 setValue:_str_WeekDay1TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay1TimeEnd isEqualToString:@""]){
                                        [bodypaper1 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper1 setValue:_str_WeekDay1TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper2 = [[NSMutableDictionary alloc] init];
                                    [bodypaper2 setValue:[NSNumber numberWithLong:1] forKey:@"wday"];
                                    if(_bln_WeekDay2Registration == false){
                                        [bodypaper2 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper2 setValue:[NSNumber numberWithBool:_bln_WeekDay2State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay2TimeStart isEqualToString:@""]){
                                        [bodypaper2 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper2 setValue:_str_WeekDay2TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay2TimeEnd isEqualToString:@""]){
                                        [bodypaper2 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper2 setValue:_str_WeekDay2TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper3 = [[NSMutableDictionary alloc] init];
                                    [bodypaper3 setValue:[NSNumber numberWithLong:2] forKey:@"wday"];
                                    if(_bln_WeekDay3Registration == false){
                                        [bodypaper3 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper3 setValue:[NSNumber numberWithBool:_bln_WeekDay3State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay3TimeStart isEqualToString:@""]){
                                        [bodypaper3 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper3 setValue:_str_WeekDay3TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay3TimeEnd isEqualToString:@""]){
                                        [bodypaper3 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper3 setValue:_str_WeekDay3TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper4 = [[NSMutableDictionary alloc] init];
                                    [bodypaper4 setValue:[NSNumber numberWithLong:3] forKey:@"wday"];
                                    if(_bln_WeekDay4Registration == false){
                                        [bodypaper4 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper4 setValue:[NSNumber numberWithBool:_bln_WeekDay4State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay4TimeStart isEqualToString:@""]){
                                        [bodypaper4 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper4 setValue:_str_WeekDay4TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay4TimeEnd isEqualToString:@""]){
                                        [bodypaper4 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper4 setValue:_str_WeekDay4TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper5 = [[NSMutableDictionary alloc] init];
                                    [bodypaper5 setValue:[NSNumber numberWithLong:4] forKey:@"wday"];
                                    if(_bln_WeekDay5Registration == false){
                                        [bodypaper5 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper5 setValue:[NSNumber numberWithBool:_bln_WeekDay5State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay5TimeStart isEqualToString:@""]){
                                        [bodypaper5 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper5 setValue:_str_WeekDay5TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay5TimeEnd isEqualToString:@""]){
                                        [bodypaper5 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper5 setValue:_str_WeekDay5TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper6 = [[NSMutableDictionary alloc] init];
                                    [bodypaper6 setValue:[NSNumber numberWithLong:5] forKey:@"wday"];
                                    if(_bln_WeekDay6Registration == false){
                                        [bodypaper6 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper6 setValue:[NSNumber numberWithBool:_bln_WeekDay6State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay6TimeStart isEqualToString:@""]){
                                        [bodypaper6 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper6 setValue:_str_WeekDay6TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay6TimeEnd isEqualToString:@""]){
                                        [bodypaper6 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper6 setValue:_str_WeekDay6TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper7 = [[NSMutableDictionary alloc] init];
                                    [bodypaper7 setValue:[NSNumber numberWithLong:6] forKey:@"wday"];
                                    if(_bln_WeekDay7Registration == false){
                                        [bodypaper7 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper7 setValue:[NSNumber numberWithBool:_bln_WeekDay7State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay7TimeStart isEqualToString:@""]){
                                        [bodypaper7 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper7 setValue:_str_WeekDay7TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay7TimeEnd isEqualToString:@""]){
                                        [bodypaper7 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper7 setValue:_str_WeekDay7TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    [root_bodypaper addObject:bodypaper1];
                                    [root_bodypaper addObject:bodypaper2];
                                    [root_bodypaper addObject:bodypaper3];
                                    [root_bodypaper addObject:bodypaper4];
                                    [root_bodypaper addObject:bodypaper5];
                                    [root_bodypaper addObject:bodypaper6];
                                    [root_bodypaper addObject:bodypaper7];
                                    
                                    //----------------------- 送信するパラメータの組み立て(確認画面用) ------------------------
                                    NSMutableDictionary *mutableDic_Confirmation = [NSMutableDictionary dictionary];
                                    [mutableDic_Confirmation setValue:root_bodypaper forKey:@"schedules"];
                                    
                                    //クエリパラメータ追加（BOOL型で直接は送れない）
                                    [mutableDic_Confirmation setValue:[Configuration getSessionTokenKey] forKey:@"token"];
                                    switch (_lng_WeekType) {
                                        case 1:
                                            [mutableDic_Confirmation setValue:@"this" forKey:@"week"];
                                            break;
                                        case 2:
                                            [mutableDic_Confirmation setValue:@"next" forKey:@"week"];
                                            break;
                                    }
                                    //保存パラメータセット
                                    [mutableDic_Confirmation setValue:@"false" forKey:@"confirmed"];
                                    
                                    NSError *error = nil;
                                    _data_Confirmation = nil;
                                    NSString *str_body;
                                    if([NSJSONSerialization isValidJSONObject:mutableDic_Confirmation]){
                                        _data_Confirmation = [NSJSONSerialization dataWithJSONObject:mutableDic_Confirmation options:NSJSONWritingPrettyPrinted error:&error];
                                        str_body = [[NSString alloc] initWithData:_data_Confirmation encoding:NSUTF8StringEncoding];
                                        NSLog(@"%@",str_body);
                                    }
                                    
                                    //----------------------- 送信するパラメータの組み立て(保存用) ------------------------
                                    NSMutableDictionary *mutableDic_Confirm = [NSMutableDictionary dictionary];
                                    [mutableDic_Confirm setValue:root_bodypaper forKey:@"schedules"];
                                    
                                    //クエリパラメータ追加（BOOL型で直接は送れない）
                                    [mutableDic_Confirm setValue:[Configuration getSessionTokenKey] forKey:@"token"];
                                    switch (_lng_WeekType) {
                                        case 1:
                                            [mutableDic_Confirm setValue:@"this" forKey:@"week"];
                                            break;
                                        case 2:
                                            [mutableDic_Confirm setValue:@"next" forKey:@"week"];
                                            break;
                                    }
                                    //保存パラメータセット
                                    [mutableDic_Confirm setValue:@"true" forKey:@"confirmed"];
                                    
                                    error = nil;
                                    _data_API = nil;
                                    str_body = @"";
                                    if([NSJSONSerialization isValidJSONObject:mutableDic_Confirm]){
                                        _data_API = [NSJSONSerialization dataWithJSONObject:mutableDic_Confirm options:NSJSONWritingPrettyPrinted error:&error];
                                        str_body = [[NSString alloc] initWithData:_data_API encoding:NSUTF8StringEncoding];
                                        NSLog(@"%@",str_body);
                                    }
                                    
                                    //確認データでチェック
                                    [_api Api_SchedulesWeekPostGetting:self week_json:_data_Confirmation];
                                    
                                }else{
                                    [self messageError:@"" message:@"選択されていない項目があります。"];
                                }
                            }else{
                                [self messageError:@"" message:@"選択されていない項目があります。"];
                            }
                        }else{
                            [self messageError:@"" message:@"選択されていない項目があります。"];
                        }
                    }else{
                        [self messageError:@"" message:@"選択されていない項目があります。"];
                    }
                }else{
                    [self messageError:@"" message:@"選択されていない項目があります。"];
                }
            }else{
                [self messageError:@"" message:@"選択されていない項目があります。"];
            }
        }else{
            [self messageError:@"" message:@"選択されていない項目があります。"];
        }

    }else if(_lng_WeekType == 2){
        
        //翌週設定で日をフォーム開いた日時で考慮
        if(_bln_WeekDay1Registration || ([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay1Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay1State MsgShow:true] == false)){
            if(_bln_WeekDay2Registration || ([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay2Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay2State MsgShow:true] == false)){
                if(_bln_WeekDay3Registration || ([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay3Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay3State MsgShow:true] == false)){
                    if(_bln_WeekDay4Registration || ([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay4Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay4State MsgShow:true] == false)){
                        if(_bln_WeekDay5Registration || ([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay5Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay5State MsgShow:true] == false)){
                            if(_bln_WeekDay6Registration || ([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay6Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay6State MsgShow:true] == false)){
                                if(_bln_WeekDay7Registration || ([self getDayRemake:_dt_NowDate Registration:_bln_initial_WeekDay7Registration SetWorkButton:false WorkingState:_bln_initial_WeekDay7State MsgShow:true] == false)){
                                    
                                    NSMutableArray *root_bodypaper = [[NSMutableArray alloc] init];
                                    NSMutableDictionary *bodypaper1 = [[NSMutableDictionary alloc] init];
                                    [bodypaper1 setValue:[NSNumber numberWithLong:0] forKey:@"wday"];
                                    if(_bln_WeekDay1Registration == false){
                                        [bodypaper1 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper1 setValue:[NSNumber numberWithBool:_bln_WeekDay1State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay1TimeStart isEqualToString:@""]){
                                        [bodypaper1 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper1 setValue:_str_WeekDay1TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay1TimeEnd isEqualToString:@""]){
                                        [bodypaper1 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper1 setValue:_str_WeekDay1TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper2 = [[NSMutableDictionary alloc] init];
                                    [bodypaper2 setValue:[NSNumber numberWithLong:1] forKey:@"wday"];
                                    if(_bln_WeekDay2Registration == false){
                                        [bodypaper2 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper2 setValue:[NSNumber numberWithBool:_bln_WeekDay2State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay2TimeStart isEqualToString:@""]){
                                        [bodypaper2 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper2 setValue:_str_WeekDay2TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay2TimeEnd isEqualToString:@""]){
                                        [bodypaper2 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper2 setValue:_str_WeekDay2TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper3 = [[NSMutableDictionary alloc] init];
                                    [bodypaper3 setValue:[NSNumber numberWithLong:2] forKey:@"wday"];
                                    if(_bln_WeekDay3Registration == false){
                                        [bodypaper3 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper3 setValue:[NSNumber numberWithBool:_bln_WeekDay3State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay3TimeStart isEqualToString:@""]){
                                        [bodypaper3 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper3 setValue:_str_WeekDay3TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay3TimeEnd isEqualToString:@""]){
                                        [bodypaper3 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper3 setValue:_str_WeekDay3TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper4 = [[NSMutableDictionary alloc] init];
                                    [bodypaper4 setValue:[NSNumber numberWithLong:3] forKey:@"wday"];
                                    if(_bln_WeekDay4Registration == false){
                                        [bodypaper4 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper4 setValue:[NSNumber numberWithBool:_bln_WeekDay4State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay4TimeStart isEqualToString:@""]){
                                        [bodypaper4 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper4 setValue:_str_WeekDay4TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay4TimeEnd isEqualToString:@""]){
                                        [bodypaper4 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper4 setValue:_str_WeekDay4TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper5 = [[NSMutableDictionary alloc] init];
                                    [bodypaper5 setValue:[NSNumber numberWithLong:4] forKey:@"wday"];
                                    if(_bln_WeekDay5Registration == false){
                                        [bodypaper5 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper5 setValue:[NSNumber numberWithBool:_bln_WeekDay5State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay5TimeStart isEqualToString:@""]){
                                        [bodypaper5 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper5 setValue:_str_WeekDay5TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay5TimeEnd isEqualToString:@""]){
                                        [bodypaper5 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper5 setValue:_str_WeekDay5TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper6 = [[NSMutableDictionary alloc] init];
                                    [bodypaper6 setValue:[NSNumber numberWithLong:5] forKey:@"wday"];
                                    if(_bln_WeekDay6Registration == false){
                                        [bodypaper6 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper6 setValue:[NSNumber numberWithBool:_bln_WeekDay6State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay6TimeStart isEqualToString:@""]){
                                        [bodypaper6 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper6 setValue:_str_WeekDay6TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay6TimeEnd isEqualToString:@""]){
                                        [bodypaper6 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper6 setValue:_str_WeekDay6TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    NSMutableDictionary *bodypaper7 = [[NSMutableDictionary alloc] init];
                                    [bodypaper7 setValue:[NSNumber numberWithLong:6] forKey:@"wday"];
                                    if(_bln_WeekDay7Registration == false){
                                        [bodypaper7 setValue:[NSNumber numberWithBool:true] forKey:@"absence"];
                                    }else{
                                        [bodypaper7 setValue:[NSNumber numberWithBool:_bln_WeekDay7State] forKey:@"absence"];
                                    }
                                    if([_str_WeekDay7TimeStart isEqualToString:@""]){
                                        [bodypaper7 setValue:[NSNull null] forKey:@"start_at"];
                                    }else{
                                        [bodypaper7 setValue:_str_WeekDay7TimeStart forKey:@"start_at"];
                                    }
                                    if([_str_WeekDay7TimeEnd isEqualToString:@""]){
                                        [bodypaper7 setValue:[NSNull null] forKey:@"end_at"];
                                    }else{
                                        [bodypaper7 setValue:_str_WeekDay7TimeEnd forKey:@"end_at"];
                                    }
                                    
                                    [root_bodypaper addObject:bodypaper1];
                                    [root_bodypaper addObject:bodypaper2];
                                    [root_bodypaper addObject:bodypaper3];
                                    [root_bodypaper addObject:bodypaper4];
                                    [root_bodypaper addObject:bodypaper5];
                                    [root_bodypaper addObject:bodypaper6];
                                    [root_bodypaper addObject:bodypaper7];
                                    
                                    //----------------------- 送信するパラメータの組み立て(確認画面用) ------------------------
                                    NSMutableDictionary *mutableDic_Confirmation = [NSMutableDictionary dictionary];
                                    [mutableDic_Confirmation setValue:root_bodypaper forKey:@"schedules"];
                                    
                                    //クエリパラメータ追加（BOOL型で直接は送れない）
                                    [mutableDic_Confirmation setValue:[Configuration getSessionTokenKey] forKey:@"token"];
                                    switch (_lng_WeekType) {
                                        case 1:
                                            [mutableDic_Confirmation setValue:@"this" forKey:@"week"];
                                            break;
                                        case 2:
                                            [mutableDic_Confirmation setValue:@"next" forKey:@"week"];
                                            break;
                                    }
                                    //保存パラメータセット
                                    [mutableDic_Confirmation setValue:@"false" forKey:@"confirmed"];
                                    
                                    NSError *error = nil;
                                    _data_Confirmation = nil;
                                    NSString *str_body;
                                    if([NSJSONSerialization isValidJSONObject:mutableDic_Confirmation]){
                                        _data_Confirmation = [NSJSONSerialization dataWithJSONObject:mutableDic_Confirmation options:NSJSONWritingPrettyPrinted error:&error];
                                        str_body = [[NSString alloc] initWithData:_data_Confirmation encoding:NSUTF8StringEncoding];
                                        NSLog(@"%@",str_body);
                                    }
                                    
                                    //----------------------- 送信するパラメータの組み立て(保存用) ------------------------
                                    NSMutableDictionary *mutableDic_Confirm = [NSMutableDictionary dictionary];
                                    [mutableDic_Confirm setValue:root_bodypaper forKey:@"schedules"];
                                    
                                    //クエリパラメータ追加（BOOL型で直接は送れない）
                                    [mutableDic_Confirm setValue:[Configuration getSessionTokenKey] forKey:@"token"];
                                    switch (_lng_WeekType) {
                                        case 1:
                                            [mutableDic_Confirm setValue:@"this" forKey:@"week"];
                                            break;
                                        case 2:
                                            [mutableDic_Confirm setValue:@"next" forKey:@"week"];
                                            break;
                                    }
                                    //保存パラメータセット
                                    [mutableDic_Confirm setValue:@"true" forKey:@"confirmed"];
                                    
                                    error = nil;
                                    _data_API = nil;
                                    str_body = @"";
                                    if([NSJSONSerialization isValidJSONObject:mutableDic_Confirm]){
                                        _data_API = [NSJSONSerialization dataWithJSONObject:mutableDic_Confirm options:NSJSONWritingPrettyPrinted error:&error];
                                        str_body = [[NSString alloc] initWithData:_data_API encoding:NSUTF8StringEncoding];
                                        NSLog(@"%@",str_body);
                                    }
                                    
                                    //確認データでチェック
                                    [_api Api_SchedulesWeekPostGetting:self week_json:_data_Confirmation];
                                    
                                }else{
                                    [self messageError:@"" message:@"選択されていない項目があります。"];
                                }
                            }else{
                                [self messageError:@"" message:@"選択されていない項目があります。"];
                            }
                        }else{
                            [self messageError:@"" message:@"選択されていない項目があります。"];
                        }
                    }else{
                        [self messageError:@"" message:@"選択されていない項目があります。"];
                    }
                }else{
                    [self messageError:@"" message:@"選択されていない項目があります。"];
                }
            }else{
                [self messageError:@"" message:@"選択されていない項目があります。"];
            }
        }else{
            [self messageError:@"" message:@"選択されていない項目があります。"];
        }
    }
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"")
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//バックアクション
//今週来週画面チェンジ
- (void)SyussyaYoyakuKakuninWeek_SetScreenType:(long)lng_WeekType {
    
    //週の設定変更
    _lng_WeekType = lng_WeekType;
    
    //APIからの再読み込み設定
    _bln_reloadFlg = true;
}
- (void)OneDayTimeSetting_backActionView:(long)DayPotion
                               StartTime:(NSString*)stratTime
                                 EndTime:(NSString*)endTime {
    
    //値の再セットさせない
    _bln_reloadFlg = false;
    
    switch (DayPotion) {
        case 1:
            _str_WeekDay1TimeStart = stratTime;
            _str_WeekDay1TimeEnd = endTime;
            break;
        case 2:
            _str_WeekDay2TimeStart = stratTime;
            _str_WeekDay2TimeEnd = endTime;
            break;
        case 3:
            _str_WeekDay3TimeStart = stratTime;
            _str_WeekDay3TimeEnd = endTime;
            break;
        case 4:
            _str_WeekDay4TimeStart = stratTime;
            _str_WeekDay4TimeEnd = endTime;
            break;
        case 5:
            _str_WeekDay5TimeStart = stratTime;
            _str_WeekDay5TimeEnd = endTime;
            break;
        case 6:
            _str_WeekDay6TimeStart = stratTime;
            _str_WeekDay6TimeEnd = endTime;
            break;
        case 7:
            _str_WeekDay7TimeStart = stratTime;
            _str_WeekDay7TimeEnd = endTime;
            break;
    }
    
    //時間ラベル再セット
    [self setTime];
}
- (void)OneDayTimeSetting_canselBackActionView {
    
    //値の再セットさせない
    _bln_reloadFlg = false;
}

- (void)BasicTimeSetting_BackActionView {
}

//確認画面からのキャンセル処理
- (void)SyussyaYoyakuKakuninWeek_CanselBack {
    
    //値の再セットさせない
    _bln_reloadFlg = false;
}

//Login画面移動用
- (void)TotalBasicTimeSetting_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}
- (void)BasicTimeSetting_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}
- (void)OneDayTimeSetting_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}
- (void)SyussyaYoyakuKakuninWeek_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        //値取得
        _ary_months = [arrayData valueForKey:@"months"];
        _ary_weeks = [arrayData valueForKey:@"weeks"];
        
    }else{
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
    
    //今週来週の切り替え
    switch (_lng_WeekType) {
        case 1:
            //スケジュールステータス取得
            [_api Api_SchedulesWeekGetting:self week:@"this"];
            break;
        case 2:
            //日付取得
            [_api Api_SchedulesWeekGetting:self week:@"next"];
            break;
    }
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    if(FLG == YES){
        
        //------------------------------- 日別データ格納 -------------------------------
        NSMutableArray* ary_dt1;
        NSMutableArray* ary_dt2;
        NSMutableArray* ary_dt3;
        NSMutableArray* ary_dt4;
        NSMutableArray* ary_dt5;
        NSMutableArray* ary_dt6;
        NSMutableArray* ary_dt7;
        NSMutableArray* ary_week = [arrayData valueForKey:@"week"];
        for(long c=0;c<ary_week.count;c++){
            
            //日別での格納
            switch ([[[ary_week valueForKey:@"wday"] objectAtIndex:c] longValue]) {
                case 0:
                    ary_dt1 = [[arrayData valueForKey:@"week"] objectAtIndex:0];
                    break;
                case 1:
                    ary_dt2 = [[arrayData valueForKey:@"week"] objectAtIndex:1];
                    break;
                case 2:
                    ary_dt3 = [[arrayData valueForKey:@"week"] objectAtIndex:2];
                    break;
                case 3:
                    ary_dt4 = [[arrayData valueForKey:@"week"] objectAtIndex:3];
                    break;
                case 4:
                    ary_dt5 = [[arrayData valueForKey:@"week"] objectAtIndex:4];
                    break;
                case 5:
                    ary_dt6 = [[arrayData valueForKey:@"week"] objectAtIndex:5];
                    break;
                case 6:
                    ary_dt7 = [[arrayData valueForKey:@"week"] objectAtIndex:6];
                    break;
            }
        }
        
        //-------------------------------　予定を登録しているか否かフラグ　-------------------------------
        _bln_WeekDay1Registration = [[ary_dt1 valueForKey:@"registration"] boolValue];
        _bln_WeekDay2Registration = [[ary_dt2 valueForKey:@"registration"] boolValue];
        _bln_WeekDay3Registration = [[ary_dt3 valueForKey:@"registration"] boolValue];
        _bln_WeekDay4Registration = [[ary_dt4 valueForKey:@"registration"] boolValue];
        _bln_WeekDay5Registration = [[ary_dt5 valueForKey:@"registration"] boolValue];
        _bln_WeekDay6Registration = [[ary_dt6 valueForKey:@"registration"] boolValue];
        _bln_WeekDay7Registration = [[ary_dt7 valueForKey:@"registration"] boolValue];
        
        _bln_initial_WeekDay1Registration = _bln_WeekDay1Registration;
        _bln_initial_WeekDay2Registration = _bln_WeekDay2Registration;
        _bln_initial_WeekDay3Registration = _bln_WeekDay3Registration;
        _bln_initial_WeekDay4Registration = _bln_WeekDay4Registration;
        _bln_initial_WeekDay5Registration = _bln_WeekDay5Registration;
        _bln_initial_WeekDay6Registration = _bln_WeekDay6Registration;
        _bln_initial_WeekDay7Registration = _bln_WeekDay7Registration;
        
        //------------------------------- 日付の設定 -----------------------------------
        _dt_wday1 = [_calHoliday RemakeDatetime:[ary_dt1 valueForKey:@"date"]];
        _dt_wday2 = [_calHoliday RemakeDatetime:[ary_dt2 valueForKey:@"date"]];
        _dt_wday3 = [_calHoliday RemakeDatetime:[ary_dt3 valueForKey:@"date"]];
        _dt_wday4 = [_calHoliday RemakeDatetime:[ary_dt4 valueForKey:@"date"]];
        _dt_wday5 = [_calHoliday RemakeDatetime:[ary_dt5 valueForKey:@"date"]];
        _dt_wday6 = [_calHoliday RemakeDatetime:[ary_dt6 valueForKey:@"date"]];
        _dt_wday7 = [_calHoliday RemakeDatetime:[ary_dt7 valueForKey:@"date"]];
        
        //日付表示を更新
        [self setDay];
        
        //-------------------------------　指定マーク設定　-------------------------------
        _bln_WeekDay1Designation = [[ary_dt1 valueForKey:@"designation"] boolValue];
        _bln_WeekDay2Designation = [[ary_dt2 valueForKey:@"designation"] boolValue];
        _bln_WeekDay3Designation = [[ary_dt3 valueForKey:@"designation"] boolValue];
        _bln_WeekDay4Designation = [[ary_dt4 valueForKey:@"designation"] boolValue];
        _bln_WeekDay5Designation = [[ary_dt5 valueForKey:@"designation"] boolValue];
        _bln_WeekDay6Designation = [[ary_dt6 valueForKey:@"designation"] boolValue];
        _bln_WeekDay7Designation = [[ary_dt7 valueForKey:@"designation"] boolValue];
        
        if(_bln_WeekDay1Designation){
            self.Day1Shite_view.hidden = false;
        }else{
            self.Day1Shite_view.hidden = true;
        }
        if(_bln_WeekDay2Designation){
            self.Day2Shite_view.hidden = false;
        }else{
            self.Day2Shite_view.hidden = true;
        }
        if(_bln_WeekDay3Designation){
            self.Day3Shite_view.hidden = false;
        }else{
            self.Day3Shite_view.hidden = true;
        }
        if(_bln_WeekDay4Designation){
            self.Day4Shite_view.hidden = false;
        }else{
            self.Day4Shite_view.hidden = true;
        }
        if(_bln_WeekDay5Designation){
            self.Day5Shite_view.hidden = false;
        }else{
            self.Day5Shite_view.hidden = true;
        }
        if(_bln_WeekDay6Designation){
            self.Day6Shite_view.hidden = false;
        }else{
            self.Day6Shite_view.hidden = true;
        }
        if(_bln_WeekDay7Designation){
            self.Day7Shite_view.hidden = false;
        }else{
            self.Day7Shite_view.hidden = true;
        }
        
        //------------------------------- 出社・休み設定 -----------------------------------
        _bln_WeekDay1State = [[ary_dt1 valueForKey:@"absence"] boolValue];
        _bln_WeekDay2State = [[ary_dt2 valueForKey:@"absence"] boolValue];
        _bln_WeekDay3State = [[ary_dt3 valueForKey:@"absence"] boolValue];
        _bln_WeekDay4State = [[ary_dt4 valueForKey:@"absence"] boolValue];
        _bln_WeekDay5State = [[ary_dt5 valueForKey:@"absence"] boolValue];
        _bln_WeekDay6State = [[ary_dt6 valueForKey:@"absence"] boolValue];
        _bln_WeekDay7State = [[ary_dt7 valueForKey:@"absence"] boolValue];
        
        [self setDayWook:1 flg:_bln_WeekDay1State];
        [self setDayWook:2 flg:_bln_WeekDay2State];
        [self setDayWook:3 flg:_bln_WeekDay3State];
        [self setDayWook:4 flg:_bln_WeekDay4State];
        [self setDayWook:5 flg:_bln_WeekDay5State];
        [self setDayWook:6 flg:_bln_WeekDay6State];
        [self setDayWook:7 flg:_bln_WeekDay7State];
        
        //初期の状態保存
        _bln_initial_WeekDay1State = _bln_WeekDay1State;
        _bln_initial_WeekDay2State = _bln_WeekDay2State;
        _bln_initial_WeekDay3State = _bln_WeekDay3State;
        _bln_initial_WeekDay4State = _bln_WeekDay4State;
        _bln_initial_WeekDay5State = _bln_WeekDay5State;
        _bln_initial_WeekDay6State = _bln_WeekDay6State;
        _bln_initial_WeekDay7State = _bln_WeekDay7State;
        
        //-------------------------------　キャンセル待ち設定　-------------------------------
        _bln_WeekDay1WaitingCancel = [[ary_dt1 valueForKey:@"waiting_cancel"] boolValue];
        _bln_WeekDay2WaitingCancel = [[ary_dt2 valueForKey:@"waiting_cancel"] boolValue];
        _bln_WeekDay3WaitingCancel = [[ary_dt3 valueForKey:@"waiting_cancel"] boolValue];
        _bln_WeekDay4WaitingCancel = [[ary_dt4 valueForKey:@"waiting_cancel"] boolValue];
        _bln_WeekDay5WaitingCancel = [[ary_dt5 valueForKey:@"waiting_cancel"] boolValue];
        _bln_WeekDay6WaitingCancel = [[ary_dt6 valueForKey:@"waiting_cancel"] boolValue];
        _bln_WeekDay7WaitingCancel = [[ary_dt7 valueForKey:@"waiting_cancel"] boolValue];
        
        if(_bln_WeekDay1WaitingCancel && _bln_initial_WeekDay1State == false && _bln_WeekDay1Registration == true){
            self.Day1Cansel_View.hidden = false;
        }else{
            self.Day1Cansel_View.hidden = true;
        }
        if(_bln_WeekDay2WaitingCancel && _bln_initial_WeekDay2State == false && _bln_WeekDay2Registration == true){
            self.Day2Cansel_View.hidden = false;
        }else{
            self.Day2Cansel_View.hidden = true;
        }
        if(_bln_WeekDay3WaitingCancel && _bln_initial_WeekDay3State == false && _bln_WeekDay3Registration == true){
            self.Day3Cansel_View.hidden = false;
        }else{
            self.Day3Cansel_View.hidden = true;
        }
        if(_bln_WeekDay4WaitingCancel && _bln_initial_WeekDay4State == false && _bln_WeekDay4Registration == true){
            self.Day4Cansel_View.hidden = false;
        }else{
            self.Day4Cansel_View.hidden = true;
        }
        if(_bln_WeekDay5WaitingCancel && _bln_initial_WeekDay5State == false && _bln_WeekDay5Registration == true){
            self.Day5Cansel_View.hidden = false;
        }else{
            self.Day5Cansel_View.hidden = true;
        }
        if(_bln_WeekDay6WaitingCancel && _bln_initial_WeekDay6State == false && _bln_WeekDay6Registration == true){
            self.Day6Cansel_View.hidden = false;
        }else{
            self.Day6Cansel_View.hidden = true;
        }
        if(_bln_WeekDay7WaitingCancel && _bln_initial_WeekDay7State == false && _bln_WeekDay7Registration == true){
            self.Day7Cansel_View.hidden = false;
        }else{
            self.Day7Cansel_View.hidden = true;
        }
        
        //-------------------------------　時間の設定 -----------------------------------

        if([[ary_dt1 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            _str_WeekDay1TimeStart = @"";
        }else{
            _str_WeekDay1TimeStart = [ary_dt1 valueForKey:@"start_at"];
        }
        if([[ary_dt1 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            _str_WeekDay1TimeEnd = @"";
        }else{
            _str_WeekDay1TimeEnd = [ary_dt1 valueForKey:@"end_at"];
        }
        
        if([[ary_dt2 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            _str_WeekDay2TimeStart = @"";
        }else{
            _str_WeekDay2TimeStart = [ary_dt2 valueForKey:@"start_at"];
        }
        if([[ary_dt2 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            _str_WeekDay2TimeEnd = @"";
        }else{
            _str_WeekDay2TimeEnd = [ary_dt2 valueForKey:@"end_at"];
        }
        
        if([[ary_dt3 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            _str_WeekDay3TimeStart = @"";
        }else{
            _str_WeekDay3TimeStart = [ary_dt3 valueForKey:@"start_at"];
        }
        if([[ary_dt3 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            _str_WeekDay3TimeEnd = @"";
        }else{
            _str_WeekDay3TimeEnd = [ary_dt3 valueForKey:@"end_at"];
        }
        
        if([[ary_dt4 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            _str_WeekDay4TimeStart = @"";
        }else{
            _str_WeekDay4TimeStart = [ary_dt4 valueForKey:@"start_at"];
        }
        if([[ary_dt4 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            _str_WeekDay4TimeEnd = @"";
        }else{
            _str_WeekDay4TimeEnd = [ary_dt4 valueForKey:@"end_at"];
        }
        
        if([[ary_dt5 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            _str_WeekDay5TimeStart = @"";
        }else{
            _str_WeekDay5TimeStart = [ary_dt5 valueForKey:@"start_at"];
        }
        if([[ary_dt5 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            _str_WeekDay5TimeEnd = @"";
        }else{
            _str_WeekDay5TimeEnd = [ary_dt5 valueForKey:@"end_at"];
        }
        
        if([[ary_dt6 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            _str_WeekDay6TimeStart = @"";
        }else{
            _str_WeekDay6TimeStart = [ary_dt6 valueForKey:@"start_at"];
        }
        if([[ary_dt6 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            _str_WeekDay6TimeEnd = @"";
        }else{
            _str_WeekDay6TimeEnd = [ary_dt6 valueForKey:@"end_at"];
        }
        
        if([[ary_dt7 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            _str_WeekDay7TimeStart = @"";
        }else{
            _str_WeekDay7TimeStart = [ary_dt7 valueForKey:@"start_at"];
        }
        if([[ary_dt7 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            _str_WeekDay7TimeEnd = @"";
        }else{
            _str_WeekDay7TimeEnd = [ary_dt7 valueForKey:@"end_at"];
        }
        NSLog(@"SyussyaYoyakuWeek_ViewController　開始前データ");
        NSLog(@"日曜 %@ %@",_str_WeekDay1TimeStart,_str_WeekDay1TimeEnd);
        NSLog(@"月曜 %@ %@",_str_WeekDay2TimeStart,_str_WeekDay2TimeEnd);
        NSLog(@"火曜 %@ %@",_str_WeekDay3TimeStart,_str_WeekDay3TimeEnd);
        NSLog(@"水曜 %@ %@",_str_WeekDay4TimeStart,_str_WeekDay4TimeEnd);
        NSLog(@"木曜 %@ %@",_str_WeekDay5TimeStart,_str_WeekDay5TimeEnd);
        NSLog(@"金曜 %@ %@",_str_WeekDay6TimeStart,_str_WeekDay6TimeEnd);
        NSLog(@"土曜 %@ %@",_str_WeekDay7TimeStart,_str_WeekDay7TimeEnd);
        
        //初期の状態保存
        _str_initial_WeekDay1TimeStart = _str_WeekDay1TimeStart;
        _str_initial_WeekDay2TimeStart = _str_WeekDay2TimeStart;
        _str_initial_WeekDay3TimeStart = _str_WeekDay3TimeStart;
        _str_initial_WeekDay4TimeStart = _str_WeekDay4TimeStart;
        _str_initial_WeekDay5TimeStart = _str_WeekDay5TimeStart;
        _str_initial_WeekDay6TimeStart = _str_WeekDay6TimeStart;
        _str_initial_WeekDay7TimeStart = _str_WeekDay7TimeStart;
        
        [self setTime];
        
        //------------------------------- 報酬データ格納 -------------------------------
        NSMutableArray* ary_condition = [arrayData valueForKey:@"condition"];
        
        //------------------------------- 出社する日数 -------------------------------
        _lng_number_of_attendances = [[ary_condition valueForKey:@"number_of_attendances"] longValue];

        //------------------------------- 出社する必要のある日数 -------------------------------
        _lng_number_of_conditions = [[ary_condition valueForKey:@"number_of_conditions"] longValue];
        self.NissuCount_lbl.text = [NSString stringWithFormat:@"%ld/%ld", _lng_number_of_attendances, _lng_number_of_conditions];
        if(_lng_number_of_attendances < _lng_number_of_conditions){
            self.NissuFlg_lbl.text = @"NG";
        }else{
            self.NissuFlg_lbl.text = @"OK";
        }
        
        //------------------------------- 指定日の日数を達成しているか否か -------------------------------
        _bln_designations_clear = [[ary_condition valueForKey:@"designations_clear"] boolValue];
        if(_bln_designations_clear){
            self.ShiteiFlg_lbl.text = @"OK";
        }else{
            self.ShiteiFlg_lbl.text = @"NG";
        }
        
        //------------------------------- 報酬目安額 -------------------------------
        _lng_reward = [[arrayData valueForKey:@"reward"] longValue];
        self.meyasuHousyu_lbl.text = [NSString stringWithFormat : @"%@円",[self createStringAddedCommaFromInt:_lng_reward]];
        
        //今週の予定制限取得
        [_api Api_ScheduleslimitGetting:self];
        
    }else{
        
        //通信中解除
        [SVProgressHUD dismiss];
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        //API取得での値セット
                                                        [self setFirstValue];

                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {

    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SyussyaYoyaku_ViewController" bundle:[NSBundle mainBundle]];
        SyussyaYoyakuKakuninWeek_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"SyussyaYoyakuKakuninWeek"];
        initialViewController.SyussyaYoyakuKakuninWeek_delegate = self;
        initialViewController.rootView = _rootView;
        initialViewController.secondView = self;
        initialViewController.api = _api;
        
        //------------------------------- 日別データ格納 -------------------------------
        NSMutableArray* ary_dt1;
        NSMutableArray* ary_dt2;
        NSMutableArray* ary_dt3;
        NSMutableArray* ary_dt4;
        NSMutableArray* ary_dt5;
        NSMutableArray* ary_dt6;
        NSMutableArray* ary_dt7;
        NSMutableArray* ary_week = [arrayData valueForKey:@"week"];
        for(long c=0;c<ary_week.count;c++){
            
            //日別での格納
            switch ([[[ary_week valueForKey:@"wday"] objectAtIndex:c] longValue]) {
                case 0:
                    ary_dt1 = [[arrayData valueForKey:@"week"] objectAtIndex:0];
                    break;
                case 1:
                    ary_dt2 = [[arrayData valueForKey:@"week"] objectAtIndex:1];
                    break;
                case 2:
                    ary_dt3 = [[arrayData valueForKey:@"week"] objectAtIndex:2];
                    break;
                case 3:
                    ary_dt4 = [[arrayData valueForKey:@"week"] objectAtIndex:3];
                    break;
                case 4:
                    ary_dt5 = [[arrayData valueForKey:@"week"] objectAtIndex:4];
                    break;
                case 5:
                    ary_dt6 = [[arrayData valueForKey:@"week"] objectAtIndex:5];
                    break;
                case 6:
                    ary_dt7 = [[arrayData valueForKey:@"week"] objectAtIndex:6];
                    break;
            }
        }
        
        //今週来週の区別用
        initialViewController.lng_WeekType = _lng_WeekType;
        
        //日にちの情報
        initialViewController.ary_months = _ary_months;
        initialViewController.ary_weeks = _ary_weeks;
        initialViewController.dt_wday1 = _dt_wday1;
        initialViewController.dt_wday2 = _dt_wday2;
        initialViewController.dt_wday3 = _dt_wday3;
        initialViewController.dt_wday4 = _dt_wday4;
        initialViewController.dt_wday5 = _dt_wday5;
        initialViewController.dt_wday6 = _dt_wday6;
        initialViewController.dt_wday7 = _dt_wday7;
        
        //予定を登録しているか否かフラグ
        initialViewController.bln_WeekDay1Registration =_bln_WeekDay1Registration;
        initialViewController.bln_WeekDay2Registration =_bln_WeekDay2Registration;
        initialViewController.bln_WeekDay3Registration =_bln_WeekDay3Registration;
        initialViewController.bln_WeekDay4Registration =_bln_WeekDay4Registration;
        initialViewController.bln_WeekDay5Registration =_bln_WeekDay5Registration;
        initialViewController.bln_WeekDay6Registration =_bln_WeekDay6Registration;
        initialViewController.bln_WeekDay7Registration =_bln_WeekDay7Registration;
        
        //勤務状態設定フラグ
        initialViewController.bln_WeekDay1State = [[ary_dt1 valueForKey:@"absence"] boolValue];
        initialViewController.bln_WeekDay2State = [[ary_dt2 valueForKey:@"absence"] boolValue];
        initialViewController.bln_WeekDay3State = [[ary_dt3 valueForKey:@"absence"] boolValue];
        initialViewController.bln_WeekDay4State = [[ary_dt4 valueForKey:@"absence"] boolValue];
        initialViewController.bln_WeekDay5State = [[ary_dt5 valueForKey:@"absence"] boolValue];
        initialViewController.bln_WeekDay6State = [[ary_dt6 valueForKey:@"absence"] boolValue];
        initialViewController.bln_WeekDay7State = [[ary_dt7 valueForKey:@"absence"] boolValue];
        
        //-------------------------------　時間の設定 -----------------------------------
        
        if([[ary_dt1 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay1TimeStart = @"";
        }else{
            initialViewController.str_WeekDay1TimeStart = [ary_dt1 valueForKey:@"start_at"];
        }
        if([[ary_dt1 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay1TimeEnd = @"";
        }else{
            initialViewController.str_WeekDay1TimeEnd = [ary_dt1 valueForKey:@"end_at"];
        }
        
        if([[ary_dt2 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay2TimeStart = @"";
        }else{
            initialViewController.str_WeekDay2TimeStart = [ary_dt2 valueForKey:@"start_at"];
        }
        if([[ary_dt2 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay2TimeEnd = @"";
        }else{
            initialViewController.str_WeekDay2TimeEnd = [ary_dt2 valueForKey:@"end_at"];
        }
        
        if([[ary_dt3 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay3TimeStart = @"";
        }else{
            initialViewController.str_WeekDay3TimeStart = [ary_dt3 valueForKey:@"start_at"];
        }
        if([[ary_dt3 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay3TimeEnd = @"";
        }else{
            initialViewController.str_WeekDay3TimeEnd = [ary_dt3 valueForKey:@"end_at"];
        }
        
        if([[ary_dt4 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay4TimeStart = @"";
        }else{
            initialViewController.str_WeekDay4TimeStart = [ary_dt4 valueForKey:@"start_at"];
        }
        if([[ary_dt4 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay4TimeEnd = @"";
        }else{
            initialViewController.str_WeekDay4TimeEnd = [ary_dt4 valueForKey:@"end_at"];
        }
        
        if([[ary_dt5 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay5TimeStart = @"";
        }else{
            initialViewController.str_WeekDay5TimeStart = [ary_dt5 valueForKey:@"start_at"];
        }
        if([[ary_dt5 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay5TimeEnd = @"";
        }else{
            initialViewController.str_WeekDay5TimeEnd = [ary_dt5 valueForKey:@"end_at"];
        }
        
        if([[ary_dt6 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay6TimeStart = @"";
        }else{
            initialViewController.str_WeekDay6TimeStart = [ary_dt6 valueForKey:@"start_at"];
        }
        if([[ary_dt6 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay6TimeEnd = @"";
        }else{
            initialViewController.str_WeekDay6TimeEnd = [ary_dt6 valueForKey:@"end_at"];
        }
        
        if([[ary_dt7 valueForKey:@"start_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay7TimeStart = @"";
        }else{
            initialViewController.str_WeekDay7TimeStart = [ary_dt7 valueForKey:@"start_at"];
        }
        if([[ary_dt7 valueForKey:@"end_at"] isEqual:[NSNull null]]){
            initialViewController.str_WeekDay7TimeEnd = @"";
        }else{
            initialViewController.str_WeekDay7TimeEnd = [ary_dt7 valueForKey:@"end_at"];
        }
        
        //キャンセル待ち設定
        initialViewController.bln_WeekDay1WaitingCancel = [[ary_dt1 valueForKey:@"waiting_cancel"] boolValue];
        initialViewController.bln_WeekDay2WaitingCancel = [[ary_dt2 valueForKey:@"waiting_cancel"] boolValue];
        initialViewController.bln_WeekDay3WaitingCancel = [[ary_dt3 valueForKey:@"waiting_cancel"] boolValue];
        initialViewController.bln_WeekDay4WaitingCancel = [[ary_dt4 valueForKey:@"waiting_cancel"] boolValue];
        initialViewController.bln_WeekDay5WaitingCancel = [[ary_dt5 valueForKey:@"waiting_cancel"] boolValue];
        initialViewController.bln_WeekDay6WaitingCancel = [[ary_dt6 valueForKey:@"waiting_cancel"] boolValue];
        initialViewController.bln_WeekDay7WaitingCancel = [[ary_dt7 valueForKey:@"waiting_cancel"] boolValue];
        
        //指定日フラグ
        initialViewController.bln_WeekDay1Designation = [[ary_dt1 valueForKey:@"designation"] boolValue];
        initialViewController.bln_WeekDay2Designation = [[ary_dt2 valueForKey:@"designation"] boolValue];
        initialViewController.bln_WeekDay3Designation = [[ary_dt3 valueForKey:@"designation"] boolValue];
        initialViewController.bln_WeekDay4Designation = [[ary_dt4 valueForKey:@"designation"] boolValue];
        initialViewController.bln_WeekDay5Designation = [[ary_dt5 valueForKey:@"designation"] boolValue];
        initialViewController.bln_WeekDay6Designation = [[ary_dt6 valueForKey:@"designation"] boolValue];
        initialViewController.bln_WeekDay7Designation = [[ary_dt7 valueForKey:@"designation"] boolValue];
        
        //報酬
        initialViewController.lng_WeekDay1reward = [[ary_dt1 valueForKey:@"reward"] longValue];
        initialViewController.lng_WeekDay2reward = [[ary_dt2 valueForKey:@"reward"] longValue];
        initialViewController.lng_WeekDay3reward = [[ary_dt3 valueForKey:@"reward"] longValue];
        initialViewController.lng_WeekDay4reward = [[ary_dt4 valueForKey:@"reward"] longValue];
        initialViewController.lng_WeekDay5reward = [[ary_dt5 valueForKey:@"reward"] longValue];
        initialViewController.lng_WeekDay6reward = [[ary_dt6 valueForKey:@"reward"] longValue];
        initialViewController.lng_WeekDay7reward = [[ary_dt7 valueForKey:@"reward"] longValue];

        
        NSMutableArray* ary_condition = [arrayData valueForKey:@"condition"];
        initialViewController.lng_number_of_attendances = [[ary_condition valueForKey:@"number_of_attendances"] longValue];
        initialViewController.lng_number_of_conditions = [[ary_condition valueForKey:@"number_of_conditions"] longValue];
        initialViewController.bln_designations_clear = [[ary_condition valueForKey:@"designations_clear"] boolValue];
        
        initialViewController.lng_reward = [[arrayData valueForKey:@"reward"] longValue];
        
        //確定設定データ
        initialViewController.data_API = _data_API;
        
        [self.navigationController pushViewController:initialViewController animated:NO];
        
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_SchedulesWeekPostGetting:self week_json:_data_Confirmation];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
    
    if(FLG == YES){
        
        //値取得
        _lng_limitwday = [[arrayData valueForKey:@"wday"] longValue];
        _lng_limithour = [[arrayData valueForKey:@"hour"] longValue];
        
        //解禁日の取得
        [_api Api_SchedulesReceptionGetting:self];

    }else{
        
        //通信中解除
        [SVProgressHUD dismiss];
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_ScheduleslimitGetting:self];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        //値取得
        _lng_ReceptionDay = [[arrayData valueForKey:@"wday"] longValue];
        _lng_ReceptionHour = [[arrayData valueForKey:@"hour"] longValue];
        
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_SchedulesReceptionGetting:self];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        if([errorcode isEqualToString:@"Err_401"]){
            
            _bln_loginFlg = true;
            
            // メイン画面に戻る
            [self.navigationController popToViewController:_rootView animated:YES];
        }
    }
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
