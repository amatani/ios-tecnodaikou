//
//  SyussyaYoyakuKakuninWeek_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2016/01/24.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "SyussyaYoyakuKakuteiWeek_ViewController.h"
#import "TotalBasicTimeSetting_ViewController.h"
#import "Wheel_Callender_Library.h"

@protocol SyussyaYoyakuKakuninWeek_ViewControllerDelegate <NSObject>
- (void)SyussyaYoyakuKakuninWeek_LoginBackActionView;
- (void)SyussyaYoyakuKakuninWeek_SetScreenType:(long)lng_WeekType;
- (void)SyussyaYoyakuKakuninWeek_CanselBack;
@end

@interface SyussyaYoyakuKakuninWeek_ViewController : UIViewController <ApiDelegate, SyussyaYoyakuKakuteiWeek_ViewControllerDelegate, TotalBasicTimeSetting_ViewControllerDelegate> {
    
    id<SyussyaYoyakuKakuninWeek_ViewControllerDelegate> _SyussyaYoyakuKakuninWeek_delegate;
    UIViewController* _rootView;
    UIViewController* _secondView;
    Api* _api;
    
    //カレンダーライブラリ用
    Wheel_Callender_Library* _calHoliday;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //引き継がれるパラメータ
    //今週来週の区別用
    long _lng_WeekType;
    
    //日にちの情報
    NSMutableArray* _ary_months;
    NSMutableArray* _ary_weeks;
    NSDate* _dt_wday1;
    NSDate* _dt_wday2;
    NSDate* _dt_wday3;
    NSDate* _dt_wday4;
    NSDate* _dt_wday5;
    NSDate* _dt_wday6;
    NSDate* _dt_wday7;
    
    //確定設定データ
    NSData* _data_API;
    
    //予定を登録しているか否かフラグ
    BOOL _bln_WeekDay1Registration;
    BOOL _bln_WeekDay2Registration;
    BOOL _bln_WeekDay3Registration;
    BOOL _bln_WeekDay4Registration;
    BOOL _bln_WeekDay5Registration;
    BOOL _bln_WeekDay6Registration;
    BOOL _bln_WeekDay7Registration;
    
    //勤務状態設定フラグ
    BOOL _bln_WeekDay1State;
    BOOL _bln_WeekDay2State;
    BOOL _bln_WeekDay3State;
    BOOL _bln_WeekDay4State;
    BOOL _bln_WeekDay5State;
    BOOL _bln_WeekDay6State;
    BOOL _bln_WeekDay7State;

    //勤務時間設定
    NSString* _str_WeekDay1TimeStart;
    NSString* _str_WeekDay1TimeEnd;
    NSString* _str_WeekDay2TimeStart;
    NSString* _str_WeekDay2TimeEnd;
    NSString* _str_WeekDay3TimeStart;
    NSString* _str_WeekDay3TimeEnd;
    NSString* _str_WeekDay4TimeStart;
    NSString* _str_WeekDay4TimeEnd;
    NSString* _str_WeekDay5TimeStart;
    NSString* _str_WeekDay5TimeEnd;
    NSString* _str_WeekDay6TimeStart;
    NSString* _str_WeekDay6TimeEnd;
    NSString* _str_WeekDay7TimeStart;
    NSString* _str_WeekDay7TimeEnd;
    
    //キャンセル待ちフラグ
    BOOL _bln_WeekDay1WaitingCancel;
    BOOL _bln_WeekDay2WaitingCancel;
    BOOL _bln_WeekDay3WaitingCancel;
    BOOL _bln_WeekDay4WaitingCancel;
    BOOL _bln_WeekDay5WaitingCancel;
    BOOL _bln_WeekDay6WaitingCancel;
    BOOL _bln_WeekDay7WaitingCancel;
    
    //指定日フラグ
    BOOL _bln_WeekDay1Designation;
    BOOL _bln_WeekDay2Designation;
    BOOL _bln_WeekDay3Designation;
    BOOL _bln_WeekDay4Designation;
    BOOL _bln_WeekDay5Designation;
    BOOL _bln_WeekDay6Designation;
    BOOL _bln_WeekDay7Designation;
    
    //報酬目安
    long _lng_WeekDay1reward;
    long _lng_WeekDay2reward;
    long _lng_WeekDay3reward;
    long _lng_WeekDay4reward;
    long _lng_WeekDay5reward;
    long _lng_WeekDay6reward;
    long _lng_WeekDay7reward;
    
    //白帯
    long _lng_number_of_attendances;
    long _lng_number_of_conditions;
    BOOL _bln_designations_clear;
    long _lng_reward;
}
@property (nonatomic) id<SyussyaYoyakuKakuninWeek_ViewControllerDelegate> SyussyaYoyakuKakuninWeek_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) UIViewController* secondView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
//今週来週の区別用
@property (nonatomic) long lng_WeekType;

//日にちの情報
@property (nonatomic) NSMutableArray* ary_months;
@property (nonatomic) NSMutableArray* ary_weeks;
@property (nonatomic) NSDate* dt_wday1;
@property (nonatomic) NSDate* dt_wday2;
@property (nonatomic) NSDate* dt_wday3;
@property (nonatomic) NSDate* dt_wday4;
@property (nonatomic) NSDate* dt_wday5;
@property (nonatomic) NSDate* dt_wday6;
@property (nonatomic) NSDate* dt_wday7;

//確定設定データ
@property (nonatomic) NSData* data_API;

//予定を登録しているか否かフラグ
@property (nonatomic) BOOL bln_WeekDay1Registration;
@property (nonatomic) BOOL bln_WeekDay2Registration;
@property (nonatomic) BOOL bln_WeekDay3Registration;
@property (nonatomic) BOOL bln_WeekDay4Registration;
@property (nonatomic) BOOL bln_WeekDay5Registration;
@property (nonatomic) BOOL bln_WeekDay6Registration;
@property (nonatomic) BOOL bln_WeekDay7Registration;

//勤務状態設定フラグ
@property (nonatomic) BOOL bln_WeekDay1State;
@property (nonatomic) BOOL bln_WeekDay2State;
@property (nonatomic) BOOL bln_WeekDay3State;
@property (nonatomic) BOOL bln_WeekDay4State;
@property (nonatomic) BOOL bln_WeekDay5State;
@property (nonatomic) BOOL bln_WeekDay6State;
@property (nonatomic) BOOL bln_WeekDay7State;

//勤務時間設定
@property (nonatomic) NSString* str_WeekDay1TimeStart;
@property (nonatomic) NSString* str_WeekDay1TimeEnd;
@property (nonatomic) NSString* str_WeekDay2TimeStart;
@property (nonatomic) NSString* str_WeekDay2TimeEnd;
@property (nonatomic) NSString* str_WeekDay3TimeStart;
@property (nonatomic) NSString* str_WeekDay3TimeEnd;
@property (nonatomic) NSString* str_WeekDay4TimeStart;
@property (nonatomic) NSString* str_WeekDay4TimeEnd;
@property (nonatomic) NSString* str_WeekDay5TimeStart;
@property (nonatomic) NSString* str_WeekDay5TimeEnd;
@property (nonatomic) NSString* str_WeekDay6TimeStart;
@property (nonatomic) NSString* str_WeekDay6TimeEnd;
@property (nonatomic) NSString* str_WeekDay7TimeStart;
@property (nonatomic) NSString* str_WeekDay7TimeEnd;

//キャンセル待ちフラグ
@property (nonatomic) BOOL bln_WeekDay1WaitingCancel;
@property (nonatomic) BOOL bln_WeekDay2WaitingCancel;
@property (nonatomic) BOOL bln_WeekDay3WaitingCancel;
@property (nonatomic) BOOL bln_WeekDay4WaitingCancel;
@property (nonatomic) BOOL bln_WeekDay5WaitingCancel;
@property (nonatomic) BOOL bln_WeekDay6WaitingCancel;
@property (nonatomic) BOOL bln_WeekDay7WaitingCancel;

//指定日フラグ
@property (nonatomic) BOOL bln_WeekDay1Designation;
@property (nonatomic) BOOL bln_WeekDay2Designation;
@property (nonatomic) BOOL bln_WeekDay3Designation;
@property (nonatomic) BOOL bln_WeekDay4Designation;
@property (nonatomic) BOOL bln_WeekDay5Designation;
@property (nonatomic) BOOL bln_WeekDay6Designation;
@property (nonatomic) BOOL bln_WeekDay7Designation;

//報酬目安
@property (nonatomic) long lng_WeekDay1reward;
@property (nonatomic) long lng_WeekDay2reward;
@property (nonatomic) long lng_WeekDay3reward;
@property (nonatomic) long lng_WeekDay4reward;
@property (nonatomic) long lng_WeekDay5reward;
@property (nonatomic) long lng_WeekDay6reward;
@property (nonatomic) long lng_WeekDay7reward;

//白帯
@property (nonatomic) long lng_number_of_attendances;
@property (nonatomic) long lng_number_of_conditions;
@property (nonatomic) BOOL bln_designations_clear;
@property (nonatomic) long lng_reward;

- (IBAction)LeftButton:(id)sender;
- (IBAction)RightButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Syuu;

@property (weak, nonatomic) IBOutlet UIView *Day1DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day2DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day3DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day4DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day5DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day6DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day7DayBack_View;

@property (weak, nonatomic) IBOutlet UILabel *Day1Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Day_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Week_lbl;

@property (weak, nonatomic) IBOutlet UIView *Day1Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day2Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day3Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day4Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day5Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day6Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day7Shite_view;

@property (weak, nonatomic) IBOutlet UIView *Day1Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day2Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day3Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day4Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day5Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day6Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day7Work_View;

@property (weak, nonatomic) IBOutlet UILabel *Day1Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Work_lbl;

@property (weak, nonatomic) IBOutlet UIView *Day1Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day2Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day3Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day4Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day5Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day6Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day7Cansel_View;

@property (weak, nonatomic) IBOutlet UILabel *Day1TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7TimeFrom_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7TimeTo_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1Money_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Money_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Money_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Money_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Money_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Money_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Money_lbl;

@property (weak, nonatomic) IBOutlet UILabel *NissuCount_lbl;
@property (weak, nonatomic) IBOutlet UILabel *NissuFlg_lbl;
@property (weak, nonatomic) IBOutlet UILabel *ShiteiFlg_lbl;
@property (weak, nonatomic) IBOutlet UILabel *meyasuHousyu_lbl;

@property (weak, nonatomic) IBOutlet UIView *NextWeek_View;
@property (weak, nonatomic) IBOutlet UIView *BackWeek_View;
- (IBAction)NextWeek_Push:(id)sender;
- (IBAction)BackWeek_Push:(id)sender;

- (IBAction)Back_Push:(id)sender;

- (IBAction)OK_Push:(id)sender;

@end