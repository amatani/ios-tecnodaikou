//
//  SyussyaYoyakuWeek_ViewController.h
//  tecnodaikou
//
//  Created by MacServer on 2015/12/27.
//  Copyright © 2015年 Mobile Innovation, LLC. All rights reserved.
//

#import "SyussyaYoyakuKakuninWeek_ViewController.h"
#import "TotalBasicTimeSetting_ViewController.h"
#import "OneDayTimeSetting_ViewController.h"
#import "Wheel_Callender_Library.h"

@protocol SyussyaYoyakuWeek_ViewControllerDelegate <NSObject>
- (void)SyussyaYoyakuWeek_LoginBackActionView;
@end

@interface SyussyaYoyakuWeek_ViewController : UIViewController <ApiDelegate, SyussyaYoyakuKakuninWeek_ViewControllerDelegate, OneDayTimeSetting_ViewControllerDelegate, TotalBasicTimeSetting_ViewControllerDelegate> {
    
    id<SyussyaYoyakuWeek_ViewControllerDelegate> _SyussyaYoyakuWeek_delegate;
    UIViewController* _rootView;
    Api* _api;
    
    //カレンダーライブラリ用
    Wheel_Callender_Library* _calHoliday;
    
    //セッション切れ判別フラグ
    BOOL _bln_loginFlg;
    
    //時間セットから戻った時の再セット無効化フラグ
    BOOL _bln_reloadFlg;
    
    //引き継がれるパラメータ
    //今週来週の区別用
    long _lng_WeekType;
    
    //日にちの情報
    NSMutableArray* _ary_months;
    NSMutableArray* _ary_weeks;
    NSDate* _dt_NowDate;
    NSDate* _dt_wday1;
    NSDate* _dt_wday2;
    NSDate* _dt_wday3;
    NSDate* _dt_wday4;
    NSDate* _dt_wday5;
    NSDate* _dt_wday6;
    NSDate* _dt_wday7;
    
    //今週の制限
    long _lng_limitwday;
    long _lng_limithour;
    
    //解禁日の制限
    long _lng_ReceptionDay;
    long _lng_ReceptionHour;
    
    //確認データ確認用
    NSData* _data_Confirmation;
    
    //確定設定データ
    NSData* _data_API;
    
    //予定を登録しているか否かフラグ
    BOOL _bln_WeekDay1Registration;
    BOOL _bln_WeekDay2Registration;
    BOOL _bln_WeekDay3Registration;
    BOOL _bln_WeekDay4Registration;
    BOOL _bln_WeekDay5Registration;
    BOOL _bln_WeekDay6Registration;
    BOOL _bln_WeekDay7Registration;
    
    //予定を登録しているか否かフラグ(初期フラグ)
    BOOL _bln_initial_WeekDay1Registration;
    BOOL _bln_initial_WeekDay2Registration;
    BOOL _bln_initial_WeekDay3Registration;
    BOOL _bln_initial_WeekDay4Registration;
    BOOL _bln_initial_WeekDay5Registration;
    BOOL _bln_initial_WeekDay6Registration;
    BOOL _bln_initial_WeekDay7Registration;
    
    //勤務状態設定フラグ
    BOOL _bln_WeekDay1State;
    BOOL _bln_WeekDay2State;
    BOOL _bln_WeekDay3State;
    BOOL _bln_WeekDay4State;
    BOOL _bln_WeekDay5State;
    BOOL _bln_WeekDay6State;
    BOOL _bln_WeekDay7State;
    
    //勤務状態設定フラグ(初期フラグ)
    BOOL _bln_initial_WeekDay1State;
    BOOL _bln_initial_WeekDay2State;
    BOOL _bln_initial_WeekDay3State;
    BOOL _bln_initial_WeekDay4State;
    BOOL _bln_initial_WeekDay5State;
    BOOL _bln_initial_WeekDay6State;
    BOOL _bln_initial_WeekDay7State;
    
    //勤務時間設定
    NSString* _str_WeekDay1TimeStart;
    NSString* _str_WeekDay1TimeEnd;
    NSString* _str_WeekDay2TimeStart;
    NSString* _str_WeekDay2TimeEnd;
    NSString* _str_WeekDay3TimeStart;
    NSString* _str_WeekDay3TimeEnd;
    NSString* _str_WeekDay4TimeStart;
    NSString* _str_WeekDay4TimeEnd;
    NSString* _str_WeekDay5TimeStart;
    NSString* _str_WeekDay5TimeEnd;
    NSString* _str_WeekDay6TimeStart;
    NSString* _str_WeekDay6TimeEnd;
    NSString* _str_WeekDay7TimeStart;
    NSString* _str_WeekDay7TimeEnd;
    
    //勤務時間設定(初期フラグ)
    NSString* _str_initial_WeekDay1TimeStart;
    NSString* _str_initial_WeekDay1TimeEnd;
    NSString* _str_initial_WeekDay2TimeStart;
    NSString* _str_initial_WeekDay2TimeEnd;
    NSString* _str_initial_WeekDay3TimeStart;
    NSString* _str_initial_WeekDay3TimeEnd;
    NSString* _str_initial_WeekDay4TimeStart;
    NSString* _str_initial_WeekDay4TimeEnd;
    NSString* _str_initial_WeekDay5TimeStart;
    NSString* _str_initial_WeekDay5TimeEnd;
    NSString* _str_initial_WeekDay6TimeStart;
    NSString* _str_initial_WeekDay6TimeEnd;
    NSString* _str_initial_WeekDay7TimeStart;
    NSString* _str_initial_WeekDay7TimeEnd;
    
    //キャンセル待ちフラグ
    BOOL _bln_WeekDay1WaitingCancel;
    BOOL _bln_WeekDay2WaitingCancel;
    BOOL _bln_WeekDay3WaitingCancel;
    BOOL _bln_WeekDay4WaitingCancel;
    BOOL _bln_WeekDay5WaitingCancel;
    BOOL _bln_WeekDay6WaitingCancel;
    BOOL _bln_WeekDay7WaitingCancel;
    
    //指定日フラグ
    BOOL _bln_WeekDay1Designation;
    BOOL _bln_WeekDay2Designation;
    BOOL _bln_WeekDay3Designation;
    BOOL _bln_WeekDay4Designation;
    BOOL _bln_WeekDay5Designation;
    BOOL _bln_WeekDay6Designation;
    BOOL _bln_WeekDay7Designation;
    
    //出社日数
    long _lng_number_of_attendances;
    long _lng_number_of_conditions;
    BOOL _bln_designations_clear;
    long _lng_reward;
}
@property (nonatomic) id<SyussyaYoyakuWeek_ViewControllerDelegate> SyussyaYoyakuWeek_delegate;
@property (nonatomic) UIViewController* rootView;
@property (nonatomic) Api* api;

//引き継がれるパラメータ
//今週来週の区別用
@property (nonatomic) long lng_WeekType;

- (IBAction)LeftButton:(id)sender;
- (IBAction)RightButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *SetWeekTitle_lbl;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Syuu;

@property (weak, nonatomic) IBOutlet UIView *Day1DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day2DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day3DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day4DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day5DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day6DayBack_View;
@property (weak, nonatomic) IBOutlet UIView *Day7DayBack_View;

@property (weak, nonatomic) IBOutlet UILabel *Day1Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Day_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Week_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Week_lbl;

@property (weak, nonatomic) IBOutlet UIView *Day1Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day2Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day3Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day4Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day5Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day6Shite_view;
@property (weak, nonatomic) IBOutlet UIView *Day7Shite_view;

@property (weak, nonatomic) IBOutlet UIView *Day1Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day2Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day3Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day4Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day5Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day6Work_View;
@property (weak, nonatomic) IBOutlet UIView *Day7Work_View;

@property (weak, nonatomic) IBOutlet UIView *Day1Holiday_View;
@property (weak, nonatomic) IBOutlet UIView *Day2Holiday_View;
@property (weak, nonatomic) IBOutlet UIView *Day3Holiday_View;
@property (weak, nonatomic) IBOutlet UIView *Day4Holiday_View;
@property (weak, nonatomic) IBOutlet UIView *Day5Holiday_View;
@property (weak, nonatomic) IBOutlet UIView *Day6Holiday_View;
@property (weak, nonatomic) IBOutlet UIView *Day7Holiday_View;

@property (weak, nonatomic) IBOutlet UILabel *Day1Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Work_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Work_lbl;

@property (weak, nonatomic) IBOutlet UIView *Day1Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day2Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day3Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day4Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day5Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day6Cansel_View;
@property (weak, nonatomic) IBOutlet UIView *Day7Cansel_View;

@property (weak, nonatomic) IBOutlet UILabel *Day1Holiday_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2Holiday_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3Holiday_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4Holiday_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5Holiday_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6Holiday_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7Holiday_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6TimeFrom_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7TimeFrom_lbl;

@property (weak, nonatomic) IBOutlet UILabel *Day1TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day2TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day3TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day4TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day5TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day6TimeTo_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Day7TimeTo_lbl;

@property (weak, nonatomic) IBOutlet UILabel *NissuCount_lbl;
@property (weak, nonatomic) IBOutlet UILabel *NissuFlg_lbl;
@property (weak, nonatomic) IBOutlet UILabel *ShiteiFlg_lbl;
@property (weak, nonatomic) IBOutlet UILabel *meyasuHousyu_lbl;

- (IBAction)OneWeekHoliday_Push:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *NextWeek_View;
@property (weak, nonatomic) IBOutlet UIView *BackWeek_View;
- (IBAction)NextWeek_Push:(id)sender;
- (IBAction)BackWeek_Push:(id)sender;

- (IBAction)Day1Work_Push:(id)sender;
- (IBAction)Day2Work_Push:(id)sender;
- (IBAction)Day3Work_Push:(id)sender;
- (IBAction)Day4Work_Push:(id)sender;
- (IBAction)Day5Work_Push:(id)sender;
- (IBAction)Day6Work_Push:(id)sender;
- (IBAction)Day7Work_Push:(id)sender;

- (IBAction)Day1Holiday_Push:(id)sender;
- (IBAction)Day2Holiday_Push:(id)sender;
- (IBAction)Day3Holiday_Push:(id)sender;
- (IBAction)Day4Holiday_Push:(id)sender;
- (IBAction)Day5Holiday_Push:(id)sender;
- (IBAction)Day6Holiday_Push:(id)sender;
- (IBAction)Day7Holiday_Push:(id)sender;

- (IBAction)Day1Time_Push:(id)sender;
- (IBAction)Day2Time_Push:(id)sender;
- (IBAction)Day3Time_Push:(id)sender;
- (IBAction)Day4Time_Push:(id)sender;
- (IBAction)Day5Time_Push:(id)sender;
- (IBAction)Day6Time_Push:(id)sender;
- (IBAction)Day7Time_Push:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *OK_Push_lbl;
- (IBAction)OK_Push:(id)sender;

@end