//
//  SyussyaYoyakuKakuninWeek_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2016/01/24.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "SyussyaYoyakuKakuninWeek_ViewController.h"

@interface SyussyaYoyakuKakuninWeek_ViewController ()
@end

@implementation SyussyaYoyakuKakuninWeek_ViewController

@synthesize SyussyaYoyakuKakuninWeek_delegate = _SyussyaYoyakuKakuninWeek_delegate;
@synthesize rootView = _rootView;
@synthesize secondView = _secondView;
@synthesize api = _api;

//引き継がれるパラメータ
@synthesize lng_WeekType = _lng_WeekType;

@synthesize data_API = _data_API;

@synthesize bln_WeekDay1Registration = _bln_WeekDay1Registration;
@synthesize bln_WeekDay2Registration = _bln_WeekDay2Registration;
@synthesize bln_WeekDay3Registration = _bln_WeekDay3Registration;
@synthesize bln_WeekDay4Registration = _bln_WeekDay4Registration;
@synthesize bln_WeekDay5Registration = _bln_WeekDay5Registration;
@synthesize bln_WeekDay6Registration = _bln_WeekDay6Registration;
@synthesize bln_WeekDay7Registration = _bln_WeekDay7Registration;

@synthesize bln_WeekDay1State = _bln_WeekDay1State;
@synthesize bln_WeekDay2State = _bln_WeekDay2State;
@synthesize bln_WeekDay3State = _bln_WeekDay3State;
@synthesize bln_WeekDay4State = _bln_WeekDay4State;
@synthesize bln_WeekDay5State = _bln_WeekDay5State;
@synthesize bln_WeekDay6State = _bln_WeekDay6State;
@synthesize bln_WeekDay7State = _bln_WeekDay7State;

@synthesize str_WeekDay1TimeStart = _str_WeekDay1TimeStart;
@synthesize str_WeekDay1TimeEnd = _str_WeekDay1TimeEnd;
@synthesize str_WeekDay2TimeStart = _str_WeekDay2TimeStart;
@synthesize str_WeekDay2TimeEnd = _str_WeekDay2TimeEnd;
@synthesize str_WeekDay3TimeStart = _str_WeekDay3TimeStart;
@synthesize str_WeekDay3TimeEnd = _str_WeekDay3TimeEnd;
@synthesize str_WeekDay4TimeStart = _str_WeekDay4TimeStart;
@synthesize str_WeekDay4TimeEnd = _str_WeekDay4TimeEnd;
@synthesize str_WeekDay5TimeStart = _str_WeekDay5TimeStart;
@synthesize str_WeekDay5TimeEnd = _str_WeekDay5TimeEnd;
@synthesize str_WeekDay6TimeStart = _str_WeekDay6TimeStart;
@synthesize str_WeekDay6TimeEnd = _str_WeekDay6TimeEnd;
@synthesize str_WeekDay7TimeStart = _str_WeekDay7TimeStart;
@synthesize str_WeekDay7TimeEnd = _str_WeekDay7TimeEnd;

@synthesize bln_WeekDay1WaitingCancel = _bln_WeekDay1WaitingCancel;
@synthesize bln_WeekDay2WaitingCancel = _bln_WeekDay2WaitingCancel;
@synthesize bln_WeekDay3WaitingCancel = _bln_WeekDay3WaitingCancel;
@synthesize bln_WeekDay4WaitingCancel = _bln_WeekDay4WaitingCancel;
@synthesize bln_WeekDay5WaitingCancel = _bln_WeekDay5WaitingCancel;
@synthesize bln_WeekDay6WaitingCancel = _bln_WeekDay6WaitingCancel;
@synthesize bln_WeekDay7WaitingCancel = _bln_WeekDay7WaitingCancel;

@synthesize bln_WeekDay1Designation = _bln_WeekDay1Designation;
@synthesize bln_WeekDay2Designation = _bln_WeekDay2Designation;
@synthesize bln_WeekDay3Designation = _bln_WeekDay3Designation;
@synthesize bln_WeekDay4Designation = _bln_WeekDay4Designation;
@synthesize bln_WeekDay5Designation = _bln_WeekDay5Designation;
@synthesize bln_WeekDay6Designation = _bln_WeekDay6Designation;
@synthesize bln_WeekDay7Designation = _bln_WeekDay7Designation;

@synthesize lng_WeekDay1reward = _lng_WeekDay1reward;
@synthesize lng_WeekDay2reward = _lng_WeekDay2reward;
@synthesize lng_WeekDay3reward = _lng_WeekDay3reward;
@synthesize lng_WeekDay4reward = _lng_WeekDay4reward;
@synthesize lng_WeekDay5reward = _lng_WeekDay5reward;
@synthesize lng_WeekDay6reward = _lng_WeekDay6reward;
@synthesize lng_WeekDay7reward = _lng_WeekDay7reward;

@synthesize lng_number_of_attendances = _lng_number_of_attendances;
@synthesize lng_number_of_conditions = _lng_number_of_conditions;
@synthesize bln_designations_clear = _bln_designations_clear;
@synthesize lng_reward = _lng_reward;

@synthesize ary_months = _ary_months;
@synthesize ary_weeks = _ary_weeks;

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
    
    NSLog(@"SyussyaYoyakuKakuninWeek_ViewController　開始前データ");
    NSLog(@"日曜 %@ %@",_str_WeekDay1TimeStart,_str_WeekDay1TimeEnd);
    NSLog(@"月曜 %@ %@",_str_WeekDay2TimeStart,_str_WeekDay2TimeEnd);
    NSLog(@"火曜 %@ %@",_str_WeekDay3TimeStart,_str_WeekDay3TimeEnd);
    NSLog(@"水曜 %@ %@",_str_WeekDay4TimeStart,_str_WeekDay4TimeEnd);
    NSLog(@"木曜 %@ %@",_str_WeekDay5TimeStart,_str_WeekDay5TimeEnd);
    NSLog(@"金曜 %@ %@",_str_WeekDay6TimeStart,_str_WeekDay6TimeEnd);
    NSLog(@"土曜 %@ %@",_str_WeekDay7TimeStart,_str_WeekDay7TimeEnd);
    
    //ログイン画面移動フラグ初期化
    _bln_loginFlg = false;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIFont *font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/25];
    self.Day1Day_lbl.font = font;
    self.Day2Day_lbl.font = font;
    self.Day3Day_lbl.font = font;
    self.Day4Day_lbl.font = font;
    self.Day5Day_lbl.font = font;
    self.Day6Day_lbl.font = font;
    self.Day7Day_lbl.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/13];
    self.Day1Week_lbl.font = font;
    self.Day2Week_lbl.font = font;
    self.Day3Week_lbl.font = font;
    self.Day4Week_lbl.font = font;
    self.Day5Week_lbl.font = font;
    self.Day6Week_lbl.font = font;
    self.Day7Week_lbl.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W6" size:[Configuration getScreenWidth]/12];
    self.Day1Work_lbl.font = font;
    self.Day2Work_lbl.font = font;
    self.Day3Work_lbl.font = font;
    self.Day4Work_lbl.font = font;
    self.Day5Work_lbl.font = font;
    self.Day6Work_lbl.font = font;
    self.Day7Work_lbl.font = font;
    
    font = [UIFont fontWithName:@"HiraKakuProN-W3" size:[Configuration getScreenWidth]/30];
    self.Day1TimeFrom_lbl.font = font;
    self.Day2TimeFrom_lbl.font = font;
    self.Day3TimeFrom_lbl.font = font;
    self.Day4TimeFrom_lbl.font = font;
    self.Day5TimeFrom_lbl.font = font;
    self.Day6TimeFrom_lbl.font = font;
    self.Day7TimeFrom_lbl.font = font;
    self.Day1TimeTo_lbl.font = font;
    self.Day2TimeTo_lbl.font = font;
    self.Day3TimeTo_lbl.font = font;
    self.Day4TimeTo_lbl.font = font;
    self.Day5TimeTo_lbl.font = font;
    self.Day6TimeTo_lbl.font = font;
    self.Day7TimeTo_lbl.font = font;
    
    //カレンダーライブラリ用初期化
    _calHoliday = [[Wheel_Callender_Library alloc]init];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
    
    //親Viewセットチェック
    if(_rootView == nil){
        
        //全画面に戻る
        [self.navigationController popViewControllerAnimated:YES];
    }else if(_api == nil){
        
        //エラーメッセージ用トースト
        [self.view makeToast:@"Not Set Api."
                    duration:3.0
                    position:CSToastPositionBottom
                       title:nil
                       image:nil
                       style:nil
                  completion:^(BOOL didTap) {
                      
                      // メイン画面に戻る
                      [self.navigationController popToViewController:_rootView animated:YES];
                  }];
    }else{
        
        //apiDelegate設定
        _api.apidelegate = self;
    }
    
    switch (_lng_WeekType) {
        case 0:
            self.NextWeek_View.hidden = true;
            self.BackWeek_View.hidden = true;
            break;
            
        case 1:
            self.NextWeek_View.hidden = false;
            self.BackWeek_View.hidden = true;
            break;
            
        case 2:
            self.NextWeek_View.hidden = true;
            self.BackWeek_View.hidden = false;
            break;
            
        default:
            break;
    }
    
    //日付表示設定
    [self setDay];
    
    //勤務指定のマーク設定
    [self setWorkMark];
    
    //キャンセルの設定
    [self setCansel];
    
    //ボタンの設定変更
    [self setDayWook];
    
    //時間表示設定
    [self setTime];
    
    //金額設定
    [self setMoney];
    
    //------------------------------- 出社する必要のある日数 -------------------------------
    self.NissuCount_lbl.text = [NSString stringWithFormat:@"%ld/%ld", _lng_number_of_attendances, _lng_number_of_conditions];
    if(_lng_number_of_attendances < _lng_number_of_conditions){
        self.NissuFlg_lbl.text = @"NG";
    }else{
        self.NissuFlg_lbl.text = @"OK";
    }
    
    //------------------------------- 指定日の日数を達成しているか否か -------------------------------
    if(_bln_designations_clear){
        self.ShiteiFlg_lbl.text = @"OK";
    }else{
        self.ShiteiFlg_lbl.text = @"NG";
    }
    
    //------------------------------- 報酬目安額 -------------------------------
    self.meyasuHousyu_lbl.text = [NSString stringWithFormat : @"%@円",[self createStringAddedCommaFromInt:_lng_reward]];
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([self class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

//日付の設定
- (void)setDay {
    
    //self.lbl_Syuu.text = [_calHoliday getWeek:[_calHoliday yearNoGet:_dt_wday1] tMonth:[_calHoliday monthNoGet:_dt_wday1] tDay:[_calHoliday dayNoGet:_dt_wday1]];
    if(_ary_months.count == 1){
        
        long lng_month = [[_ary_months objectAtIndex:0] longValue];
        long lng_week = [[_ary_weeks objectAtIndex:0] longValue];
        self.lbl_Syuu.text = [NSString stringWithFormat : @"%ld月第%ld週目", lng_month, lng_week];
    }else if(_ary_months.count == 2){
        
        long lng_month1 = [[_ary_months objectAtIndex:0] longValue];
        long lng_week1 = [[_ary_weeks objectAtIndex:0] longValue];
        long lng_month2 = [[_ary_months objectAtIndex:1] longValue];
        long lng_week2 = [[_ary_weeks objectAtIndex:1] longValue];
        self.lbl_Syuu.text = [NSString stringWithFormat : @"%ld月第%ld週目\n%ld月第%ld週目", lng_month1, lng_week1, lng_month2, lng_week2];
    }
    
    self.Day1Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday1]];
    self.Day2Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday2]];
    self.Day3Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday3]];
    self.Day4Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday4]];
    self.Day5Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday5]];
    self.Day6Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday6]];
    self.Day7Day_lbl.text = [NSString stringWithFormat : @"%ld日", [_calHoliday dayNoGet:_dt_wday7]];
    
    NSString *str_Backcolor_Weekday = @"ffffff";
    NSString *str_Backcolor_Holiday = @"FF33FF";
    NSString *str_Character_Weekday = @"FF33FF";
    NSString *str_Character_Holiday = @"ffffff";
    
    //曜日設定
    //１日目
    switch ([_calHoliday weekdayNoGet:_dt_wday1]) {
        case 1:
            _Day1Week_lbl.text = @"日";
            break;
        case 2:
            _Day1Week_lbl.text = @"月";
            break;
        case 3:
            _Day1Week_lbl.text = @"火";
            break;
        case 4:
            _Day1Week_lbl.text = @"水";
            break;
        case 5:
            _Day1Week_lbl.text = @"木";
            break;
        case 6:
            _Day1Week_lbl.text = @"金";
            break;
        case 7:
            _Day1Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday1] tMonth:[_calHoliday monthNoGet:_dt_wday1] tDay:[_calHoliday dayNoGet:_dt_wday1]]) || ([_calHoliday weekdayNoGet:_dt_wday1] == 1)){
        //日曜のみ色を変える
        _Day1Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day1DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day1Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day1Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day1DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day1Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //２日目
    switch ([_calHoliday weekdayNoGet:_dt_wday2]) {
        case 1:
            _Day2Week_lbl.text = @"日";
            break;
        case 2:
            _Day2Week_lbl.text = @"月";
            break;
        case 3:
            _Day2Week_lbl.text = @"火";
            break;
        case 4:
            _Day2Week_lbl.text = @"水";
            break;
        case 5:
            _Day2Week_lbl.text = @"木";
            break;
        case 6:
            _Day2Week_lbl.text = @"金";
            break;
        case 7:
            _Day2Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday2] tMonth:[_calHoliday monthNoGet:_dt_wday2] tDay:[_calHoliday dayNoGet:_dt_wday2]]) || ([_calHoliday weekdayNoGet:_dt_wday2] == 1)){
        //日曜のみ色を変える
        _Day2Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day2DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day2Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day2Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day2DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day2Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //３日目
    switch ([_calHoliday weekdayNoGet:_dt_wday3]) {
        case 1:
            _Day3Week_lbl.text = @"日";
            break;
        case 2:
            _Day3Week_lbl.text = @"月";
            break;
        case 3:
            _Day3Week_lbl.text = @"火";
            break;
        case 4:
            _Day3Week_lbl.text = @"水";
            break;
        case 5:
            _Day3Week_lbl.text = @"木";
            break;
        case 6:
            _Day3Week_lbl.text = @"金";
            break;
        case 7:
            _Day3Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday3] tMonth:[_calHoliday monthNoGet:_dt_wday3] tDay:[_calHoliday dayNoGet:_dt_wday3]]) || ([_calHoliday weekdayNoGet:_dt_wday3] == 1)){
        //日曜のみ色を変える
        _Day3Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day3DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day3Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day3Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day3DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day3Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //４日目
    switch ([_calHoliday weekdayNoGet:_dt_wday4]) {
        case 1:
            _Day4Week_lbl.text = @"日";
            break;
        case 2:
            _Day4Week_lbl.text = @"月";
            break;
        case 3:
            _Day4Week_lbl.text = @"火";
            break;
        case 4:
            _Day4Week_lbl.text = @"水";
            break;
        case 5:
            _Day4Week_lbl.text = @"木";
            break;
        case 6:
            _Day4Week_lbl.text = @"金";
            break;
        case 7:
            _Day4Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday4] tMonth:[_calHoliday monthNoGet:_dt_wday4] tDay:[_calHoliday dayNoGet:_dt_wday4]]) || ([_calHoliday weekdayNoGet:_dt_wday4] == 1)){
        //日曜のみ色を変える
        _Day4Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day4DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day4Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day4Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day4DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day4Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //５日目
    switch ([_calHoliday weekdayNoGet:_dt_wday5]) {
        case 1:
            _Day5Week_lbl.text = @"日";
            break;
        case 2:
            _Day5Week_lbl.text = @"月";
            break;
        case 3:
            _Day5Week_lbl.text = @"火";
            break;
        case 4:
            _Day5Week_lbl.text = @"水";
            break;
        case 5:
            _Day5Week_lbl.text = @"木";
            break;
        case 6:
            _Day5Week_lbl.text = @"金";
            break;
        case 7:
            _Day5Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday5] tMonth:[_calHoliday monthNoGet:_dt_wday5] tDay:[_calHoliday dayNoGet:_dt_wday5]]) || ([_calHoliday weekdayNoGet:_dt_wday5] == 1)){
        //日曜のみ色を変える
        _Day5Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day5DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day5Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day5Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day5DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day5Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //６日目
    switch ([_calHoliday weekdayNoGet:_dt_wday6]) {
        case 1:
            _Day6Week_lbl.text = @"日";
            break;
        case 2:
            _Day6Week_lbl.text = @"月";
            break;
        case 3:
            _Day6Week_lbl.text = @"火";
            break;
        case 4:
            _Day6Week_lbl.text = @"水";
            break;
        case 5:
            _Day6Week_lbl.text = @"木";
            break;
        case 6:
            _Day6Week_lbl.text = @"金";
            break;
        case 7:
            _Day6Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday6] tMonth:[_calHoliday monthNoGet:_dt_wday6] tDay:[_calHoliday dayNoGet:_dt_wday6]]) || ([_calHoliday weekdayNoGet:_dt_wday6] == 1)){
        //日曜のみ色を変える
        _Day6Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day6DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day6Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day6Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day6DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day6Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
    
    //７日目
    switch ([_calHoliday weekdayNoGet:_dt_wday7]) {
        case 1:
            _Day7Week_lbl.text = @"日";
            break;
        case 2:
            _Day7Week_lbl.text = @"月";
            break;
        case 3:
            _Day7Week_lbl.text = @"火";
            break;
        case 4:
            _Day7Week_lbl.text = @"水";
            break;
        case 5:
            _Day7Week_lbl.text = @"木";
            break;
        case 6:
            _Day7Week_lbl.text = @"金";
            break;
        case 7:
            _Day7Week_lbl.text = @"土";
            break;
    }
    //曜日での色を変える
    if(([_calHoliday holidayCalc:[_calHoliday yearNoGet:_dt_wday7] tMonth:[_calHoliday monthNoGet:_dt_wday7] tDay:[_calHoliday dayNoGet:_dt_wday7]]) || ([_calHoliday weekdayNoGet:_dt_wday7] == 1)){
        //日曜のみ色を変える
        _Day7Day_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
        _Day7DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Weekday];
        _Day7Week_lbl.textColor = [UIColor colorWithHex:str_Character_Weekday];
    }else{
        _Day7Day_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
        _Day7DayBack_View.backgroundColor = [UIColor colorWithHex:str_Backcolor_Holiday];
        _Day7Week_lbl.textColor = [UIColor colorWithHex:str_Character_Holiday];
    }
}

//勤務指定のマーク設定
- (void)setWorkMark {
    
    if(_bln_WeekDay1Designation){
        self.Day1Shite_view.hidden = false;
    }else{
        self.Day1Shite_view.hidden = true;
    }
    if(_bln_WeekDay2Designation){
        self.Day2Shite_view.hidden = false;
    }else{
        self.Day2Shite_view.hidden = true;
    }
    if(_bln_WeekDay3Designation){
        self.Day3Shite_view.hidden = false;
    }else{
        self.Day3Shite_view.hidden = true;
    }
    if(_bln_WeekDay4Designation){
        self.Day4Shite_view.hidden = false;
    }else{
        self.Day4Shite_view.hidden = true;
    }
    if(_bln_WeekDay5Designation){
        self.Day5Shite_view.hidden = false;
    }else{
        self.Day5Shite_view.hidden = true;
    }
    if(_bln_WeekDay6Designation){
        self.Day6Shite_view.hidden = false;
    }else{
        self.Day6Shite_view.hidden = true;
    }
    if(_bln_WeekDay7Designation){
        self.Day7Shite_view.hidden = false;
    }else{
        self.Day7Shite_view.hidden = true;
    }
}

//日別の勤務状態設定
- (void)setDayWook {
    
    [self setDayWook:1 flg:_bln_WeekDay1State];
    [self setDayWook:2 flg:_bln_WeekDay2State];
    [self setDayWook:3 flg:_bln_WeekDay3State];
    [self setDayWook:4 flg:_bln_WeekDay4State];
    [self setDayWook:5 flg:_bln_WeekDay5State];
    [self setDayWook:6 flg:_bln_WeekDay6State];
    [self setDayWook:7 flg:_bln_WeekDay7State];
}

- (void)setDayWook:(long)dayCount
              flg:(BOOL)flag {
    switch (dayCount) {
        case 1:
            if(_bln_WeekDay1Registration == false){
                self.Day1Work_lbl.text = @"未登録";
            }else{
                
                if(flag){
                    self.Day1Work_lbl.text = @"／";
                }else{
                    self.Day1Work_lbl.text = @"出社";
                }
            }
            break;
        case 2:
            if(_bln_WeekDay2Registration == false){
                self.Day2Work_lbl.text = @"未登録";
            }else{
                
                if(flag){
                    self.Day2Work_lbl.text = @"／";
                }else{
                    self.Day2Work_lbl.text = @"出社";
                }
            }
            break;
        case 3:
            if(_bln_WeekDay3Registration == false){
                self.Day3Work_lbl.text = @"未登録";
            }else{
                
                if(flag){
                    self.Day3Work_lbl.text = @"／";
                }else{
                    self.Day3Work_lbl.text = @"出社";
                }
            }
            break;
        case 4:
            if(_bln_WeekDay4Registration == false){
                self.Day4Work_lbl.text = @"未登録";
            }else{
                
                if(flag){
                    self.Day4Work_lbl.text = @"／";
                }else{
                    self.Day4Work_lbl.text = @"出社";
                }
            }
            break;
        case 5:
            if(_bln_WeekDay5Registration == false){
                self.Day5Work_lbl.text = @"未登録";
            }else{
                
                if(flag){
                    self.Day5Work_lbl.text = @"／";
                }else{
                    self.Day5Work_lbl.text = @"出社";
                }
            }
            break;
        case 6:
            if(_bln_WeekDay6Registration == false){
                self.Day6Work_lbl.text = @"未登録";
            }else{
                
                if(flag){
                    self.Day6Work_lbl.text = @"／";
                }else{
                    self.Day6Work_lbl.text = @"出社";
                }
            }
            break;
        case 7:
            if(_bln_WeekDay7Registration == false){
                self.Day7Work_lbl.text = @"未登録";
            }else{
                
                if(flag){
                    self.Day7Work_lbl.text = @"／";
                }else{
                    self.Day7Work_lbl.text = @"出社";
                }
            }
            break;
    }
}

//キャンセルの設定
- (void)setCansel {
    
    BOOL bln_chk = false;
    
    //1日目
    if(_bln_WeekDay1State){
        
        self.Day1Cansel_View.hidden = true;
    }else{
        
        if(_bln_WeekDay1WaitingCancel){
            
            if(_bln_WeekDay1Registration){
                
                self.Day1Cansel_View.hidden = false;
                //キャンセル表示フラグ設定
                bln_chk = true;
            }else{
                
                self.Day1Cansel_View.hidden = true;
            }
            
        }else{
            
            self.Day1Cansel_View.hidden = true;
        }
    }
    
    //2日目
    if(_bln_WeekDay2State){
        
        self.Day2Cansel_View.hidden = true;
    }else{
        
        if(_bln_WeekDay2WaitingCancel){
            
            if(_bln_WeekDay2Registration){
                
                self.Day2Cansel_View.hidden = false;
                //キャンセル表示フラグ設定
                bln_chk = true;
            }else{
                
                self.Day2Cansel_View.hidden = true;
            }
        }else{
            
            self.Day2Cansel_View.hidden = true;
        }
    }
    
    //3日目
    if(_bln_WeekDay3State){
        
        self.Day3Cansel_View.hidden = true;
    }else{
        
        if(_bln_WeekDay3WaitingCancel){
            
            if(_bln_WeekDay3Registration){
                
                self.Day3Cansel_View.hidden = false;
                //キャンセル表示フラグ設定
                bln_chk = true;
            }else{
                
                self.Day3Cansel_View.hidden = true;
            }
        }else{
            
            self.Day3Cansel_View.hidden = true;
        }
    }
    
    //4日目
    if(_bln_WeekDay4State){
        
        self.Day4Cansel_View.hidden = true;
    }else{
        
        if(_bln_WeekDay4WaitingCancel){
            
            if(_bln_WeekDay4Registration){
                
                self.Day4Cansel_View.hidden = false;
                //キャンセル表示フラグ設定
                bln_chk = true;
            }else{
                
                self.Day4Cansel_View.hidden = true;
            }
        }else{
            
            self.Day4Cansel_View.hidden = true;
        }
    }
    
    //5日目
    if(_bln_WeekDay5State){
        
        self.Day5Cansel_View.hidden = true;
    }else{
        
        if(_bln_WeekDay5WaitingCancel){
            
            if(_bln_WeekDay5Registration){
                
                self.Day5Cansel_View.hidden = false;
                //キャンセル表示フラグ設定
                bln_chk = true;
            }else{
                
                self.Day5Cansel_View.hidden = true;
            }
        }else{
            
            self.Day5Cansel_View.hidden = true;
        }
    }
    
    //6日目
    if(_bln_WeekDay6State){
        
        self.Day6Cansel_View.hidden = true;
    }else{
        
        if(_bln_WeekDay6WaitingCancel){
            
            if(_bln_WeekDay6Registration){
                
                self.Day6Cansel_View.hidden = false;
                //キャンセル表示フラグ設定
                bln_chk = true;
            }else{
                
                self.Day6Cansel_View.hidden = true;
            }
        }else{
            
            self.Day6Cansel_View.hidden = true;
        }
    }
    
    //7日目
    if(_bln_WeekDay7State){
        
        self.Day7Cansel_View.hidden = true;
    }else{
        
        if(_bln_WeekDay7WaitingCancel){
            
            if(_bln_WeekDay7Registration){
                
                self.Day7Cansel_View.hidden = false;
                //キャンセル表示フラグ設定
                bln_chk = true;
            }else{
                
                self.Day7Cansel_View.hidden = true;
            }
        }else{
            
            self.Day7Cansel_View.hidden = true;
        }
    }
    
    if(bln_chk){
        
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@""
                                            message:@"キャンセル待ちがあります。"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    
                                                }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

//時間の設定
- (void)setTime {
    
    //1日目
    if(_bln_WeekDay1State){
        
        self.Day1TimeFrom_lbl.text = @"";
        self.Day1TimeTo_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay1Registration == false){
            self.Day1TimeFrom_lbl.text = @"";
            self.Day1TimeTo_lbl.text = @"";
        }else{
            if([_str_WeekDay1TimeStart isEqualToString:@""]){
                self.Day1TimeFrom_lbl.text = @"未定";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay1TimeStart];
                self.Day1TimeFrom_lbl.text = [ary_time objectAtIndex:1];
            }
            if([_str_WeekDay1TimeEnd isEqualToString:@""]){
                self.Day1TimeTo_lbl.text = @"未定";
            }else if([_str_WeekDay1TimeEnd isEqualToString:@"08:00"]){
                self.Day1TimeTo_lbl.text = @"最終";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay1TimeEnd];
                self.Day1TimeTo_lbl.text = [ary_time objectAtIndex:1];
            }
        }
    }
    
    //2日目
    if(_bln_WeekDay2State){
        
        self.Day2TimeFrom_lbl.text = @"";
        self.Day2TimeTo_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay2Registration == false){
            self.Day2TimeFrom_lbl.text = @"";
            self.Day2TimeTo_lbl.text = @"";
        }else{
            if([_str_WeekDay2TimeStart isEqualToString:@""]){
                self.Day2TimeFrom_lbl.text = @"未定";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay2TimeStart];
                self.Day2TimeFrom_lbl.text = [ary_time objectAtIndex:1];
            }
            if([_str_WeekDay2TimeEnd isEqualToString:@""]){
                self.Day2TimeTo_lbl.text = @"未定";
            }else if([_str_WeekDay2TimeEnd isEqualToString:@"08:00"]){
                self.Day2TimeTo_lbl.text = @"最終";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay2TimeEnd];
                self.Day2TimeTo_lbl.text = [ary_time objectAtIndex:1];
            }
        }
    }
    
    //3日目
    if(_bln_WeekDay3State){
        
        self.Day3TimeFrom_lbl.text = @"";
        self.Day3TimeTo_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay3Registration == false){
            self.Day3TimeFrom_lbl.text = @"";
            self.Day3TimeTo_lbl.text = @"";
        }else{
            if([_str_WeekDay3TimeStart isEqualToString:@""]){
                self.Day3TimeFrom_lbl.text = @"未定";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay3TimeStart];
                self.Day3TimeFrom_lbl.text = [ary_time objectAtIndex:1];
            }
            if([_str_WeekDay3TimeEnd isEqualToString:@""]){
                self.Day3TimeTo_lbl.text = @"未定";
            }else if([_str_WeekDay3TimeEnd isEqualToString:@"08:00"]){
                self.Day3TimeTo_lbl.text = @"最終";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay3TimeEnd];
                self.Day3TimeTo_lbl.text = [ary_time objectAtIndex:1];
            }
        }
    }
    
    //4日目
    if(_bln_WeekDay4State){
        
        self.Day4TimeFrom_lbl.text = @"";
        self.Day4TimeTo_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay4Registration == false){
            self.Day4TimeFrom_lbl.text = @"";
            self.Day4TimeTo_lbl.text = @"";
        }else{
            if([_str_WeekDay4TimeStart isEqualToString:@""]){
                self.Day4TimeFrom_lbl.text = @"未定";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay4TimeStart];
                self.Day4TimeFrom_lbl.text = [ary_time objectAtIndex:1];
            }
            if([_str_WeekDay4TimeEnd isEqualToString:@""]){
                self.Day4TimeTo_lbl.text = @"未定";
            }else if([_str_WeekDay4TimeEnd isEqualToString:@"08:00"]){
                self.Day4TimeTo_lbl.text = @"最終";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay4TimeEnd];
                self.Day4TimeTo_lbl.text = [ary_time objectAtIndex:1];
            }
        }
    }
    
    //5日目
    if(_bln_WeekDay5State){
        
        self.Day5TimeFrom_lbl.text = @"";
        self.Day5TimeTo_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay5Registration == false){
            self.Day5TimeFrom_lbl.text = @"";
            self.Day5TimeTo_lbl.text = @"";
        }else{
            if([_str_WeekDay5TimeStart isEqualToString:@""]){
                self.Day5TimeFrom_lbl.text = @"未定";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay5TimeStart];
                self.Day5TimeFrom_lbl.text = [ary_time objectAtIndex:1];
            }
            if([_str_WeekDay5TimeEnd isEqualToString:@""]){
                self.Day5TimeTo_lbl.text = @"未定";
            }else if([_str_WeekDay5TimeEnd isEqualToString:@"08:00"]){
                self.Day5TimeTo_lbl.text = @"最終";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay5TimeEnd];
                self.Day5TimeTo_lbl.text = [ary_time objectAtIndex:1];
            }
        }
    }
    
    //6日目
    if(_bln_WeekDay6State){
        
        self.Day6TimeFrom_lbl.text = @"";
        self.Day6TimeTo_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay6Registration == false){
            self.Day6TimeFrom_lbl.text = @"";
            self.Day6TimeTo_lbl.text = @"";
        }else{
            if([_str_WeekDay6TimeStart isEqualToString:@""]){
                self.Day6TimeFrom_lbl.text = @"未定";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay6TimeStart];
                self.Day6TimeFrom_lbl.text = [ary_time objectAtIndex:1];
            }
            if([_str_WeekDay6TimeEnd isEqualToString:@""]){
                self.Day6TimeTo_lbl.text = @"未定";
            }else if([_str_WeekDay6TimeEnd isEqualToString:@"08:00"]){
                self.Day6TimeTo_lbl.text = @"最終";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay6TimeEnd];
                self.Day6TimeTo_lbl.text = [ary_time objectAtIndex:1];
            }
        }
    }
    
    //7日目
    if(_bln_WeekDay7State){
        
        self.Day7TimeFrom_lbl.text = @"";
        self.Day7TimeTo_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay7Registration == false){
            self.Day7TimeFrom_lbl.text = @"";
            self.Day7TimeTo_lbl.text = @"";
        }else{
            if([_str_WeekDay7TimeStart isEqualToString:@""]){
                self.Day7TimeFrom_lbl.text = @"未定";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay7TimeStart];
                self.Day7TimeFrom_lbl.text = [ary_time objectAtIndex:1];
            }
            if([_str_WeekDay7TimeEnd isEqualToString:@""]){
                self.Day7TimeTo_lbl.text = @"未定";
            }else if([_str_WeekDay7TimeEnd isEqualToString:@"08:00"]){
                self.Day7TimeTo_lbl.text = @"最終";
            }else{
                NSArray* ary_time = [_calHoliday GetTime12:_str_WeekDay7TimeEnd];
                self.Day7TimeTo_lbl.text = [ary_time objectAtIndex:1];
            }
        }
    }
}

//金額設定
- (void)setMoney {
    
    //1日目
    if(_bln_WeekDay1State){
        
        self.Day1Money_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay1Registration == false){
            self.Day1Money_lbl.text = @"";
        }else{
            self.Day1Money_lbl.text = [NSString stringWithFormat:@"%@円",[self createStringAddedCommaFromInt:_lng_WeekDay1reward]];
        }
    }
    
    //2日目
    if(_bln_WeekDay2State){
        
        self.Day2Money_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay2Registration == false){
            self.Day2Money_lbl.text = @"";
        }else{
            self.Day2Money_lbl.text = [NSString stringWithFormat:@"%@円",[self createStringAddedCommaFromInt:_lng_WeekDay2reward]];
        }
    }
    
    //3日目
    if(_bln_WeekDay3State){
        
        self.Day3Money_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay3Registration == false){
            self.Day3Money_lbl.text = @"";
        }else{
            self.Day3Money_lbl.text = [NSString stringWithFormat:@"%@円",[self createStringAddedCommaFromInt:_lng_WeekDay3reward]];
        }
    }
    
    //4日目
    if(_bln_WeekDay4State){
        
        self.Day4Money_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay4Registration == false){
            self.Day4Money_lbl.text = @"";
        }else{
            self.Day4Money_lbl.text = [NSString stringWithFormat:@"%@円",[self createStringAddedCommaFromInt:_lng_WeekDay4reward]];
        }
    }
    
    //5日目
    if(_bln_WeekDay5State){
        
        self.Day5Money_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay5Registration == false){
            self.Day5Money_lbl.text = @"";
        }else{
            self.Day5Money_lbl.text = [NSString stringWithFormat:@"%@円",[self createStringAddedCommaFromInt:_lng_WeekDay5reward]];
        }
    }

    //6日目
    if(_bln_WeekDay6State){
        
        self.Day6Money_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay6Registration == false){
            self.Day6Money_lbl.text = @"";
        }else{
            self.Day6Money_lbl.text = [NSString stringWithFormat:@"%@円",[self createStringAddedCommaFromInt:_lng_WeekDay6reward]];
        }
    }
    
    //7日目
    if(_bln_WeekDay7State){
        
        self.Day7Money_lbl.text = @"";
    }else{
        
        if(_bln_WeekDay7Registration == false){
            self.Day7Money_lbl.text = @"";
        }else{
            self.Day7Money_lbl.text = [NSString stringWithFormat:@"%@円",[self createStringAddedCommaFromInt:_lng_WeekDay7reward]];
        }
    }
}

- (NSString *)createStringAddedCommaFromInt:(long)number
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    [format setGroupingSeparator:@","];
    [format setGroupingSize:3];
    
    return [format stringForObjectValue:[NSNumber numberWithLong:number]];
}

- (IBAction)LeftButton:(id)sender {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:nil
                                        message:@"今週の予定を確定させていません。このままホームに移動しますか？"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                
                                                // メイン画面に戻る
                                                [self.navigationController popToViewController:_rootView animated:YES];
                                            }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)RightButton:(id)sender {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:nil
                                        message:@"今週の予定を確定させていません。このまま基本時間変更に移動しますか？"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                
                                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TimeSetting_ViewController" bundle:[NSBundle mainBundle]];
                                                TotalBasicTimeSetting_ViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"TotalBasicTimeSetting"];
                                                initialViewController.TotalBasicTimeSetting_delegate = self;
                                                initialViewController.rootView = _rootView;
                                                initialViewController.secondView = self;
                                                initialViewController.api = _api;
                                                
                                                [self.navigationController pushViewController:initialViewController animated:YES];
                                            }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)NextWeek_Push:(id)sender {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:nil
                                        message:@"今週の予定を確定させていません。このまま翌週の予定追加ページに移動しますか？"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                
                                                //翌週へ変更
                                                [_SyussyaYoyakuKakuninWeek_delegate SyussyaYoyakuKakuninWeek_SetScreenType:2];
                                                
                                                // 前画面に戻る
                                                [self.navigationController popViewControllerAnimated:NO];
                                            }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)BackWeek_Push:(id)sender {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:nil
                                        message:@"翌週の予定を確定させていません。このまま今週の予定追加ページに移動しますか？"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"戻る"
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                
                                                //今週へ変更
                                                [_SyussyaYoyakuKakuninWeek_delegate SyussyaYoyakuKakuninWeek_SetScreenType:1];
                                                
                                                // 前画面に戻る
                                                [self.navigationController popViewControllerAnimated:NO];
                                            }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)Back_Push:(id)sender {
    
    //キャンセルバック時の値の更新をさせない
    [_SyussyaYoyakuKakuninWeek_delegate SyussyaYoyakuKakuninWeek_CanselBack];
    
    // 前画面に戻る
    [self.navigationController popToViewController:_secondView animated:NO];
}

- (IBAction)OK_Push:(id)sender {
    
    NSLog(@"SyussyaYoyakuKakuninWeek_ViewController　保存時データ");
    NSLog(@"保存時のJSON %@",_data_API);
    
    //確認データでチェック
    [_api Api_SchedulesWeekPostGetting:self week_json:_data_API];
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dialog_Ok",@"")
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//バックアクション
- (void)SyussyaYoyakuKakuninNextWeek_backActionView:(NSMutableArray *)ary {
}
- (void)BasicTimeSetting_BackActionView {
}

//Login画面移動用
- (void)TotalBasicTimeSetting_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}
- (void)SyussyaYoyakuKakuteiWeek_LoginBackActionView {
    
    _bln_loginFlg = true;
    
    // メイン画面に戻る
    [self.navigationController popToViewController:_rootView animated:NO];
}

//Api Delegateからのアクション
- (void)Api_Err_Other {
}
- (void)Api_NewAcountSet_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_LoginSessionCheck_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_Logout_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_PasswordReset_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_MailAdressChenge_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData empty:(BOOL)empty before_attendance:(BOOL)before_attendance errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ProfileGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData area:(NSString*)area username:(NSString*)username username_kana:(NSString*)username_kana parent:(BOOL)parent num_schedule_kind:(long)num_schedule_kind errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NormalNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_InportantNotificationDetailGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicPeriodTimeGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_BasicTimeWeekSetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_CarrierDomainsGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_ScheduleCallenderDayGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_SchedulesWeekPostGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
    
    //通信中解除
    [SVProgressHUD dismiss];
    
    if(FLG == YES){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SyussyaYoyaku_ViewController" bundle:[NSBundle mainBundle]];
        SyussyaYoyakuKakuteiWeek_ViewController* initialViewController = [storyboard instantiateViewControllerWithIdentifier: @"SyussyaYoyakuKakuteiWeek"];
        initialViewController.SyussyaYoyakuKakuteiWeek_delegate = self;
        initialViewController.rootView = _rootView;
        initialViewController.api = _api;
        
        initialViewController.lng_WeekType = _lng_WeekType;
        
        [self.navigationController pushViewController:initialViewController animated:YES];
    }else{
        
        //ステータス５００対策
        if(([errorcode longLongValue] == 500) || ([errorcode longLongValue] == 502) || ([errorcode longLongValue] == 503)){
            
            NSString* str_errMessage = [arrayData valueForKey:@"message"];
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:str_errMessage
                                                message:[NSString stringWithFormat:@"コード:%@",errorcode]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                    }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"リトライ"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        [_api Api_SchedulesWeekPostGetting:self week_json:_data_API];
                                                        
                                                    }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
    }
}
- (void)Api_ScheduleslimitGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode{
}
- (void)Api_SchedulesReceptionGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}
- (void)Api_NotificationCountGetting_BackAction:(BOOL)FLG arrayData:(NSMutableArray*)arrayData errorcode:(NSString*)errorcode {
}

@end
