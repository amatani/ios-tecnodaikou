//
//  Template_ViewController.m
//  tecnodaikou
//
//  Created by MacServer on 2016/01/09.
//  Copyright © 2016年 Mobile Innovation, LLC. All rights reserved.
//

#import "Template_ViewController.h"

@interface Template_ViewController ()

@end

@implementation Template_ViewController

- (void)viewDidLoad {
    
    NSString* className = NSStringFromClass([Template_ViewController class]);
    NSLog(@"%@ - viewDidLoad",className);
    
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([Template_ViewController class]);
    NSLog(@"%@ - viewWillAppear",className);
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([Template_ViewController class]);
    NSLog(@"%@ - viewDidAppear",className);
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([Template_ViewController class]);
    NSLog(@"%@ - viewWillDisappear",className);
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    NSString* className = NSStringFromClass([Template_ViewController class]);
    NSLog(@"%@ - viewDidDisappear",className);
    
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    
    NSString* className = NSStringFromClass([Template_ViewController class]);
    NSLog(@"%@ - viewDidUnload",className);
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    
    NSString* className = NSStringFromClass([Template_ViewController class]);
    NSLog(@"%@ - didReceiveMemoryWarning",className);
    
    [super didReceiveMemoryWarning];
}

-(void)messageError:(NSString*)errTitle
            message:(NSString*)errMessage {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:errTitle
                                        message:errMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
